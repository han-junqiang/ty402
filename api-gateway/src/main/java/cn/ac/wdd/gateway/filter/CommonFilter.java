package cn.ac.wdd.gateway.filter;


import cn.ac.wdd.common.util.TokenUtil;
import cn.ac.wdd.gateway.service.ValidateService;
import cn.hutool.core.collection.CollectionUtil;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ObjectNode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.cloud.gateway.route.Route;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.http.server.reactive.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;
import org.springframework.core.io.buffer.DataBuffer;

import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_REQUEST_URL_ATTR;
import static org.springframework.cloud.gateway.support.ServerWebExchangeUtils.GATEWAY_ROUTE_ATTR;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.List;
/**
 * Common gateway entrance checks Param checks, auth checks, etc.
 */
@Component
@Slf4j
public class CommonFilter implements GlobalFilter {

//  private final LoggerUtil logger = new LoggerUtil(CommonFilter.class);

  private static final String TIMESTAMP = "AUTH-TIMESTAMP";

  private static final String SIGN = "AUTH-SIGN";

  @Autowired
  private ValidateService validateService;

  @Override
  public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
    ServerHttpRequest serverHttpRequest = exchange.getRequest();
    String originalUri = serverHttpRequest.getPath().toString();
    Route route = exchange.getAttribute(GATEWAY_ROUTE_ATTR);
    URI routeUri = exchange.getAttribute(GATEWAY_REQUEST_URL_ATTR);
    log.info("request: " + originalUri + " route to : " + route.getId()
        + ", uri:" + routeUri);
    try {
      //TODO do auth here
      validateService.checkToken(originalUri, serverHttpRequest.getMethodValue(),
          getToken(serverHttpRequest),
          getHeaderParam(serverHttpRequest, TIMESTAMP), getHeaderParam(serverHttpRequest, SIGN));
    } catch (Exception e) {
      log.error("auth error", e);
      return endWithError(exchange, e);
    }
    return chain.filter(exchange);
  }

  private String getHeaderParam(ServerHttpRequest serverHttpRequest, String key) {
    List<String> values = serverHttpRequest.getHeaders().get(key);
    if (CollectionUtil.isEmpty(values)) {
      return null;
    }
    return values.get(0);
  }

  private String getToken(ServerHttpRequest serverHttpRequest) {
    String authHeader = getHeaderParam(serverHttpRequest, TokenUtil.AUTH_HEADER_KEY);
    return TokenUtil.getTokenFromHeader(authHeader).orElse(null);
  }

  private Mono<Void> endWithError(ServerWebExchange exchange, Exception e){
    //TODO get http status code and error message from exception(BaseException or ohter)
    ServerHttpResponse response = exchange.getResponse();
    response.setStatusCode(HttpStatus.OK);
    response.getHeaders().setContentType(MediaType.APPLICATION_JSON);
    ObjectMapper objectMapper = new ObjectMapper();
    ObjectNode objectNode = objectMapper.createObjectNode();
    objectNode.put("code", 0);
    objectNode.put("msg", e.getMessage());
    DataBuffer dataBuffer = null;
    try {
      dataBuffer = response.bufferFactory().allocateBuffer().write(objectMapper.writeValueAsString(objectNode).getBytes(
          StandardCharsets.UTF_8));
    } catch (JsonProcessingException ex) {
      log.error("json format failed");
      dataBuffer = response.bufferFactory().allocateBuffer().write("Service temporarily unavailable".getBytes(
          StandardCharsets.UTF_8));
    }

    return response.writeWith(Mono.just(dataBuffer));
  }
}
