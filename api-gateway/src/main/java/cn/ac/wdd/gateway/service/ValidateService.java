package cn.ac.wdd.gateway.service;

public interface ValidateService {

  void checkToken(String uri, String method, String token, String timestamp, String sign)
      throws Exception;

}
