package cn.ac.wdd.gateway.grpc;

import cn.ac.wdd.access.grpc.AuthorizationRequest;
import cn.ac.wdd.access.grpc.AuthorizationResponse;
import cn.ac.wdd.access.grpc.AuthorizationServiceGrpc;
import io.grpc.ManagedChannel;
import io.grpc.ManagedChannelBuilder;
import io.grpc.StatusRuntimeException;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class GrpcClient {

  private final ManagedChannel channel;



  public GrpcClient(String host, int port) {
    channel = ManagedChannelBuilder.forAddress(host, port).usePlaintext().build();
  }

  public Integer authorize(String token, Integer accessType, String url, String method) {
    AuthorizationServiceGrpc.AuthorizationServiceBlockingStub authorizationServiceBlockingStub = AuthorizationServiceGrpc.newBlockingStub(channel);
    AuthorizationRequest request = AuthorizationRequest.newBuilder().setToken(token)
        .setAccessType(accessType).setUrl(url).setMethod(method).build();
    AuthorizationResponse response;
    try {
      response = authorizationServiceBlockingStub.authorize(request);
      log.debug("GrpcClient#getUserById response, code:{}", response.getResCode());
    } catch (StatusRuntimeException e) {
      log.error("GrpcClient#addBook error, msg:{}, exception:{}", e.toString(), e);
      return null;
    }
    return response.getResCode();
  }

}
