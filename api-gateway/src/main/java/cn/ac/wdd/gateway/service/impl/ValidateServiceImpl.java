package cn.ac.wdd.gateway.service.impl;

import cn.ac.wdd.common.util.StringUtil;
import cn.ac.wdd.gateway.grpc.GrpcClient;
import cn.ac.wdd.gateway.service.ValidateService;
import com.google.common.collect.Lists;
import java.util.List;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;

@Service
public class ValidateServiceImpl implements ValidateService {

  private GrpcClient grpcClient;

  @Value("${grpc.host}")
  private String host;

  @Value("${grpc.port}")
  private Integer port;

  @PostConstruct
  public void init() {
    grpcClient = new GrpcClient(host, port);
  }

  private static final List<String> whiteList = Lists.newArrayList(
      "POST:/fitlive/course",
          "GET:/fitlive/access/doc.html",
          "GET:/fitlive/access/swagge**",
          "GET:**api-docs**",
          "GET:/fitlive/access/webjars/**",
          "POST:/fitlive/access/auth/wechat/login",
          "GET:/fitlive/access/auth/getSms",
          "POST:/fitlive/access/auth/phoneNumber/web"
  );

  private static final Integer HTTP_URL_TYPE = 1;

  @Override
  public void checkToken(String uri, String method, String token, String timestamp, String sign)
      throws Exception {

    return;
//    if (needCheck(uri, method)) {
//      if (StringUtil.isEmpty(token)) {
//        throw new RuntimeException( uri + "token is empty");
//      }
//      Integer resCode = grpcClient.authorize(token, HTTP_URL_TYPE, uri, method);
//      if (resCode == null || !resCode.equals(1)) {
//        throw new RuntimeException("auth failed");
//      }
//    }
  }

  private boolean needCheck(String uri, String method) {
    String key = String.format("%s:%s", method, uri);
    for (String url : whiteList) {
      if (key.matches(url.replace("*", ".*"))) {
        return false;
      }
    }
    return true;
  }

}
