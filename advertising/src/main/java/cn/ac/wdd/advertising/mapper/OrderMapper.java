package cn.ac.wdd.advertising.mapper;

import cn.ac.wdd.advertising.entity.Gender;
import cn.ac.wdd.advertising.entity.Order;
import cn.ac.wdd.advertising.entity.model;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.swagger.models.Model;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-16
 */
public interface OrderMapper extends BaseMapper<Order> {

    List<model> getorderTotalByWeek();

    Gender getUserTotal();

    List<model> getorderTotalByWeekByMerchantId(Long id);
}
