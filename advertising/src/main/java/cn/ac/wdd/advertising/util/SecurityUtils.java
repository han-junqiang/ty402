/**
 * *****************************************************
 * <p>
 * Copyright (C) 2021 bodypark.ai All Rights Reserved This file is part of bodypark project.
 * Unauthorized copy of this file, via any medium is strictly prohibited. Proprietary and
 * Confidential.
 * <p>
 * ****************************************************
 **/
package cn.ac.wdd.advertising.util;

import cn.ac.wdd.advertising.entity.User;
import cn.ac.wdd.common.util.TokenUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;
import java.util.Optional;


@Slf4j
/**
 * @author Zou Pengfei<zoupengfei @ bodypark.ai>
 * @date 05/11/2021 12:48
 */
public class SecurityUtils {

  /**
   * 获取当前用户的id
   */
  public static Optional<Long> getCurrUserId() {
    Optional<User> userInfoOptional = getCurrUser();
    return userInfoOptional.map(User::getId);
  }


  /**
   * 获取当前用户的id（checked）
   */
  public static Long getCurrUserIdChecked() {
    Optional<Long> userIdOptional = SecurityUtils.getCurrUserId();
    Long userId = null;
    userId = userIdOptional.orElseGet(MockContextUserIdHolder::get);
    return userId;
  }


  /**
   * 获取当前用户的信息
   */
  public static Optional<User> getCurrUser() {
    RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
    if (requestAttributes instanceof ServletRequestAttributes) {
      HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
      Object userInfoAttr = request.getAttribute(TokenUtil.USER_INFO_ATTRIBUTE_NAME);
      if (userInfoAttr != null) {
        User userInfo = (User) userInfoAttr;
        return Optional.of(userInfo);
      }
    }
    return Optional.empty();
  }

  /**
   * 获取当前用户的信息（checked）
   */
  public static User getCurrUserChecked() {
    Optional<User> userInfoOptional = getCurrUser();
    return userInfoOptional.get();
  }

  public static class MockContextUserIdHolder {

    static final ThreadLocal<Long> USER_ID_HOLDER = new ThreadLocal<Long>();

    public static Long get() {
      return USER_ID_HOLDER.get();
    }

    public static void set(Long userId) {
      USER_ID_HOLDER.set(userId);
    }

    public static void remove() {
      USER_ID_HOLDER.remove();
    }
  }
}
