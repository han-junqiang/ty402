package cn.ac.wdd.advertising;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @createTime 2022年05月04日 02:07:00
 */
@SpringBootApplication
@MapperScan("cn.ac.wdd.advertising.mapper")
public class Applivation {
    public static void main(String[] args) {
        SpringApplication.run(Applivation.class, args);
    }
}
