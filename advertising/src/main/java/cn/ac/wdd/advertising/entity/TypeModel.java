package cn.ac.wdd.advertising.entity;

/**
 * @createTime 2022年05月16日 15:45:00
 */

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TypeModel {
    private String name;
    private Integer value;
}
