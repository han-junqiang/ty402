package cn.ac.wdd.advertising.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-15
 */
@Getter
@Setter
@TableName("t_advertising_comment")
@ApiModel(value = "AdvertisingComment对象", description = "")
public class AdvertisingComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("商品id")
    @TableField("product_id")
    private String productId;

    @ApiModelProperty("投放id")
    @TableField("market_register_id")
    private Long marketRegisterId;

    @ApiModelProperty("评论内容")
    @TableField("comment_detail")
    private String commentDetail;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("商品名称")
    @TableField(exist = false)
    private String productName;

    @ApiModelProperty("用户名称")
    @TableField(exist = false)
    private String userName;

    @ApiModelProperty("投放名称")
    @TableField(exist = false)
    private String marketRegisterName;
    @ApiModelProperty("回复用户")
    @TableField("answer_detail")
    private String answerDetail;

    @ApiModelProperty("待处理 pending 已经处理：done")
    @TableField("status")
    private String status;


    public static final String ID = "id";

    public static final String USER_ID = "user_id";

    public static final String PRODUCT_ID = "product_id";

    public static final String MARKET_REGISTER_ID = "market_register_id";

    public static final String COMMENT_DETAIL = "comment_detail";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String ANSWER_DETAIL = "answer_detail";

    public static final String STATUS = "status";

}
