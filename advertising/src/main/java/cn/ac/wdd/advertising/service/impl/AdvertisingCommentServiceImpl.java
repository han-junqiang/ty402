package cn.ac.wdd.advertising.service.impl;

import cn.ac.wdd.advertising.entity.AdvertisingComment;
import cn.ac.wdd.advertising.entity.MarketRegister;
import cn.ac.wdd.advertising.entity.Product;
import cn.ac.wdd.advertising.entity.User;
import cn.ac.wdd.advertising.mapper.AdvertisingCommentMapper;
import cn.ac.wdd.advertising.mapper.MarketRegisterMapper;
import cn.ac.wdd.advertising.mapper.ProductMapper;
import cn.ac.wdd.advertising.mapper.UserMapper;
import cn.ac.wdd.advertising.service.IAdvertisingCommentService;
import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.StringUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-15
 */
@Service
public class AdvertisingCommentServiceImpl extends ServiceImpl<AdvertisingCommentMapper, AdvertisingComment> implements IAdvertisingCommentService {

    @Autowired
    private AdvertisingCommentMapper advertisingCommentMapper;

    @Autowired
    private UserMapper userMapper;


    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private MarketRegisterMapper marketRegisterMapper;

    @Override
    public Page<AdvertisingComment> getCommetByPage(Page<AdvertisingComment> page) {
        QueryWrapper<AdvertisingComment> advertisingQueryWrapper = new QueryWrapper<>();
        if (page.getParams().get("detail") != null && StringUtil.isNotEmpty(page.getParams().get("detail").toString())){
            advertisingQueryWrapper.like(AdvertisingComment.COMMENT_DETAIL,page.getParams().get("detail").toString());
        }
        if (page.getParams().get("status") != null && StringUtil.isNotEmpty(page.getParams().get("status").toString())){
            advertisingQueryWrapper.like(AdvertisingComment.STATUS,page.getParams().get("status").toString());
        }
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<AdvertisingComment> advertisingPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>();
        advertisingPage.setSize(page.getPageSize());
        advertisingPage.setCurrent(page.getCurrentPage());
        advertisingPage = advertisingCommentMapper.selectPage(advertisingPage, advertisingQueryWrapper);
        page.setTotalCount((int)advertisingPage.getTotal());
        page.setCurrentPage((int)advertisingPage.getCurrent());
        List<AdvertisingComment> advertisings = advertisingCommentMapper.selectList(advertisingQueryWrapper);
        page.setTotalCount(advertisings.size());
        List<AdvertisingComment> records = advertisingPage.getRecords();

        for (AdvertisingComment record : records) {
            User user = userMapper.selectById(record.getUserId());
            record.setUserName(user.getNickName());
            Product product = productMapper.selectById(record.getProductId());
            record.setProductName(product.getName());
            MarketRegister marketRegister = marketRegisterMapper.selectById(record.getMarketRegisterId());
            record.setMarketRegisterName(marketRegister.getName());
        }

        page.setList(records);
        return page;
    }

    @Override
    public AdvertisingComment getById(Serializable id) {
        AdvertisingComment byId = super.getById(id);

        User user = userMapper.selectById(byId.getUserId());
        byId.setUserName(user.getNickName());
        Product product = productMapper.selectById(byId.getProductId());
        byId.setProductName(product.getName());
        MarketRegister marketRegister = marketRegisterMapper.selectById(byId.getMarketRegisterId());
        byId.setMarketRegisterName(marketRegister.getName());
        return byId;
    }
}
