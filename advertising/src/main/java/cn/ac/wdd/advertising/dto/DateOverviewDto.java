package cn.ac.wdd.advertising.dto;

import cn.ac.wdd.advertising.entity.TypeModel;
import io.swagger.annotations.ApiModelProperty;
import io.swagger.models.auth.In;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @createTime 2022年05月16日 12:30:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class DateOverviewDto {

    private Long merchantTotal;

    private Long productTotal;

    private Long orderTotal;

    private List<Integer> orderTotalList;

    // 一周内订单量
    private List<String> dateAbscissa;
    private List<Integer> everyDayOrderNumber;

    @ApiModelProperty("用户统计")
    private List<Integer> genderList;

    @ApiModelProperty("广告模板数量")
    private Integer advTempletTotal;

    @ApiModelProperty("广告投放数量")
    private Integer advRegistTotal;


    @ApiModelProperty("广告投放情况统计")
    private List<Integer> registerStatusTotal;


    @ApiModelProperty("广告曝光量统计")
    private List<String> exposureAbscissa;
    private List<Integer> exposureNumber;

    @ApiModelProperty("广告投放类型统计")
    private List<TypeModel> registerTypeTotal;

    @ApiModelProperty("广告版面类型统计")
    private List<String> boardAbscissa;
    private List<Integer> boardNumber;


    @ApiModelProperty("广告版面类型收支统计")
    private List<TypeModel> RegisterCapital;

}
