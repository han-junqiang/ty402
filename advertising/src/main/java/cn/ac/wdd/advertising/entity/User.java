package cn.ac.wdd.advertising.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Getter
@Setter
@TableName("t_user")
@ApiModel(value = "User对象", description = "")
public class User implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户手机号")
    @TableField("phone_nunmber")
    private String phoneNunmber;

    @ApiModelProperty("用户昵称")
    @TableField("nickName")
    private String nickName;

    @ApiModelProperty("用户头像")
    @TableField("avatar")
    private String avatar;

    @ApiModelProperty("用户性别")
    @TableField("gender")
    private Integer gender;

    @ApiModelProperty("生日")
    @TableField("birthday")
    private Date birthday;

    @ApiModelProperty("oppen_id")
    @TableField("oppen_id")
    private String oppenId;

    @ApiModelProperty("用户地址")
    @TableField("address")
    private String address;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private Date createAt;

    @ApiModelProperty("更新时间(最后一次登录时间)")
    @TableField("last_visit_at")
    private Date lastVisitAt;

    @ApiModelProperty("最后一次登录时间")
    @TableField(exist = false)
    private  String  lastVisitAtStr;

    public static final String ID = "id";

    public static final String PHONE_NUNMBER = "phone_nunmber";

    public static final String NICKNAME = "nickName";

    public static final String AVATAR = "avatar";

    public static final String GENDER = "gender";

    public static final String BIRTHDAY = "birthday";

    public static final String OPPEN_ID = "oppen_id";

    public static final String ADDRESS = "address";

    public static final String CREATE_AT = "create_at";

    public static final String LAST_VISIT_AT = "last_visit_at";

}
