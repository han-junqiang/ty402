package cn.ac.wdd.advertising.util;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author 韩俊强 hjq13111055378@163.com
 * @createTime 2021年09月24日 14:18:00
 * @Description
 */
@Component
@Slf4j
public class FileUtil {


  public static void deleteFile(String fileName) {
    // 如果文件路径所对应的文件存在，并且是一个文件，则直接删除
    try {
      Files.deleteIfExists(Paths.get(fileName));
    } catch (IOException e) {
      log.error("本地文件删除失败", e);
    }
  }

  public static String uploadFile(MultipartFile file) {
    String fileName = file.getOriginalFilename();
    String localPath = "classpath:\\" + fileName;
    try (BufferedInputStream bufferedInputStream = new BufferedInputStream(file.getInputStream());
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(new FileOutputStream(localPath))) {
      byte[] bytes = new byte[1024];
      int len = 0;
      while ((len = bufferedInputStream.read(bytes)) != -1) {
        bufferedOutputStream.write(bytes, 0, len);
      }
    } catch (IOException e) {
      log.error("文件上传本地服务器出错", e);
      throw new RuntimeException("upload local server user avatar failed", e);
    }
    return localPath;
  }
}
