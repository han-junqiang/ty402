package cn.ac.wdd.advertising.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-16
 */
@Getter
@Setter
@TableName("t_advertising_detail")
@ApiModel(value = "AdvertisingDetail对象", description = "")
public class AdvertisingDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("广告商品id")
    @TableField("product_id")
    private Long productId;

    @ApiModelProperty("广告商品名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("广告商品描述")
    @TableField("detail")
    private String detail;

    @ApiModelProperty("广告物料地址")
    @TableField("url")
    private String url;

    @ApiModelProperty("创建人")
    @TableField("create_by")
    private Integer createBy;

    @ApiModelProperty("更新人")
    @TableField("update_by")
    private Integer updateBy;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;


    public static final String ID = "id";

    public static final String PRODUCT_ID = "product_id";

    public static final String NAME = "name";

    public static final String DETAIL = "detail";

    public static final String URL = "url";

    public static final String CREATE_BY = "create_by";

    public static final String UPDATE_BY = "update_by";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

}
