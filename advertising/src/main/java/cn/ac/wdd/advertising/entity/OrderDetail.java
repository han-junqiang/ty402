package cn.ac.wdd.advertising.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-16
 */
@Getter
@Setter
@TableName("t_order_detail")
@ApiModel(value = "OrderDetail对象", description = "")
public class OrderDetail implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("订单号")
    @TableField("order_number")
    private String orderNumber;

    @ApiModelProperty("规格id")
    @TableField("product_specs_id")
    private Long productSpecsId;

    @ApiModelProperty("商品数量")
    @TableField("product_number")
    private Integer productNumber;

    @ApiModelProperty("原始单价")
    @TableField("ori_price")
    private BigDecimal oriPrice;

    @ApiModelProperty("购买单价")
    @TableField("buy_price")
    private BigDecimal buyPrice;

    @ApiModelProperty("支付单价")
    @TableField("pay_price")
    private BigDecimal payPrice;

    @ApiModelProperty("购买金额")
    @TableField("buy_amount")
    private BigDecimal buyAmount;

    @ApiModelProperty("支付金额")
    @TableField("pay_amount")
    private BigDecimal payAmount;

    @ApiModelProperty("实付金额")
    @TableField("real_pay_amount")
    private BigDecimal realPayAmount;

    @ApiModelProperty("支付类型")
    @TableField("pay_type")
    private String payType;

    @ApiModelProperty("支付完成时间")
    @TableField("pay_finished_time")
    private LocalDateTime payFinishedTime;

    @ApiModelProperty("支付取消时间")
    @TableField("pay_canceled_time")
    private LocalDateTime payCanceledTime;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("商品id")
    @TableField("product_id")
    private Long productId;

    @ApiModelProperty("订单id")
    @TableField("order_id")
    private Long orderId;


    public static final String ID = "id";

    public static final String ORDER_NUMBER = "order_number";

    public static final String PRODUCT_SPECS_ID = "product_specs_id";

    public static final String PRODUCT_NUMBER = "product_number";

    public static final String ORI_PRICE = "ori_price";

    public static final String BUY_PRICE = "buy_price";

    public static final String PAY_PRICE = "pay_price";

    public static final String BUY_AMOUNT = "buy_amount";

    public static final String PAY_AMOUNT = "pay_amount";

    public static final String REAL_PAY_AMOUNT = "real_pay_amount";

    public static final String PAY_TYPE = "pay_type";

    public static final String PAY_FINISHED_TIME = "pay_finished_time";

    public static final String PAY_CANCELED_TIME = "pay_canceled_time";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String PRODUCT_ID = "product_id";

    public static final String ORDER_ID = "order_id";

}
