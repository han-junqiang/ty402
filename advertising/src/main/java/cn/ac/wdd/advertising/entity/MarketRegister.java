package cn.ac.wdd.advertising.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-15
 */
@Getter
@Setter
@TableName("t_market_register")
@ApiModel(value = "MarketRegister对象", description = "")
public class MarketRegister implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("广告商品id")
    @TableField("product_id")
    private Long productId;

    @ApiModelProperty("广告版面id")
    @TableField("advertising_board_id")
    private Long advertisingBoardId;

    @ApiModelProperty("投放类型")
    @TableField("market_type_id")
    private Long marketTypeId;

    @ApiModelProperty("广告商品名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("广告商品描述")
    @TableField("detail")
    private String detail;

    @ApiModelProperty("广告物料地址")
    @TableField("url")
    private String url;

    @ApiModelProperty("创建人")
    @TableField("create_by")
    private Integer createBy;

    @ApiModelProperty("更新人")
    @TableField("update_by")
    private Integer updateBy;

    @ApiModelProperty("投放状态 -1 拒绝  1 待审核  2 待投放 3投放中 4 投放完成")
    @TableField("status")
    private String status;

    @ApiModelProperty("投放开始时间")
    @TableField("market_start_time")
    private LocalDateTime marketStartTime;

    @ApiModelProperty("投放结束时间")
    @TableField("market_end_time")
    private LocalDateTime marketEndTime;

    @ApiModelProperty("单次曝光预算")
    @TableField("once_budget")
    private BigDecimal onceBudget;

    @ApiModelProperty("总曝光次数限制")
    @TableField("show_budget")
    private Long showBudget;

    @ApiModelProperty("总预算")
    @TableField("total_budget")
    private BigDecimal totalBudget;

    @ApiModelProperty("总曝光次数")
    @TableField("total_show")
    private Long totalShow;

    @ApiModelProperty("总价格")
    @TableField("total_price")
    private BigDecimal totalPrice;

    @ApiModelProperty("目标用户性别限制")
    @TableField("target_gender")
    private Integer targetGender;

    @ApiModelProperty("目标用户年龄最大限制")
    @TableField("target_age_max")
    private Integer targetAgeMax;

    @ApiModelProperty("目标用户年龄最小限制")
    @TableField("target_age_min")
    private Integer targetAgeMin;

    @ApiModelProperty("目标用户地址限制")
    @TableField("target_address")
    private String targetAddress;

    @ApiModelProperty("目标用户标签")
    @TableField("target_label")
    private String targetLabel;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;


    public static final String ID = "id";

    public static final String PRODUCT_ID = "product_id";

    public static final String ADVERTISING_BOARD_ID = "advertising_board_id";

    public static final String MARKET_TYPE_ID = "market_type_id";

    public static final String NAME = "name";

    public static final String DETAIL = "detail";

    public static final String URL = "url";

    public static final String CREATE_BY = "create_by";

    public static final String UPDATE_BY = "update_by";

    public static final String STATUS = "status";

    public static final String MARKET_START_TIME = "market_start_time";

    public static final String MARKET_END_TIME = "market_end_time";

    public static final String ONCE_BUDGET = "once_budget";

    public static final String SHOW_BUDGET = "show_budget";

    public static final String TOTAL_BUDGET = "total_budget";

    public static final String TOTAL_SHOW = "total_show";

    public static final String TOTAL_PRICE = "total_price";

    public static final String TARGET_GENDER = "target_gender";

    public static final String TARGET_AGE_MAX = "target_age_max";

    public static final String TARGET_AGE_MIN = "target_age_min";

    public static final String TARGET_ADDRESS = "target_address";

    public static final String TARGET_LABEL = "target_label";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

}
