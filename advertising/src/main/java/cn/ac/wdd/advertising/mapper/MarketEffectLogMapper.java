package cn.ac.wdd.advertising.mapper;

import cn.ac.wdd.advertising.entity.MarketEffectLog;
import cn.ac.wdd.advertising.entity.model;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-16
 */
public interface MarketEffectLogMapper extends BaseMapper<MarketEffectLog> {

    List<model> listWeek();

    List<model> listWeekCurrent(List<Long> ids);
}
