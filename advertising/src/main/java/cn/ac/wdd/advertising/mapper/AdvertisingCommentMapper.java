package cn.ac.wdd.advertising.mapper;

import cn.ac.wdd.advertising.entity.AdvertisingComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-15
 */
public interface AdvertisingCommentMapper extends BaseMapper<AdvertisingComment> {

}
