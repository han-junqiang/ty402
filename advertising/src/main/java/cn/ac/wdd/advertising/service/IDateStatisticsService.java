package cn.ac.wdd.advertising.service;

import cn.ac.wdd.advertising.dto.DateOverviewDto;

/**
 * @createTime 2022年05月16日 12:34:00
 */
public interface IDateStatisticsService {

    public DateOverviewDto getDateOverviewDto();

    DateOverviewDto getorderTotal();

    DateOverviewDto getorderTotalByWeek();

    DateOverviewDto getUserTotal();

    DateOverviewDto getAdvDateOverviewDto();

    DateOverviewDto getRegisterStatusTotal();

    DateOverviewDto getRegisterStatusTotalCurrent();

    DateOverviewDto getRegisterExposureTotal();

    DateOverviewDto getRegisterExposureTotalCurrent();

    DateOverviewDto getAdvDateOverviewDtoCurrent();

    DateOverviewDto getOrderTotalCurrent();

    DateOverviewDto getorderTotalCurrent();

    DateOverviewDto getorderTotalByWeekCurrent();

    DateOverviewDto getRegisterType();

    DateOverviewDto getRegisterTypeCurrent();

    DateOverviewDto getRegisterBoard();

    DateOverviewDto getRegisterBoardCurrent();

    DateOverviewDto getRegisterCapital();

    DateOverviewDto getRegisterCapitalCurrent();
}
