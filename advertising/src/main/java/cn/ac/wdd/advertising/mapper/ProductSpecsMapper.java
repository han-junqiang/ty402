package cn.ac.wdd.advertising.mapper;

import cn.ac.wdd.advertising.entity.ProductSpecs;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-16
 */
public interface ProductSpecsMapper extends BaseMapper<ProductSpecs> {

}
