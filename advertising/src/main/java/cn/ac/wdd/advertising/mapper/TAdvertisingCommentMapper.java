package cn.ac.wdd.advertising.mapper;

import cn.ac.wdd.advertising.entity.TAdvertisingComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-16
 */
public interface TAdvertisingCommentMapper extends BaseMapper<TAdvertisingComment> {

}
