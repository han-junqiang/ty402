package cn.ac.wdd.advertising.controller;

import cn.ac.wdd.advertising.dto.DateOverviewDto;
import cn.ac.wdd.advertising.service.IDateStatisticsService;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @createTime 2022年05月16日 11:45:00
 */
@RestController("statistics")
@ApiOperation("数据统计接口")
public class DateStatisticsController {

    @Autowired
    private IDateStatisticsService dateStatisticsService;

    @GetMapping("/")
    @ApiOperation("数据总揽")
    public ResultMessage<DateOverviewDto> getDateStatisics(){
        return R.ok(dateStatisticsService.getDateOverviewDto());
    }

    @GetMapping("/orderTotal")
    @ApiOperation("订单统计")
    public ResultMessage<DateOverviewDto> getorderTotal(){
        return R.ok(dateStatisticsService.getorderTotal());
    }


    @GetMapping("/orderTotal/Current")
    @ApiOperation("当前商家订单统计")
    public ResultMessage<DateOverviewDto> getorderTotalCurrent(){
        return R.ok(dateStatisticsService.getorderTotalCurrent());
    }



    @GetMapping("/orderTotal/week")
    @ApiOperation("最近一周订单统计")
    public ResultMessage<DateOverviewDto> getorderTotalByWeek(){
        return R.ok(dateStatisticsService.getorderTotalByWeek());
    }

    @GetMapping("/orderTotal/week/Current")
    @ApiOperation("当前商家最近一周订单统计")
    public ResultMessage<DateOverviewDto> getorderTotalByWeekCurrent(){
        return R.ok(dateStatisticsService.getorderTotalByWeekCurrent());
    }



    @GetMapping("/user/total")
    @ApiOperation("用户统计")
    public ResultMessage<DateOverviewDto> getUserTotal(){
        return R.ok(dateStatisticsService.getUserTotal());
    }


    @GetMapping("/adv/Statisics")
    @ApiOperation("广告数据总览")
    public ResultMessage<DateOverviewDto> getAdvDateStatisics(){
        return R.ok(dateStatisticsService.getAdvDateOverviewDto());
    }

    @GetMapping("/adv/Statisics/Current")
    @ApiOperation("当前商家广告数据总览")
    public ResultMessage<DateOverviewDto> getAdvDateStatisicsCurrent(){
        return R.ok(dateStatisticsService.getAdvDateOverviewDtoCurrent());
    }


    @GetMapping("/adv/register/status")
    @ApiOperation("全部广告投放数据统计")
    public ResultMessage<DateOverviewDto> getRegisterStatusTotal(){
        return R.ok(dateStatisticsService.getRegisterStatusTotal());
    }

    @GetMapping("/adv/register/status/current")
    @ApiOperation("当前商家广告投放数据统计")
    public ResultMessage<DateOverviewDto> getRegisterStatusTotalCurrent(){
        return R.ok(dateStatisticsService.getRegisterStatusTotalCurrent());
    }
    @GetMapping("/order/current")
    @ApiOperation("当前商家订单数据总览")
    public ResultMessage<DateOverviewDto> getOrderTotalCurrent(){
        return R.ok(dateStatisticsService.getOrderTotalCurrent());
    }

    @GetMapping("/adv/register/exposure")
    @ApiOperation("全部广告曝光率数据统计")
    public ResultMessage<DateOverviewDto> getRegisterExposureTotal(){
        return R.ok(dateStatisticsService.getRegisterExposureTotal());
    }

    @GetMapping("/adv/register/exposure/current")
    @ApiOperation("当前商家广告曝光率数据统计")
    public ResultMessage<DateOverviewDto> getRegisterExposureTotalCurrent(){
        return R.ok(dateStatisticsService.getRegisterExposureTotalCurrent());
    }


    @GetMapping("/adv/register/type")
    @ApiOperation("广告投放类型数据统计")
    public ResultMessage<DateOverviewDto> getRegisterType(){
        return R.ok(dateStatisticsService.getRegisterType());
    }

    @GetMapping("/adv/register/type/Current")
    @ApiOperation("当前商家广告投放类型数据统计")
    public ResultMessage<DateOverviewDto> getRegisterTypeCurrent(){
        return R.ok(dateStatisticsService.getRegisterTypeCurrent());
    }

    @GetMapping("/adv/register/board")
    @ApiOperation("广告位投放统计")
    public ResultMessage<DateOverviewDto> getRegisterBoard(){
        return R.ok(dateStatisticsService.getRegisterBoard());
    }

    @GetMapping("/adv/register/board/current")
    @ApiOperation("当前商家广告位投放统计")
    public ResultMessage<DateOverviewDto> getRegisterBoardCurrent(){
        return R.ok(dateStatisticsService.getRegisterBoardCurrent());
    }

    @GetMapping("/adv/register/capital")
    @ApiOperation("广告位投放收入统计")
    public ResultMessage<DateOverviewDto> getRegisterCapital(){
        return R.ok(dateStatisticsService.getRegisterCapital());
    }

    @GetMapping("/adv/register/capital/current")
    @ApiOperation("当前商家广告位投放支出统计")
    public ResultMessage<DateOverviewDto> getRegisterCapitalCurrent(){
        return R.ok(dateStatisticsService.getRegisterCapitalCurrent());
    }

}
