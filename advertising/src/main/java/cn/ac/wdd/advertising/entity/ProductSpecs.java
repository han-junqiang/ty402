package cn.ac.wdd.advertising.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-16
 */
@Getter
@Setter
@TableName("t_product_specs")
@ApiModel(value = "ProductSpecs对象", description = "")
public class ProductSpecs implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("商品id")
    @TableField("product_id")
    private Long productId;

    @ApiModelProperty("排序")
    @TableField("product_seq")
    private Integer productSeq;

    @ApiModelProperty("详情")
    @TableField("product_specs")
    private String productSpecs;

    @ApiModelProperty("库存")
    @TableField("product_stock")
    private Integer productStock;

    @ApiModelProperty("原价")
    @TableField("ori_price")
    private BigDecimal oriPrice;

    @ApiModelProperty("售价")
    @TableField("sale_price")
    private BigDecimal salePrice;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;


    public static final String ID = "id";

    public static final String PRODUCT_ID = "product_id";

    public static final String PRODUCT_SEQ = "product_seq";

    public static final String PRODUCT_SPECS = "product_specs";

    public static final String PRODUCT_STOCK = "product_stock";

    public static final String ORI_PRICE = "ori_price";

    public static final String SALE_PRICE = "sale_price";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

}
