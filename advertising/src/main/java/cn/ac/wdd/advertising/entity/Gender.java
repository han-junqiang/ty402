package cn.ac.wdd.advertising.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @createTime 2022年05月16日 13:53:00
 */

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Gender {

    private Integer man;
    private Integer woman;
    private Integer other;
}
