package cn.ac.wdd.advertising.mapper;

import cn.ac.wdd.advertising.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
public interface UserMapper extends BaseMapper<User> {

}
