package cn.ac.wdd.advertising.controller;


import cn.ac.wdd.advertising.entity.AdvertisingComment;
import cn.ac.wdd.advertising.service.IAdvertisingCommentService;
import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/advertising/comment")
public class AdvertisingCommentController {

    @Autowired
    private IAdvertisingCommentService advertisingCommentService;

    @PostMapping("/list")
    @ApiOperation("获取反馈列表")
    public ResultMessage<Page<AdvertisingComment>> getCommetByPage(@RequestBody Page<AdvertisingComment> page){
        page = advertisingCommentService.getCommetByPage(page);
        return R.ok(page);
    }

    @PostMapping("/update")
    @ApiOperation("更新评论状态")
    public ResultMessage<Boolean> updateCommetByPage(@RequestBody AdvertisingComment advertisingComment){
        Boolean update = advertisingCommentService.updateById(advertisingComment);
        return R.ok(update);
    }

    @GetMapping("/")
    @ApiOperation("根据id查看详情")
    public ResultMessage<AdvertisingComment> getById(Long id){
        AdvertisingComment byId = advertisingCommentService.getById(id);
        return R.ok(byId);
    }
}
