package cn.ac.wdd.advertising.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @createTime 2022年05月16日 13:28:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class model {
    private String key;
    private Integer value;
}
