package cn.ac.wdd.advertising.util;



import cn.ac.wdd.advertising.config.TencentCosOSSProperties;
import com.qcloud.cos.COSClient;
import com.qcloud.cos.ClientConfig;
import com.qcloud.cos.auth.BasicCOSCredentials;
import com.qcloud.cos.auth.COSCredentials;
import com.qcloud.cos.http.HttpMethodName;
import com.qcloud.cos.http.HttpProtocol;
import com.qcloud.cos.model.GeneratePresignedUrlRequest;
import com.qcloud.cos.model.ObjectMetadata;
import com.qcloud.cos.region.Region;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

import java.io.*;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.TimeUnit;

@Slf4j
@Component
public class TencentOSSClient {

  private static final long EXPIRE_DURATION = TimeUnit.HOURS.toMillis(1);
  private static COSClient cosClient;
  private TencentCosOSSProperties tencentCosOSSProperties;

  public TencentOSSClient(TencentCosOSSProperties tencentCosOSSProperties) {
    this.tencentCosOSSProperties = tencentCosOSSProperties;
    COSCredentials cred = new BasicCOSCredentials(tencentCosOSSProperties.getSecretId(),
        tencentCosOSSProperties.getSecretKey());
    ClientConfig clientConfig = new ClientConfig(new Region(tencentCosOSSProperties.getRegion()));
    clientConfig.setHttpProtocol(HttpProtocol.https);
    cosClient = new COSClient(cred, clientConfig);
  }

  public String getFullPathWithSignature(String relativePath) {
    if (!StringUtils.isEmpty(relativePath)) {
      // 访问COS获得预签名链接
      GeneratePresignedUrlRequest req =
          new GeneratePresignedUrlRequest(tencentCosOSSProperties.getBucketName(), relativePath,
              HttpMethodName.GET);
      req.setExpiration(new Date(System.currentTimeMillis() + EXPIRE_DURATION));
      URL url = cosClient.generatePresignedUrl(req);
      return url.toString();
    }
    return null;
  }

  public String getFullPathOfStaticResource(String relativePath) {
    if (!StringUtils.isEmpty(relativePath)) {
      return String.format("https://%s.cos.%s.myqcloud.com/static%s",
          tencentCosOSSProperties.getBucketName(), tencentCosOSSProperties.getRegion(),
          relativePath);
    }
    return null;
  }

  public String getFullPathOfResource(String relativePath) {
    if (!StringUtils.isEmpty(relativePath)) {
      return String.format("https://%s.cos.%s.myqcloud.com%s",
          tencentCosOSSProperties.getBucketName(), tencentCosOSSProperties.getRegion(),
          relativePath);
    }
    return null;
  }

  public void uploadImg(byte[] bytes, String relativePath) throws IOException {
    InputStream inputStream = new ByteArrayInputStream(bytes);
    ObjectMetadata objectMetadata = new ObjectMetadata();
    objectMetadata.setContentLength(inputStream.available());
    objectMetadata.setContentType("image/png");
    cosClient.putObject(tencentCosOSSProperties.getBucketName(),
        relativePath, inputStream, objectMetadata);
  }

  public void uploadImg(String filePath, String relativePath) throws IOException {
    InputStream inputStream = new FileInputStream(filePath);
    ObjectMetadata objectMetadata = new ObjectMetadata();
    objectMetadata.setContentLength(inputStream.available());
    objectMetadata.setContentType("image/png");
    cosClient.putObject(tencentCosOSSProperties.getBucketName(),
        relativePath, inputStream, objectMetadata);
  }

  public void uploadFile(String localPath, String relativePath) {
    File file = new File(localPath);
    try (FileInputStream fileInputStream = new FileInputStream(file)) {
      ObjectMetadata objectMetadata = new ObjectMetadata();
      objectMetadata.setContentLength(fileInputStream.available());
      cosClient.putObject(tencentCosOSSProperties.getBucketName(),
          relativePath, fileInputStream, objectMetadata);
    } catch (Exception e) {
      log.error("文件没有找到", e);
      throw new RuntimeException(e);
    }
  }


  public void uploadImgFromUrl(String fileUrl, String relativePath) throws IOException {
    InputStream inputStream = new URL(fileUrl).openStream();
    ObjectMetadata objectMetadata = new ObjectMetadata();
    objectMetadata.setContentLength(inputStream.available());
    objectMetadata.setContentType("image/png");
    cosClient.putObject(tencentCosOSSProperties.getBucketName(),
        relativePath, inputStream, objectMetadata);
  }
}
