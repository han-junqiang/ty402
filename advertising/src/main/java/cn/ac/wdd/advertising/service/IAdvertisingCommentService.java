package cn.ac.wdd.advertising.service;

import cn.ac.wdd.advertising.entity.AdvertisingComment;
import cn.ac.wdd.common.util.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-15
 */
public interface IAdvertisingCommentService extends IService<AdvertisingComment> {

    Page<AdvertisingComment> getCommetByPage(Page<AdvertisingComment> page);
}
