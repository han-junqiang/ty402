package cn.ac.wdd.advertising.service.impl;

import cn.ac.wdd.advertising.dto.DateOverviewDto;
import cn.ac.wdd.advertising.entity.*;
import cn.ac.wdd.advertising.mapper.*;
import cn.ac.wdd.advertising.service.IDateStatisticsService;
import cn.ac.wdd.advertising.util.SecurityUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @createTime 2022年05月16日 12:34:00
 */
@Service
public class DateStatisticsServiceImpl implements IDateStatisticsService {

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private ProductSpecsMapper productSpecsMapper;

    @Autowired
    private MerchantMapper merchantMapper;

    @Autowired
    private AdvertisingDetailMapper advertisingDetailMapper;

    @Autowired
    private MarketRegisterMapper marketRegisterMapper;

    @Autowired
    private MarketEffectLogMapper marketEffectLogMapper;

    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Override
    public DateOverviewDto getDateOverviewDto() {
        List<Order> orders = orderMapper.selectList(null);
        List<ProductSpecs> productSpecs = productSpecsMapper.selectList(null);
        List<Merchant> merchants = merchantMapper.selectList(null);
        return DateOverviewDto.builder().productTotal(Long.valueOf(productSpecs.size()))
                .orderTotal(Long.valueOf(orders.size()))
                .merchantTotal(Long.valueOf(merchants.size()))
                .build();
    }

    @Override
    public DateOverviewDto getorderTotal() {
        ArrayList<Integer> orderList = new ArrayList<>();
        QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
        List<Order> orders = orderMapper.selectList(null);
        orderList.add(orders.size());
        orderQueryWrapper.eq(Order.ORDER_STATE,"unpaid");
        orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        orderQueryWrapper.clear();
        orderQueryWrapper.eq(Order.ORDER_STATE,"paid");
        orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        orderQueryWrapper.clear();
        orderQueryWrapper.eq(Order.ORDER_STATE,"receive");
        orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        orderQueryWrapper.clear();
        orderQueryWrapper.eq(Order.ORDER_STATE,"evaluate");
        orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        orderQueryWrapper.clear();
        orderQueryWrapper.eq(Order.ORDER_STATE,"closure");
        orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        return DateOverviewDto.builder().orderTotalList(orderList).build();
    }

    @Override
    public DateOverviewDto getorderTotalByWeek() {
        ArrayList<String> dateAbscissa = new ArrayList<>();
        ArrayList<Integer> everyDayOrderNumber = new ArrayList<>();
        List<model> modelList = orderMapper.getorderTotalByWeek();

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate now = LocalDate.now();
        for (int i = 0; i < 7; i++) {
            now = now.plusDays(-1 * i);
            String format = dateTimeFormatter.format(now);
            dateAbscissa.add(format);
            everyDayOrderNumber.add(0);
            for (model model : modelList) {
                if (model.getKey().equals(format)){
                    everyDayOrderNumber.set(everyDayOrderNumber.size()-1,model.getValue());
                    break;
                }
            }

        }
        return DateOverviewDto.builder().dateAbscissa(dateAbscissa).everyDayOrderNumber(everyDayOrderNumber).build();
    }

    @Override
    public DateOverviewDto getUserTotal() {
        Gender gender = orderMapper.getUserTotal();
        ArrayList<Integer> genderList = new ArrayList<>();
        genderList.add(gender.getMan());
        genderList.add(gender.getWoman());
        genderList.add(gender.getOther());
        return DateOverviewDto.builder().genderList(genderList).build();
    }

    @Override
    public DateOverviewDto getAdvDateOverviewDto() {
        List<ProductSpecs> productSpecs = productSpecsMapper.selectList(null);
        List<AdvertisingDetail> advertisingDetailList = advertisingDetailMapper.selectList(null);
        List<MarketRegister> marketRegisters = marketRegisterMapper.selectList(null);
        return DateOverviewDto.builder().productTotal(Long.valueOf(productSpecs.size())).advTempletTotal(advertisingDetailList.size())
                .advRegistTotal(marketRegisters.size()).build();
    }

    @Override
    public DateOverviewDto getRegisterStatusTotal() {
        List<MarketRegister> marketRegisters = marketRegisterMapper.selectList(null);
        Integer refuse = 0;
        Integer audit = 0;
        Integer willThrow = 0;
        Integer throwing = 0;
        Integer throwed = 0;
        for (MarketRegister marketRegister : marketRegisters) {
            switch (marketRegister.getStatus()){
                case "-1": refuse++; break;
                case "1":  audit++;break;
                case "2": willThrow++;break;
                case "3": throwing++;break;
                case "4": throwed++;break;
            }
        }
        ArrayList<Integer> registerStatusTotal = new ArrayList<>();
        registerStatusTotal.add(marketRegisters.size());
        registerStatusTotal.add(refuse);
        registerStatusTotal.add(audit);
        registerStatusTotal.add(willThrow);
        registerStatusTotal.add(throwing);
        registerStatusTotal.add(throwed);
        return DateOverviewDto.builder().registerStatusTotal(registerStatusTotal).build();
    }

    @Override
    public DateOverviewDto getRegisterStatusTotalCurrent() {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.MERCHANT_ID,merchant.getId());
        List<Product> products = productMapper.selectList(productQueryWrapper);QueryWrapper<MarketRegister> marketRegisterQueryWrapper = new QueryWrapper<>();
        marketRegisterQueryWrapper.in(MarketRegister.PRODUCT_ID, products.stream().map(Product::getId).collect(Collectors.toList()));
        List<MarketRegister> marketRegisters = marketRegisterMapper.selectList(marketRegisterQueryWrapper);

        Integer refuse = 0;
        Integer audit = 0;
        Integer willThrow = 0;
        Integer throwing = 0;
        Integer throwed = 0;
        for (MarketRegister marketRegister : marketRegisters) {
            switch (marketRegister.getStatus()){
                case "-1": refuse++; break;
                case "1":  audit++;break;
                case "2": willThrow++;break;
                case "3": throwing++;break;
                case "4": throwed++;break;
            }
        }
        ArrayList<Integer> registerStatusTotal = new ArrayList<>();
        registerStatusTotal.add(marketRegisters.size());
        registerStatusTotal.add(refuse);
        registerStatusTotal.add(audit);
        registerStatusTotal.add(willThrow);
        registerStatusTotal.add(throwing);
        registerStatusTotal.add(throwed);
        return DateOverviewDto.builder().registerStatusTotal(registerStatusTotal).build();
    }

    @Override
    public DateOverviewDto getRegisterExposureTotal() {
        List<model> modelList = marketEffectLogMapper.listWeek();
        List<String> exposureAbscissa = new ArrayList<>();
        List<Integer> exposureNumber = new ArrayList<>();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate now = LocalDate.now();
        for (int i = 0; i < 7; i++) {
            now = now.plusDays(-1 * i);
            String format = dateTimeFormatter.format(now);
            exposureAbscissa.add(format);
            exposureNumber.add(0);
            for (model model : modelList) {
                if (model.getKey().equals(format)){
                    exposureNumber.set(exposureNumber.size()-1,model.getValue());
                    break;
                }
            }
        }
        return DateOverviewDto.builder().exposureAbscissa(exposureAbscissa).exposureNumber(exposureNumber).build();
    }

    @Override
    public DateOverviewDto getRegisterExposureTotalCurrent() {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.MERCHANT_ID,merchant.getId());
        List<Product> products = productMapper.selectList(productQueryWrapper);
        List<model> modelList = marketEffectLogMapper.listWeekCurrent(products.stream().map(Product::getId).collect(Collectors.toList()));

        List<String> exposureAbscissa = new ArrayList<>();
        List<Integer> exposureNumber = new ArrayList<>();
        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate now = LocalDate.now();
        for (int i = 0; i < 7; i++) {
            now = now.plusDays(-1 * i);
            String format = dateTimeFormatter.format(now);
            exposureAbscissa.add(format);
            exposureNumber.add(0);
            for (model model : modelList) {
                if (model.getKey().equals(format)){
                    exposureNumber.set(exposureNumber.size()-1,model.getValue());
                    break;
                }
            }
        }
        return DateOverviewDto.builder().exposureAbscissa(exposureAbscissa).exposureNumber(exposureNumber).build();
    }

    @Override
    public DateOverviewDto getAdvDateOverviewDtoCurrent() {

        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.MERCHANT_ID,merchant.getId());
        List<Product> products = productMapper.selectList(productQueryWrapper);

        QueryWrapper<ProductSpecs> productSpecsQueryWrapper = new QueryWrapper<>();
        productSpecsQueryWrapper.in(ProductSpecs.PRODUCT_ID, products.stream().map(Product::getId).collect(Collectors.toList()));
        List<ProductSpecs> productSpecs = productSpecsMapper.selectList(productSpecsQueryWrapper);

        QueryWrapper<AdvertisingDetail> advertisingDetailQueryWrapper = new QueryWrapper<>();
        advertisingDetailQueryWrapper.in(AdvertisingDetail.PRODUCT_ID,products.stream().map(Product::getId).collect(Collectors.toList()));
        List<AdvertisingDetail> advertisingDetailList = advertisingDetailMapper.selectList(advertisingDetailQueryWrapper);

        QueryWrapper<MarketRegister> marketRegisterQueryWrapper = new QueryWrapper<>();
        marketRegisterQueryWrapper.in(MarketRegister.PRODUCT_ID,products.stream().map(Product::getId).collect(Collectors.toList()));
        List<MarketRegister> marketRegisters = marketRegisterMapper.selectList(marketRegisterQueryWrapper);

        return DateOverviewDto.builder().productTotal(Long.valueOf(productSpecs.size())).advTempletTotal(advertisingDetailList.size())
                .advRegistTotal(marketRegisters.size()).build();
    }

    @Override
    public DateOverviewDto getOrderTotalCurrent() {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.MERCHANT_ID,merchant.getId());
        List<Product> products = productMapper.selectList(productQueryWrapper);
        QueryWrapper<ProductSpecs> productSpecsQueryWrapper = new QueryWrapper<>();
        productSpecsQueryWrapper.in(ProductSpecs.PRODUCT_ID, products.stream().map(Product::getId).collect(Collectors.toList()));
        List<ProductSpecs> productSpecs = productSpecsMapper.selectList(productSpecsQueryWrapper);

        QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
        orderDetailQueryWrapper.in(OrderDetail.PRODUCT_ID,products.stream().map(Product::getId).collect(Collectors.toList()));
        List<OrderDetail> orderDetails = orderDetailMapper.selectList(orderDetailQueryWrapper);

        return DateOverviewDto.builder().productTotal(Long.valueOf(productSpecs.size())).orderTotal(Long.valueOf(orderDetails.size())).build();
    }

    @Override
    public DateOverviewDto getorderTotalCurrent() {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);

        ArrayList<Integer> orderList = new ArrayList<>();
        QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.in(Order.MERCHANT_ID,merchant.getId());
        List<Order> orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        orderQueryWrapper.eq(Order.ORDER_STATE,"unpaid");
        orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        orderQueryWrapper.clear();
        orderQueryWrapper.in(Order.MERCHANT_ID,merchant.getId());
        orderQueryWrapper.eq(Order.ORDER_STATE,"paid");
        orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        orderQueryWrapper.clear();
        orderQueryWrapper.in(Order.MERCHANT_ID,merchant.getId());
        orderQueryWrapper.eq(Order.ORDER_STATE,"receive");
        orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        orderQueryWrapper.clear();
        orderQueryWrapper.in(Order.MERCHANT_ID,merchant.getId());
        orderQueryWrapper.eq(Order.ORDER_STATE,"evaluate");
        orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        orderQueryWrapper.clear();
        orderQueryWrapper.in(Order.MERCHANT_ID,merchant.getId());
        orderQueryWrapper.eq(Order.ORDER_STATE,"closure");
        orders = orderMapper.selectList(orderQueryWrapper);
        orderList.add(orders.size());
        return DateOverviewDto.builder().orderTotalList(orderList).build();
    }

    @Override
    public DateOverviewDto getorderTotalByWeekCurrent() {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);

        ArrayList<String> dateAbscissa = new ArrayList<>();
        ArrayList<Integer> everyDayOrderNumber = new ArrayList<>();
        List<model> modelList = orderMapper.getorderTotalByWeekByMerchantId(merchant.getId());

        DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");
        LocalDate now = LocalDate.now();
        for (int i = 0; i < 7; i++) {
            now = now.plusDays(-1 * i);
            String format = dateTimeFormatter.format(now);
            dateAbscissa.add(format);
            everyDayOrderNumber.add(0);
            for (model model : modelList) {
                if (model.getKey().equals(format)){
                    everyDayOrderNumber.set(everyDayOrderNumber.size()-1,model.getValue());
                    break;
                }
            }

        }
        return DateOverviewDto.builder().dateAbscissa(dateAbscissa).everyDayOrderNumber(everyDayOrderNumber).build();
    }

    @Override
    public DateOverviewDto getRegisterType() {
        List<TypeModel> list = marketRegisterMapper.getRegisterType();
        return DateOverviewDto.builder().registerTypeTotal(list).build();
    }

    @Override
    public DateOverviewDto getRegisterTypeCurrent() {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.MERCHANT_ID,merchant.getId());
        List<Product> products = productMapper.selectList(productQueryWrapper);
        List<TypeModel> list = marketRegisterMapper.getRegisterTypeCurrent(products.stream().map(Product::getId).collect(Collectors.toList()));
        return DateOverviewDto.builder().registerTypeTotal(list).build();
    }

    @Override
    public DateOverviewDto getRegisterBoard() {
        List<String> boardAbscissa = new ArrayList<>();
        List<Integer> boardNumber = new ArrayList<>();
        List<TypeModel> typeModels = marketRegisterMapper.getRegisterBoard();

        typeModels.forEach(p->{
            boardAbscissa.add(p.getName());
            boardNumber.add(p.getValue());
        });
         return DateOverviewDto.builder().boardAbscissa(boardAbscissa).boardNumber(boardNumber).build();
    }

    @Override
    public DateOverviewDto getRegisterBoardCurrent() {

        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.MERCHANT_ID,merchant.getId());
        List<Product> products = productMapper.selectList(productQueryWrapper);


        List<String> boardAbscissa = new ArrayList<>();
        List<Integer> boardNumber = new ArrayList<>();
        List<TypeModel> typeModels = marketRegisterMapper.getRegisterBoardCurrent(products.stream().map(Product::getId).collect(Collectors.toList()));

        typeModels.forEach(p->{
            boardAbscissa.add(p.getName());
            boardNumber.add(p.getValue());
        });
        return DateOverviewDto.builder().boardAbscissa(boardAbscissa).boardNumber(boardNumber).build();
    }

    @Override
    public DateOverviewDto getRegisterCapital() {
        List<TypeModel> typeModels = marketRegisterMapper.getRegisterCapital();
        return DateOverviewDto.builder().RegisterCapital(typeModels).build();

    }

    @Override
    public DateOverviewDto getRegisterCapitalCurrent() {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.MERCHANT_ID,merchant.getId());
        List<Product> products = productMapper.selectList(productQueryWrapper);
        List<TypeModel> typeModels = marketRegisterMapper.getRegisterCapitalCurrent(products.stream().map(Product::getId).collect(Collectors.toList()));
        return DateOverviewDto.builder().RegisterCapital(typeModels).build();
    }
}
