package cn.ac.wdd.advertising.service;

import cn.ac.wdd.advertising.entity.User;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
public interface IUserService extends IService<User> {

}
