package cn.ac.wdd.advertising.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-16
 */
@Getter
@Setter
@TableName("t_market_effect_log")
@ApiModel(value = "MarketEffectLog对象", description = "")
public class MarketEffectLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("商品id")
    @TableField("product_id")
    private Long productId;

    @ApiModelProperty("投放id")
    @TableField("market_register_id")
    private Long marketRegisterId;

    @ApiModelProperty("事件类型 onclick(点击) add_shopping_trolley(添加购物车) create_order(下单) pay_finish(支付完成) banner_show(banner显示) goods_information_stremam(信息流显示)")
    @TableField("event_type")
    private String eventType;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;


    public static final String ID = "id";

    public static final String USER_ID = "user_id";

    public static final String PRODUCT_ID = "product_id";

    public static final String MARKET_REGISTER_ID = "market_register_id";

    public static final String EVENT_TYPE = "event_type";

    public static final String CREATE_AT = "create_at";

}
