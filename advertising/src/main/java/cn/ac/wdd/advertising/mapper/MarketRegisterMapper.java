package cn.ac.wdd.advertising.mapper;

import cn.ac.wdd.advertising.entity.MarketRegister;
import cn.ac.wdd.advertising.entity.TypeModel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-15
 */
public interface MarketRegisterMapper extends BaseMapper<MarketRegister> {

    List<TypeModel> getRegisterType();

    List<TypeModel> getRegisterTypeCurrent(List<Long> ids);

    List<TypeModel> getRegisterBoard();

    List<TypeModel> getRegisterBoardCurrent(List<Long> ids);

    List<TypeModel> getRegisterCapital();

    List<TypeModel> getRegisterCapitalCurrent(List<Long> collect);
}
