package cn.ac.wdd.shopping.protocol.request;

import com.baomidou.mybatisplus.annotation.TableField;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @createTime 2022年05月12日 13:50:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class OrderRequest {

    @ApiModelProperty("用户id")
    private Long userId;

    @ApiModelProperty("商家id")
    private Long merchantId;

    @ApiModelProperty("商品id")
    private Long productId;

    @ApiModelProperty("规格id")
    @TableField("product_specs_id")
    private Long productSpecsId;

    @ApiModelProperty("商品数量")
    private Integer productNumber;

    @ApiModelProperty("购买金额")
    private BigDecimal buyAmount;

    @ApiModelProperty("投放id")
    private Long marketRegisterId;

    @ApiModelProperty("备注")
    private String comment;

    @ApiModelProperty("收获地址Id")
    private Long addressId;


    @ApiModelProperty("商品名称")
    private String productName;

    @ApiModelProperty("")
    private String status;


}
