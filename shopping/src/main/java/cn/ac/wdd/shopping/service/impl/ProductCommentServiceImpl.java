package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.ProductComment;
import cn.ac.wdd.shopping.mapper.ProductCommentMapper;
import cn.ac.wdd.shopping.service.IProductCommentService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-13
 */
@Service
public class ProductCommentServiceImpl extends ServiceImpl<ProductCommentMapper, ProductComment> implements IProductCommentService {

}
