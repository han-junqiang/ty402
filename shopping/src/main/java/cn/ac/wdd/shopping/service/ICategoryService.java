package cn.ac.wdd.shopping.service;

import cn.ac.wdd.shopping.entity.Category;
import cn.ac.wdd.shopping.protocol.request.AttributeRequest;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
public interface ICategoryService extends IService<Category> {

    Map<String, List<Category>> getAllCategory();

    void saveAttribute(AttributeRequest attributeRequest);

    void updateAttribute(AttributeRequest attributeRequest);
}
