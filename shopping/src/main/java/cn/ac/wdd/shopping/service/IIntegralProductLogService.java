package cn.ac.wdd.shopping.service;

import cn.ac.wdd.shopping.entity.IntegralProductLog;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
public interface IIntegralProductLogService extends IService<IntegralProductLog> {

    List<IntegralProductLog> getList();
}
