package cn.ac.wdd.shopping.service;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.shopping.entity.AdvertisingBoard;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
public interface IAdvertisingBoardService extends IService<AdvertisingBoard> {

    Page<AdvertisingBoard> listByPage(Page<AdvertisingBoard> page);
}
