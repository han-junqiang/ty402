package cn.ac.wdd.shopping.entity;

import cn.ac.wdd.shopping.vo.MarketingVo;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Getter
@Setter
@TableName("t_merchant")
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "Merchant对象", description = "")
public class Merchant implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("手机号吗")
    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("店铺名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("店铺头像")
    @TableField("merchant_avatar")
    private String merchantAvatar;

    @ApiModelProperty("店铺星级")
    @TableField("merchant_grade")
    private Integer merchantGrade;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("商家所在城市")
    @TableField("city")
    private String city;

    @ApiModelProperty("1待审核   2已通过 -1 拒绝")
    @TableField("status")
    private Integer status;

    @ApiModelProperty("手机号码	")
    @TableField("phone")
    private String phone;

    @ApiModelProperty("店主真实姓名")
    @TableField("faith_name")
    private String faithName;

    @ApiModelProperty("商家所在详细地址")
    @TableField("address_detail")
    private String addressDetail;

    @ApiModelProperty("商家商品")
    @TableField(exist = false)
    private List<MarketingVo> marketingVos;

    public static final String ID = "id";

    public static final String USER_ID = "user_id";

    public static final String NAME = "name";

    public static final String MERCHANT_AVATAR = "merchant_avatar";

    public static final String MERCHANT_GRADE = "merchant_grade";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String CITY = "city";

    public static final String STATUS = "status";

    public static final String PHONE = "phone";

    public static final String FAITH_NAME = "faith_name";

    public static final String ADDRESS_DETAIL = "address_detail";

}
