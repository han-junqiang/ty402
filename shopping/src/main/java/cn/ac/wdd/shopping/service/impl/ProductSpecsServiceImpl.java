package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.ProductSpecs;
import cn.ac.wdd.shopping.mapper.ProductSpecsMapper;
import cn.ac.wdd.shopping.service.IProductSpecsService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
@Service
public class ProductSpecsServiceImpl extends ServiceImpl<ProductSpecsMapper, ProductSpecs> implements IProductSpecsService {

}
