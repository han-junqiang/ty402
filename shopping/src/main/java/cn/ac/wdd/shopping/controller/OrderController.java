package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.Category;
import cn.ac.wdd.shopping.entity.Order;
import cn.ac.wdd.shopping.entity.OrderDetail;
import cn.ac.wdd.shopping.protocol.request.OrderRequest;
import cn.ac.wdd.shopping.service.IOrderService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
@RestController
@RequestMapping("/order")
public class OrderController {

    @Autowired
    private IOrderService orderService;

    @PostMapping("/add")
    @ApiOperation("下单")
    public ResultMessage<List<Order>> addOrder(@RequestBody List<OrderRequest> orderRequestList){
        return R.ok(orderService.addOrder(orderRequestList));
    }

    @GetMapping("/update")
    @ApiOperation("根据订单状态查询订单")
    public ResultMessage<List<Order>> selectOrderByStatus(String orderStatus){
        List<Order> orderList = orderService.selectOrderByStatus(orderStatus);
        return R.ok(orderList);
    }


    @PostMapping("/list")
    @ApiOperation("根据订单状态查询订单(分页)")
    public ResultMessage<Page<Order>> selectOrderByStatus(@RequestBody Page<Order> page){
        page = orderService.selectOrderByPage(page);
        return R.ok(page);
    }
    @ApiOperation("根据id获取订单信息")
    @GetMapping("")
    public ResultMessage<Order> getEntityById(Long id){
        Order entity = orderService.getById(id);
        return R.ok(entity);
    }

    @PostMapping("/paid")
    @ApiOperation("完成支付")
    public ResultMessage<Boolean> orderPaid(@RequestBody Order order){
        orderService.orderPaid(order);
        return R.ok(true);
    }

    @PostMapping("/update/status")
    @ApiOperation("更新订单状态")
    public ResultMessage<Boolean> updateOrderStatus(@RequestBody Order order){
        boolean update = orderService.updateById(order);
        return R.ok(update);
    }
}
