package cn.ac.wdd.shopping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-13
 */
@Getter
@Setter
@Builder
@TableName("t_attribute_key")
@ApiModel(value = "AttributeKey对象", description = "")
public class AttributeKey implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("种类id")
    @TableField("category_id")
    private Long categoryId;

    @ApiModelProperty("规格key")
    @TableField("attribute_name")
    private String attributeName;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("可选项")
    @TableField("value")
    private String value;


    public static final String ID = "id";

    public static final String CATEGORY_ID = "category_id";

    public static final String ATTRIBUTE_NAME = "attribute_name";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String VALUE = "value";

}
