package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.StringUtil;
import cn.ac.wdd.shopping.entity.AdvertisingBoard;
import cn.ac.wdd.shopping.entity.AdvertisingBoardType;
import cn.ac.wdd.shopping.entity.AdvertisingBoard;
import cn.ac.wdd.shopping.entity.Merchant;
import cn.ac.wdd.shopping.mapper.AdvertisingBoardMapper;
import cn.ac.wdd.shopping.mapper.AdvertisingBoardTypeMapper;
import cn.ac.wdd.shopping.service.IAdvertisingBoardService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
@Service
public class AdvertisingBoardServiceImpl extends ServiceImpl<AdvertisingBoardMapper, AdvertisingBoard> implements IAdvertisingBoardService {
    @Autowired
    private AdvertisingBoardTypeMapper advertisingBoardTypeMapper;

    @Autowired
    private AdvertisingBoardMapper advertisingBoardMapper;
    
    @Override
    public AdvertisingBoard getById(Serializable id) {
        AdvertisingBoard advertisingBoard = super.getById(id);
        AdvertisingBoardType advertisingBoardType = advertisingBoardTypeMapper.selectById(advertisingBoard.getAdvertisingBoardTypeId());
        advertisingBoard.setAdvertisingBoardType(advertisingBoardType.getName());
        return advertisingBoard;
    }

    @Override
    public Page<AdvertisingBoard> listByPage(Page<AdvertisingBoard> page) {
        
        QueryWrapper<AdvertisingBoard> merchantQueryWrapper = new QueryWrapper<>();
        if (page.getParams().get("name") != null && StringUtil.isNotEmpty(page.getParams().get("name").toString())){
            merchantQueryWrapper.like(AdvertisingBoard.NAME,page.getParams().get("name").toString());
        }
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<AdvertisingBoard> merchatPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>();
        merchatPage.setSize(page.getPageSize());
        merchatPage.setCurrent(page.getCurrentPage());
        merchatPage = advertisingBoardMapper.selectPage(merchatPage, merchantQueryWrapper);
        page.setTotalCount((int)merchatPage.getTotal());
        page.setCurrentPage((int)merchatPage.getCurrent());
        List<AdvertisingBoard> merchants = advertisingBoardMapper.selectList(merchantQueryWrapper);
        page.setTotalCount(merchants.size());
        List<AdvertisingBoard> records = merchatPage.getRecords();

        records.forEach(p->{
            AdvertisingBoardType advertisingBoardType = advertisingBoardTypeMapper.selectById(p.getAdvertisingBoardTypeId());
            p.setAdvertisingBoardType(advertisingBoardType.getName());
        });
        page.setList(records);
        return page;
    }
}
