package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.MarketEffectLog;
import cn.ac.wdd.shopping.entity.Product;
import cn.ac.wdd.shopping.entity.ShoppingTrolley;
import cn.ac.wdd.shopping.mapper.ProductMapper;
import cn.ac.wdd.shopping.mapper.ProductSpecsMapper;
import cn.ac.wdd.shopping.mapper.ShoppingTrolleyMapper;
import cn.ac.wdd.shopping.service.IMarketEffectLogService;
import cn.ac.wdd.shopping.service.IShoppingTrolleyService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
@Service
public class ShoppingTrolleyServiceImpl extends ServiceImpl<ShoppingTrolleyMapper, ShoppingTrolley> implements IShoppingTrolleyService {

    @Autowired
    private ShoppingTrolleyMapper shoppingTrolleyMapper;

    @Autowired
    private  ProductSpecsMapper productSpecsMapper;

    @Autowired
    private ProductMapper productMapper;

    @Autowired
    private IMarketEffectLogService marketEffectLogService;

    @Override
    public List<ShoppingTrolley> getTorlleyList() {
        Long userId = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<ShoppingTrolley> shoppingTrolleyQueryWrapper = new QueryWrapper<>();
        shoppingTrolleyQueryWrapper.eq(ShoppingTrolley.USER_ID, userId);
        List<ShoppingTrolley> shoppingTrolleys = shoppingTrolleyMapper.selectList(shoppingTrolleyQueryWrapper);
        return change(shoppingTrolleys);
    }

    @Override
    public List<ShoppingTrolley> addTorlleyshoppingTrolley(ShoppingTrolley shoppingTrolley) {
        Long userId = SecurityUtils.getCurrUserIdChecked();
        shoppingTrolley.setUserId(userId);
        shoppingTrolleyMapper.insert(shoppingTrolley);
        QueryWrapper<ShoppingTrolley> shoppingTrolleyQueryWrapper = new QueryWrapper<>();
        shoppingTrolleyQueryWrapper.eq(ShoppingTrolley.USER_ID, userId);
        List<ShoppingTrolley> shoppingTrolleys = shoppingTrolleyMapper.selectList(shoppingTrolleyQueryWrapper);

        marketEffectLogService.saveLog(MarketEffectLog.builder()
                .eventType("add_shopping_trolley")
                .userId(userId)
                .productId(shoppingTrolley.getPrudoctId())
                .marketRegisterId(shoppingTrolley.getPrudoctCource())
                .build());
        return change(shoppingTrolleys);
    }

    @Override
    public List<ShoppingTrolley> deleteTorlleyById(ShoppingTrolley shoppingTrolley) {
        shoppingTrolleyMapper.deleteById(shoppingTrolley.getId());
        Long userId = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<ShoppingTrolley> shoppingTrolleyQueryWrapper = new QueryWrapper<>();
        shoppingTrolleyQueryWrapper.eq(ShoppingTrolley.USER_ID, userId);
        List<ShoppingTrolley> shoppingTrolleys = shoppingTrolleyMapper.selectList(shoppingTrolleyQueryWrapper);
        return change(shoppingTrolleys);
    }

    @Override
    public List<ShoppingTrolley> updateTorlleyById(ShoppingTrolley shoppingTrolley) {
        shoppingTrolleyMapper.updateById(shoppingTrolley);

        Long userId = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<ShoppingTrolley> shoppingTrolleyQueryWrapper = new QueryWrapper<>();
        shoppingTrolleyQueryWrapper.eq(ShoppingTrolley.USER_ID, userId);
        List<ShoppingTrolley> shoppingTrolleys = shoppingTrolleyMapper.selectList(shoppingTrolleyQueryWrapper);
        return change(shoppingTrolleys);
    }

    public  List<ShoppingTrolley> change(List<ShoppingTrolley> shoppingTrolleys){
        shoppingTrolleys.forEach(p->{
            Product product = productMapper.selectById(p.getPrudoctId());
            product.setDetailPhotoList(JSON.parseArray(product.getDetailPhoto(),String.class));
            p.setProdoctDetail(product);
            p.setProdoctSpecs(productSpecsMapper.selectById(p.getProdoctSpecsId()));
        });
        return shoppingTrolleys;
    }

}
