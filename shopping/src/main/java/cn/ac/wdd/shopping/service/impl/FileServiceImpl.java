package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.Product;
import cn.ac.wdd.shopping.mapper.ProductMapper;
import cn.ac.wdd.shopping.service.IFileService;
import cn.ac.wdd.shopping.service.IProductService;
import cn.ac.wdd.shopping.util.FileUtil;
import cn.ac.wdd.shopping.util.TencentOSSClient;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-08
 */
@Service
public class FileServiceImpl implements IFileService {


    @Autowired
    private TencentOSSClient tencentOSSClient;

    @Override
    public String upload(MultipartFile file) {
        String fileUrl = FileUtil.uploadFile(file);
        String fileName = fileUrl.substring(fileUrl.lastIndexOf("/"));
        tencentOSSClient.uploadFile(fileUrl,"/shopping/" + fileName);
        return tencentOSSClient.getFullPathOfResource("/shopping/" + fileName);
    }

}
