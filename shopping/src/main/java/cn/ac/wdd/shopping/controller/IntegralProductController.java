package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.AdvertisingBoard;
import cn.ac.wdd.shopping.entity.IntegralProduct;
import cn.ac.wdd.shopping.service.IIntegralProductService;
import com.alibaba.fastjson.JSON;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@RestController
@RequestMapping("/integral/product")
public class IntegralProductController {

    @Autowired
    private IIntegralProductService integralProductService;

    @GetMapping("/list")
    @ApiOperation("获取积分商品列表")
    public ResultMessage<List<IntegralProduct>> getIntegralProductList(){
        List<IntegralProduct> list = integralProductService.list();
        return R.ok(list);
    }

    @PostMapping("/list/page")
    @ApiOperation("获取积分商品列表（分页）")
    public ResultMessage<Page<IntegralProduct>> getIntegralProductListByPage(@RequestBody  Page<IntegralProduct>  page){
        page = integralProductService.getIntegralProductListByPage(page);
        return R.ok(page);
    }

    @PostMapping("/add")
    @ApiOperation("添加积分商品")
    public ResultMessage<Boolean> addIntegralProductList(@RequestBody IntegralProduct integralProduct){

        List<String> detailPhotoList = integralProduct.getDetailPhotoList();
        String detailPhoto = JSON.toJSONString(detailPhotoList);
        integralProduct.setDetailPhoto(detailPhoto);
        boolean save = integralProductService.save(integralProduct);
        return R.ok(save);
    }
    @ApiOperation("根据Id删除积分商品")
    @PostMapping("/delete")
    public ResultMessage<Boolean> removeList(@RequestBody IntegralProduct integralProduct){
        Boolean remove = integralProductService.removeById(integralProduct.getId());
        return R.ok(remove);
    }

    @ApiOperation("根据Id更新积分商品")
    @PostMapping("/update")
    public ResultMessage<Boolean> updateList(@RequestBody IntegralProduct advertisingBoard){
        Boolean remove = integralProductService.updateById(advertisingBoard);
        return R.ok(remove);
    }

    @ApiOperation("根据Id获取积分商品")
    @GetMapping("")
    public ResultMessage<IntegralProduct> getList(Long id){
        IntegralProduct integralProduct= integralProductService.getById(id);
        return R.ok(integralProduct);
    }
}
