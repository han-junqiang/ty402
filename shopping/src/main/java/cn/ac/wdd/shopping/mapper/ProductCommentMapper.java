package cn.ac.wdd.shopping.mapper;

import cn.ac.wdd.shopping.entity.ProductComment;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-13
 */
public interface ProductCommentMapper extends BaseMapper<ProductComment> {

}
