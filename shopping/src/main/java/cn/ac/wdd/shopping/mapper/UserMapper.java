package cn.ac.wdd.shopping.mapper;

import cn.ac.wdd.shopping.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
public interface UserMapper extends BaseMapper<User> {

}
