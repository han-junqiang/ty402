package cn.ac.wdd.shopping.controller;

import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.service.IFileService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

/**
 * @createTime 2022年05月09日 19:02:00
 */
@RestController
@RequestMapping("/file")
@Api(value = "文件服务")
public class FileController {

    @Autowired
    private IFileService fileService;

    @PostMapping("/upload")
    @ApiOperation(value = "文件上传")
    private ResultMessage<String> uploadFile(MultipartFile file){
        String fileUrl = fileService.upload(file);
        return R.ok(fileUrl);
    }
}
