package cn.ac.wdd.shopping.mapper;

import cn.ac.wdd.shopping.entity.Merchant;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
public interface MerchantMapper extends BaseMapper<Merchant> {
}
