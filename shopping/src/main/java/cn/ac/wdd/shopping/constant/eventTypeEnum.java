package cn.ac.wdd.shopping.constant;

import cn.ac.wdd.common.util.IEnum;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.experimental.Accessors;

@Getter
@Accessors(fluent = true)
@AllArgsConstructor
public enum eventTypeEnum implements IEnum {

  BANNER_SHOW("banner_show", "banner显示"),
  GOODS_INFORMATION_STREAM("goods_information_stream", "信息流"),
  TOP_SERACH("top_serach", "热门搜索"),
  POP_UP("pop_up", "弹框广告"),
  CHOICENESS_PRODUCT("choiceness_product", "精选商品"),

  ;
  private String code;
  private String desc;
}
