package cn.ac.wdd.shopping.service;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.shopping.entity.Merchant;
import cn.ac.wdd.shopping.entity.Product;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
public interface IProductService extends IService<Product> {
    public List<Product> list(Long categoryId);

    List<Product> productSearch(String productSearchRequest);

    Merchant getMerchantProducts(Long merchantId);

    List<Product> getMerchantProductsCurrUser();

    Page<Product> getMerchantProductsCurrUserByPage(Page<Product> page);

    Boolean updateProduct(Product product);

    Boolean updateProductsStatus(Product product);
}
