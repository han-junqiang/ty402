package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.StringUtil;
import cn.ac.wdd.shopping.entity.IntegralProduct;
import cn.ac.wdd.shopping.mapper.IntegralProductMapper;
import cn.ac.wdd.shopping.service.IIntegralProductService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Service
public class IntegralProductServiceImpl extends ServiceImpl<IntegralProductMapper, IntegralProduct> implements IIntegralProductService {

    @Autowired
    private IntegralProductMapper integralProductMapper;
    @Override
    public Page<IntegralProduct> getIntegralProductListByPage(Page<IntegralProduct> page) {

        QueryWrapper<IntegralProduct> integralQueryWrapper = new QueryWrapper<>();
        if (page.getParams().get("name") != null && StringUtil.isNotEmpty(page.getParams().get("name").toString())){
            integralQueryWrapper.like(IntegralProduct.NAME,page.getParams().get("name").toString());
        }
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<IntegralProduct> integralPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>();
        integralPage.setSize(page.getPageSize());
        integralPage.setCurrent(page.getCurrentPage());
        integralPage = integralProductMapper.selectPage(integralPage, integralQueryWrapper);
        page.setTotalCount((int)integralPage.getTotal());
        page.setCurrentPage((int)integralPage.getCurrent());
        List<IntegralProduct> merchants = integralProductMapper.selectList(integralQueryWrapper);
        page.setTotalCount(merchants.size());
        List<IntegralProduct> records = integralPage.getRecords();
        page.setList(records);
        return page;
    }
}
