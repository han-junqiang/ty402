package cn.ac.wdd.shopping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
@Getter
@Setter
@TableName("t_advertising_board")
@ApiModel(value = "AdvertisingBoard对象", description = "")
public class AdvertisingBoard implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("广告版面类型id")
    @TableField("advertising_board_type_id")
    private Long advertisingBoardTypeId;


    @ApiModelProperty("广告版面类型")
    @TableField(exist = false)
    private String advertisingBoardType;


    @ApiModelProperty("png、jpg、mp3、mp4")
    @TableField("support_media_type")
    private String supportMediaType;

    @ApiModelProperty("最低价格")
    @TableField("min_price")
    private BigDecimal minPrice;

    @ApiModelProperty("版面高度")
    @TableField("height")
    private Integer height;

    @ApiModelProperty("版面宽度")
    @TableField("width")
    private Integer width;

    @ApiModelProperty("支持文件大小")
    @TableField("size")
    private String size;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("广告位名称")
    @TableField("name")
    private String name;


    public static final String ID = "id";

    public static final String ADVERTISING_BOARD_TYPE_ID = "advertising_board_type_id";

    public static final String SUPPORT_MEDIA_TYPE = "support_media_type";

    public static final String MIN_PRICE = "min_price";

    public static final String HEIGHT = "height";

    public static final String WIDTH = "width";

    public static final String SIZE = "size";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String NAME = "name";

}
