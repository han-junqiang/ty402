package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.*;
import cn.ac.wdd.shopping.mapper.MarketRegisterMapper;
import cn.ac.wdd.shopping.mapper.ProductMapper;
import cn.ac.wdd.shopping.mapper.ProductSpecsMapper;
import cn.ac.wdd.shopping.mapper.UserMapper;
import cn.ac.wdd.shopping.service.IMarketEffectLogService;
import cn.ac.wdd.shopping.service.MarketingService;
import cn.ac.wdd.shopping.util.RandomValuesUtils;
import cn.ac.wdd.shopping.util.SecurityUtils;
import cn.ac.wdd.shopping.vo.MarketingVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import static cn.ac.wdd.shopping.constant.eventTypeEnum.*;

/**
 * @createTime 2022年05月08日 15:50:00
 */
@Service
public class MarketingServiceImpl implements MarketingService {

    @Autowired
    private MarketRegisterMapper marketRegisterMapper;

    @Autowired
    private ProductMapper productMapperp;

    @Autowired
    private ProductSpecsMapper productSpecsMapper;

    @Autowired
    private IMarketEffectLogService marketEffectLogService;

    @Autowired
    private UserMapper userMapper;

    @Override
    public List<MarketingVo> bannerList() {
        User user = SecurityUtils.getCurrUserChecked();
        List<MarketRegister> marketRegisterList = marketRegisterMapper.selectBannerByUser(user, 1);
        marketRegisterList = RandomValuesUtils.randomValues(marketRegisterList, 5);
        if (marketRegisterList != null && marketRegisterList.size() >0 ){
            MarketRegister marketRegister = marketRegisterList.get(0);
            marketEffectLogService.save(MarketEffectLog.builder().userId(user.getId()).productId(marketRegister.getProductId()).marketRegisterId(marketRegister.getId()).eventType(BANNER_SHOW.code()).build());
        }
        return marketToMarketingVo(marketRegisterList);
    }

    @Override
    public List<MarketingVo> productListBywechat() {
        List<Product> products =  productMapperp.selectRand();
        products = RandomValuesUtils.randomValues(products, 9);
        List<MarketingVo> marketingVoList = productsToMarketingVo(products);
        User user = SecurityUtils.getCurrUserChecked();
        List<MarketRegister> marketRegisterList = marketRegisterMapper.selectBannerByUser(user,2);
        List<MarketingVo> marketingVos = new ArrayList<>();
        if (marketRegisterList != null && marketRegisterList.size() >0){
            marketRegisterList = RandomValuesUtils.randomValues(marketRegisterList, 1);
            MarketRegister marketRegister = marketRegisterList.get(0);
            marketRegister.setTotalShow(marketRegister.getTotalShow()+1);
            marketRegister.setTotalPrice(marketRegister.getTotalPrice().add(marketRegister.getOnceBudget()));
            marketRegisterMapper.updateById(marketRegister);
            marketingVos = marketToMarketingVo(marketRegisterList);
            marketEffectLogService.save(MarketEffectLog.builder().userId(user.getId()).productId(marketRegister.getProductId()).marketRegisterId(marketRegister.getId()).eventType(GOODS_INFORMATION_STREAM.code()).build());
        }
        marketingVos.addAll(marketingVoList);
        return marketingVos;
    }

    @Override
    public List<MarketingVo> topSearch() {
        User user = SecurityUtils.getCurrUserChecked();
        List<MarketRegister> marketRegisterList = marketRegisterMapper.selectBannerByUser(user, 3);
        marketRegisterList = RandomValuesUtils.randomValues(marketRegisterList, 10);
        for (MarketRegister marketRegister : marketRegisterList) {
            marketEffectLogService.save(MarketEffectLog.builder().userId(user.getId()).productId(marketRegister.getProductId()).marketRegisterId(marketRegister.getId()).eventType(TOP_SERACH.code()).build());
        }
        return marketToMarketingVo(marketRegisterList);
    }

    @Override
    public List<MarketingVo> choicenessProduct() {
        User user = SecurityUtils.getCurrUserChecked();
        List<MarketRegister> marketRegisterList = marketRegisterMapper.selectBannerByUser(user, 4);
        marketRegisterList = RandomValuesUtils.randomValues(marketRegisterList, 4);
        for (MarketRegister marketRegister : marketRegisterList) {
            marketEffectLogService.save(MarketEffectLog.builder().userId(user.getId()).productId(marketRegister.getProductId()).marketRegisterId(marketRegister.getId()).eventType(CHOICENESS_PRODUCT.code()).build());
        }
        return marketToMarketingVo(marketRegisterList);
    }

    @Override
    public MarketingVo popUpProduct() {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        User user = userMapper.selectById(userIdChecked);
        List<MarketRegister> marketRegisterList = marketRegisterMapper.selectBannerByUser(user, 5);
        marketRegisterList = RandomValuesUtils.randomValues(marketRegisterList, 1);
        if (marketRegisterList!=null && marketRegisterList.size() > 0){
            marketEffectLogService.save(MarketEffectLog.builder().userId(user.getId()).productId(marketRegisterList.get(0).getProductId()).marketRegisterId(marketRegisterList.get(0).getId()).eventType(POP_UP.code()).build());
            List<MarketingVo> marketingVos = marketToMarketingVo(marketRegisterList);
            marketingVos.get(0).setIsMarketing(true);
            return marketingVos.get(0);
        }
        return MarketingVo.builder().isMarketing(false).build();
    }

    private List<MarketingVo> marketToMarketingVo(List<MarketRegister> marketRegisterList){
        ArrayList<MarketingVo> marketingVos = new ArrayList<>();
        marketRegisterList.stream().forEach(p->{
            QueryWrapper<ProductSpecs> productSpecsQueryWrapper = new QueryWrapper<>();
            productSpecsQueryWrapper.eq(ProductSpecs.PRODUCT_ID,p.getProductId());
            productSpecsQueryWrapper.orderByAsc(ProductSpecs.SALE_PRICE);
            productSpecsQueryWrapper.last("limit 1");
            ProductSpecs productSpecs = productSpecsMapper.selectOne(productSpecsQueryWrapper);
            marketingVos.add(MarketingVo.builder().id(p.getId()).advertisingBoardId(p.getAdvertisingBoardId()).productId(p.getProductId()).url(p.getUrl()).name(p.getName()).isMarketing(true)
                            .salePrice(productSpecs.getSalePrice())
                    .detail(p.getDetail()).show(true).build());
        });
        return marketingVos;
    }

    private List<MarketingVo> productsToMarketingVo(List<Product> productsList){
        ArrayList<MarketingVo> marketingVos = new ArrayList<>();
        productsList.stream().forEach(p->{
            QueryWrapper<ProductSpecs> productSpecsQueryWrapper = new QueryWrapper<>();
            productSpecsQueryWrapper.eq(ProductSpecs.PRODUCT_ID,p.getId());
            productSpecsQueryWrapper.orderByAsc(ProductSpecs.SALE_PRICE);
            productSpecsQueryWrapper.last("limit 1");
            ProductSpecs productSpecs = productSpecsMapper.selectOne(productSpecsQueryWrapper);
            marketingVos.add(MarketingVo.builder().id(p.getId()).productId(p.getId()).name(p.getName()).isMarketing(false)
                    .detail(p.getDetail()).salePrice(productSpecs.getSalePrice()).show(true).build());
        });
        return marketingVos;
    }
}
