package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.MarketEffectLog;
import cn.ac.wdd.shopping.entity.MarketRegister;
import cn.ac.wdd.shopping.entity.Product;
import cn.ac.wdd.shopping.mapper.MarketEffectLogMapper;
import cn.ac.wdd.shopping.mapper.MarketRegisterMapper;
import cn.ac.wdd.shopping.mapper.ProductMapper;
import cn.ac.wdd.shopping.protocol.request.MarketLogRequest;
import cn.ac.wdd.shopping.service.IMarketEffectLogService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import static cn.ac.wdd.shopping.constant.eventTypeEnum.BANNER_SHOW;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author
 * @since 2022-05-09
 */
@Service
public class MarketEffectLogServiceImpl extends ServiceImpl<MarketEffectLogMapper, MarketEffectLog> implements IMarketEffectLogService {

    @Autowired
    private MarketRegisterMapper marketRegisterMapper;
    @Override
    public boolean save(MarketEffectLog entity) {
        entity.setUserId(SecurityUtils.getCurrUserIdChecked());
        MarketRegister marketRegister = marketRegisterMapper.selectById(entity.getMarketRegisterId());
        marketRegister.setTotalShow(marketRegister.getTotalShow()+1);
        marketRegister.setTotalPrice(marketRegister.getTotalPrice().add(marketRegister.getOnceBudget()));
        if(marketRegister.getTotalPrice().compareTo(marketRegister.getTotalBudget()) >=0 || marketRegister.getTotalShow() >= marketRegister.getShowBudget()){
            marketRegister.setStatus("4");
        }
        marketRegisterMapper.updateById(marketRegister);
        return super.save(entity);
    }

    @Override
    public boolean saveLog(MarketEffectLog entity) {
        entity.setUserId(SecurityUtils.getCurrUserIdChecked());
        if (BANNER_SHOW.code().equals(entity.getEventType())){
            MarketRegister marketRegister = marketRegisterMapper.selectById(entity.getMarketRegisterId());
            marketRegister.setTotalShow(marketRegister.getTotalShow()+1);
            marketRegister.setTotalPrice(marketRegister.getTotalPrice().add(marketRegister.getOnceBudget()));
            if(marketRegister.getTotalPrice().compareTo(marketRegister.getTotalBudget()) >=0 || marketRegister.getTotalShow() >= marketRegister.getShowBudget()){
                marketRegister.setStatus("4");
            }
            marketRegisterMapper.updateById(marketRegister);
        }
        return super.save(entity);
    }
}
