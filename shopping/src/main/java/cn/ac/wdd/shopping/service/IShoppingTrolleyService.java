package cn.ac.wdd.shopping.service;

import cn.ac.wdd.shopping.entity.ShoppingTrolley;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
public interface IShoppingTrolleyService extends IService<ShoppingTrolley> {

    List<ShoppingTrolley> getTorlleyList();

    List<ShoppingTrolley> addTorlleyshoppingTrolley(ShoppingTrolley shoppingTrolley);

    List<ShoppingTrolley> deleteTorlleyById(ShoppingTrolley shoppingTrolley);

    List<ShoppingTrolley> updateTorlleyById(ShoppingTrolley shoppingTrolley);
}
