package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.StringUtil;
import cn.ac.wdd.shopping.entity.*;
import cn.ac.wdd.shopping.mapper.CategoryMapper;
import cn.ac.wdd.shopping.mapper.MerchantMapper;
import cn.ac.wdd.shopping.mapper.ProductMapper;
import cn.ac.wdd.shopping.mapper.ProductSpecsMapper;
import cn.ac.wdd.shopping.service.ICategoryService;
import cn.ac.wdd.shopping.service.IProductService;
import cn.ac.wdd.shopping.util.RandomValuesUtils;
import cn.ac.wdd.shopping.util.SecurityUtils;
import cn.ac.wdd.shopping.vo.MarketingVo;
import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcloud.cos.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
@Service
public class ProductServiceImpl extends ServiceImpl<ProductMapper, Product> implements IProductService {

    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductSpecsMapper productSpecsMapper;

    @Autowired
    private MerchantMapper merchantMapper;

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private ICategoryService categoryService;

    public List<Product> list(Long categoryId) {
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.CATEGORY_ID,categoryId);
        List<Product> products = productMapper.selectList(productQueryWrapper);
        products.stream().forEach(p ->{
            QueryWrapper<ProductSpecs> productSpecsQueryWrapper = new QueryWrapper<>();
            productSpecsQueryWrapper.eq(ProductSpecs.PRODUCT_ID,p.getId());
            productSpecsQueryWrapper.orderByAsc(ProductSpecs.SALE_PRICE);
            productSpecsQueryWrapper.last("limit 1");
            ProductSpecs productSpecs = productSpecsMapper.selectOne(productSpecsQueryWrapper);
            p.setDetailPhotoList(JSON.parseArray(p.getDetailPhoto(),String.class));
            p.setSalePrice(productSpecs.getSalePrice());
        });
        return products;
    }

    @Override
    public List<Product> productSearch(String productSearchRequest) {
        QueryWrapper<Category> categoryQueryWrapper = new QueryWrapper<>();
        categoryQueryWrapper.like(Category.NAME, productSearchRequest);
        List<Category> categories = categoryMapper.selectList(categoryQueryWrapper);
        List<Long> categoryIds = categories.stream().map(Category::getId).collect(Collectors.toList());
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        if (categoryIds != null && categoryIds.size() >0){
            productQueryWrapper.in(Product.CATEGORY_ID,categoryIds);
        }
        productQueryWrapper.or().like(Product.NAME,productSearchRequest);
        List<Product> products = productMapper.selectList(productQueryWrapper);
        return RandomValuesUtils.randomValues(products, 10);
    }

    @Override
    public Merchant getMerchantProducts(Long merchantId) {
        Merchant merchant = merchantMapper.selectById(merchantId);
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.MERCHANT_ID,merchantId);
        List<Product> products = productMapper.selectList(productQueryWrapper);
        List<MarketingVo> marketingVos = productsToMarketingVo(products);
        merchant.setMarketingVos(marketingVos);
        return merchant;
    }

    @Override
    public List<Product> getMerchantProductsCurrUser() {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID,userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);

        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.MERCHANT_ID,merchant.getId());
        List<Product> products = productMapper.selectList(productQueryWrapper);

        products.stream().forEach(p->{

            QueryWrapper<ProductSpecs> productSpecsQueryWrapper = new QueryWrapper<>();
            productSpecsQueryWrapper.eq(ProductSpecs.PRODUCT_ID,p.getId());
            List<ProductSpecs> productSpecs = productSpecsMapper.selectList(productSpecsQueryWrapper);
            p.setSpecsList(productSpecs);


        });
        return products;
    }

    @Override
    public Page<Product> getMerchantProductsCurrUserByPage(Page<Product> page) {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID,userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        if (page.getParams().get("name") != null && StringUtil.isNotEmpty(page.getParams().get("name").toString())){
            productQueryWrapper.like(Product.NAME,page.getParams().get("name").toString());
        }
        productQueryWrapper.eq(Product.MERCHANT_ID,merchant.getId());
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Product> productPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>();
        productPage.setSize(page.getPageSize());
        productPage.setCurrent(page.getCurrentPage());
        productPage = productMapper.selectPage(productPage, productQueryWrapper);
        page.setTotalCount((int)productPage.getTotal());
        page.setCurrentPage((int)productPage.getCurrent());
        List<Product> products = productMapper.selectList(productQueryWrapper);
        page.setTotalCount(products.size());
        List<Product> records = productPage.getRecords();
        records.forEach(p->{
            QueryWrapper<ProductSpecs> productSpecsQueryWrapper = new QueryWrapper<>();
            productSpecsQueryWrapper.eq(ProductSpecs.PRODUCT_ID,p.getId());
            List<ProductSpecs> productSpecs = productSpecsMapper.selectList(productSpecsQueryWrapper);
            p.setSpecsList(productSpecs);
            p.setCategoryIdList(new ArrayList<String>());

            Category category = categoryMapper.selectById(p.getCategoryId());
            p.setCategoryName(category.getName());
        });
        page.setList(records);
        return  page;
    }

    @Override
    public Boolean updateProduct(Product product) {
        product.setDetailPhoto(JSON.toJSONString(product.getDetailPhotoList()));
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        product.setMerchantId(merchant.getId());
        super.updateById(product);
        List<ProductSpecs> specsList = product.getSpecsList();
        QueryWrapper<ProductSpecs> productSpecsQueryWrapper = new QueryWrapper<>();
        productSpecsQueryWrapper.eq(ProductSpecs.PRODUCT_ID,product.getId());
        productSpecsMapper.delete(productSpecsQueryWrapper);
        specsList.forEach(p->{
            p.setProductId(product.getId());
            productSpecsMapper.insert(p);
        });
        return true;
    }

    @Override
    public Boolean updateProductsStatus(Product product) {
        productMapper.updateById(product);
        return true;
    }

    private void leafWay(List<Category> tree, ArrayList<String> temp, List<String> leafWayList, Long currentId){
        for (int i = 0; i < tree.size(); i++) {
            Category p = tree.get(i);
            temp.add(p.getId() + "");
            if (p.getId() == currentId){
                leafWayList.addAll(temp);
                return;
            }
            List<Category> childList = p.getChildList();
            if (!CollectionUtils.isNullOrEmpty(childList)){
                leafWay(childList,temp,leafWayList,currentId);
            }
            temp.remove(temp.size()-1);
        }
    }
    public Product getById(Serializable id) {
        Product product = productMapper.selectId(id);
        Category category = categoryMapper.selectById(product.getCategoryId());
        product.setCategoryName(category.getName());

        Map<String, List<Category>> allCategory = categoryService.getAllCategory();
        List<Category> treeRootList = allCategory.get("tree");

        ArrayList<String> wayIdList = new ArrayList<>();
        if (!CollectionUtils.isNullOrEmpty(treeRootList)){
            leafWay(treeRootList,new ArrayList<String >(),wayIdList,product.getCategoryId());
        }
        product.setCategoryIdList(wayIdList);
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.ID, product.getMerchantId());
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        product.setMerchantAddress(merchant.getCity());
        product.setDetailPhotoList(JSON.parseArray(product.getDetailPhoto(),String.class));
        QueryWrapper<ProductSpecs> productSpecsQueryWrapper = new QueryWrapper<>();
        productSpecsQueryWrapper.eq(ProductSpecs.PRODUCT_ID,product.getId());
        List<ProductSpecs> productSpecs = productSpecsMapper.selectList(productSpecsQueryWrapper);
        product.setSpecsList(productSpecs);
        return product;
    }


    private List<MarketingVo> productsToMarketingVo(List<Product> productsList){
        ArrayList<MarketingVo> marketingVos = new ArrayList<>();
        productsList.stream().forEach(p->{
            QueryWrapper<ProductSpecs> productSpecsQueryWrapper = new QueryWrapper<>();
            productSpecsQueryWrapper.eq(ProductSpecs.PRODUCT_ID,p.getId());
            productSpecsQueryWrapper.orderByAsc(ProductSpecs.SALE_PRICE);
            productSpecsQueryWrapper.last("limit 1");
            ProductSpecs productSpecs = productSpecsMapper.selectOne(productSpecsQueryWrapper);
            marketingVos.add(MarketingVo.builder().id(p.getId()).productId(p.getId()).name(p.getName()).isMarketing(false)
                    .detail(p.getDetail()).salePrice(productSpecs.getSalePrice()).url(p.getDetailPhotoList().get(0)).show(true).build());
        });
        return marketingVos;
    }

    @Override
    public boolean save(Product entity) {
        entity.setDetailPhoto(JSON.toJSONString(entity.getDetailPhotoList()));
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, userIdChecked);
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
        entity.setMerchantId(merchant.getId());
        super.save(entity);
        List<ProductSpecs> specsList = entity.getSpecsList();
        specsList.forEach(p->{
            p.setProductId(entity.getId());
            productSpecsMapper.insert(p);
        });
        return true;
    }
}
