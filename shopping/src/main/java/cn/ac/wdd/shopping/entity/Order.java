package cn.ac.wdd.shopping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;

import com.sun.javaws.jnl.PropertyDesc;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Getter
@Setter
@TableName("t_order")
@ApiModel(value = "Order对象", description = "")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Order implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("订单号 雪花算法")
    @TableField("order_number")
    private String orderNumber;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("商家id")
    @TableField("merchant_id")
    private Long merchantId;

    @ApiModelProperty("订单状态：待支付unpaid、已支付paid cancel已取消  收货：receive 评价：evaluate 完成：closure" )
    @TableField("order_state")
    private String orderState;

    @ApiModelProperty("支付类型")
    @TableField("pay_type")
    private String payType;

    @ApiModelProperty("商品名称")
    @TableField(exist = false)
    private String productName;

    @ApiModelProperty("支付完成时间")
    @TableField("pay_finished_time")
    private LocalDateTime payFinishedTime;

    @ApiModelProperty("支付取消时间")
    @TableField("pay_canceled_time")
    private LocalDateTime payCanceledTime;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("订单来源")
    @TableField("order_cource")
    private String orderCource;

    @ApiModelProperty("备注")
    @TableField("comment")
    private String comment;


    @ApiModelProperty("收货地址")
    @TableField("address_id")
    private Long addressId;

    @ApiModelProperty("收货地址")
    @TableField(exist = false)
    private String addressCity;

    @ApiModelProperty("收货地址")
    @TableField(exist = false)
    private String addressDetail;

    @ApiModelProperty("联系人")
    @TableField(exist = false)
    private String userName;

    @ApiModelProperty("联系方式")
    @TableField(exist = false)
    private String userPhone;

    @ApiModelProperty("备注")
    @TableField(exist = false)
    private OrderDetail orderDetail;

    @ApiModelProperty("商品信息")
    @TableField(exist = false)
    private Product product;

    @ApiModelProperty("商品规格")
    @TableField(exist = false)
    private ProductSpecs productSpecs;

    public static final String ID = "id";

    public static final String ORDER_NUMBER = "order_number";

    public static final String USER_ID = "user_id";

    public static final String MERCHANT_ID = "merchant_id";

    public static final String ADDRESS_ID = "address_id";

    public static final String ORDER_STATE = "order_state";

    public static final String PAY_TYPE = "pay_type";

    public static final String PAY_FINISHED_TIME = "pay_finished_time";

    public static final String PAY_CANCELED_TIME = "pay_canceled_time";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String ORDER_COURCE = "order_cource";

    public static final String COMMENT = "comment";

}
