package cn.ac.wdd.shopping.mapper;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.shopping.entity.Order;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
public interface OrderMapper extends BaseMapper<Order> {

    List<Order> selectOrderByPage(Page<Order> page);

    Integer selectOrderByPageCount(Page<Order> page);
}
