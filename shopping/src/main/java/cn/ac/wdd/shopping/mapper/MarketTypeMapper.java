package cn.ac.wdd.shopping.mapper;

import cn.ac.wdd.shopping.entity.MarketType;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
public interface MarketTypeMapper extends BaseMapper<MarketType> {

}
