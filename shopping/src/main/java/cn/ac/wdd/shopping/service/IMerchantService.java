package cn.ac.wdd.shopping.service;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.shopping.entity.Merchant;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
public interface IMerchantService extends IService<Merchant> {

    Merchant getMerchantProducts(Long merchantId);

    Merchant getByUserId();

    Page<Merchant> getListByPage(Page<Merchant> page);
}
