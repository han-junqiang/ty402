package cn.ac.wdd.shopping.mapper;

import cn.ac.wdd.shopping.entity.MarketRegister;
import cn.ac.wdd.shopping.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import io.lettuce.core.dynamic.annotation.Param;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-08
 */
public interface MarketRegisterMapper extends BaseMapper<MarketRegister> {

    List<MarketRegister> selectBannerByUser(@Param("user") User user, @Param("boardTypeId") Integer boardTypeId);
}
