package cn.ac.wdd.shopping.service;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.shopping.entity.IntegralProduct;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
public interface IIntegralProductService extends IService<IntegralProduct> {
    Page<IntegralProduct> getIntegralProductListByPage(Page<IntegralProduct> page);
}
