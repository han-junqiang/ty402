package cn.ac.wdd.shopping.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.qcloud.cos.utils.CollectionUtils;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
@Getter
@Setter
@TableName("t_product")
@ApiModel(value = "Product对象", description = "")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Product implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("商品名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("发货地址")
    @TableField("name")
    private String merchantAddress;

    @ApiModelProperty("种类id")
    @TableField("category_id")
    private Long categoryId;

    @ApiModelProperty("种类id,List")
    @TableField(exist = false)
    private List<String> categoryIdList;

    @ApiModelProperty("种类名称")
    @TableField(exist = false)
    private String categoryName;

    @ApiModelProperty("商家id")
    @TableField("merchant_id")
    private Long merchantId;

    @ApiModelProperty("副标题")
    @TableField("subtitle")
    private String subtitle;

    @ApiModelProperty("商品图片数组")
    @TableField("detail_photo")
    private String detailPhoto;

    @TableField(exist = false)
    private List<String> detailPhotoList;

    @ApiModelProperty("商品状态（up：上架  down:下架）")
    @TableField("detail_video")
    private String detailVideo;

    @ApiModelProperty("商品详情")
    @TableField("detail")
    private String detail;

    @TableField(exist = false)
    private BigDecimal salePrice;

    @ApiModelProperty("商品参数")
    @TableField("attribute_list")
    private String attributeList;

    @TableField(exist = false)
    private List<ProductSpecs> specsList;

    @ApiModelProperty("商品星级")
    @TableField("goods_grade")
    private Integer goodsGrade;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("用户评论")
    @TableField(exist = false)
    private String comment;

    public void setDetailPhoto(String detailPhoto) {
        this.detailPhotoList = JSON.parseArray(detailPhoto,String.class);
        this.detailPhoto = detailPhoto;
    }

    public void setCategoryIdList(List<String> categoryIdList) {
        if (CollectionUtils.isNullOrEmpty(categoryIdList)){
            this.categoryIdList = new ArrayList<String>();
        }else {
            this.categoryIdList = categoryIdList;
        }
    }

    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String CATEGORY_ID = "category_id";

    public static final String MERCHANT_ID = "merchant_id";

    public static final String SUBTITLE = "subtitle";

    public static final String DETAIL_PHOTO = "detail_photo";

    public static final String DETAIL_VIDEO = "detail_video";

    public static final String DETAIL = "detail";

    public static final String ATTRIBUTE_LIST = "attribute_list";

    public static final String GOODS_GRADE = "goods_grade";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

}
