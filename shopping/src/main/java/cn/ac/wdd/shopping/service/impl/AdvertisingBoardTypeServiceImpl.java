package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.StringUtil;
import cn.ac.wdd.shopping.entity.AdvertisingBoardType;
import cn.ac.wdd.shopping.entity.Merchant;
import cn.ac.wdd.shopping.mapper.AdvertisingBoardTypeMapper;
import cn.ac.wdd.shopping.service.IAdvertisingBoardTypeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
@Service
public class AdvertisingBoardTypeServiceImpl extends ServiceImpl<AdvertisingBoardTypeMapper, AdvertisingBoardType> implements IAdvertisingBoardTypeService {

    @Autowired
    private AdvertisingBoardTypeMapper advertisingBoardTypeMapper;

    @Override
    public Page<AdvertisingBoardType> listByPage(Page<AdvertisingBoardType> page) {

        QueryWrapper<AdvertisingBoardType> merchantQueryWrapper = new QueryWrapper<>();
        if (page.getParams().get("name") != null && StringUtil.isNotEmpty(page.getParams().get("name").toString())){
            merchantQueryWrapper.like(Merchant.NAME,page.getParams().get("name").toString());
        }
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<AdvertisingBoardType> merchatPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>();
        merchatPage.setSize(page.getPageSize());
        merchatPage.setCurrent(page.getCurrentPage());
        merchatPage = advertisingBoardTypeMapper.selectPage(merchatPage, merchantQueryWrapper);
        page.setTotalCount((int)merchatPage.getTotal());
        page.setCurrentPage((int)merchatPage.getCurrent());
        List<AdvertisingBoardType> merchants = advertisingBoardTypeMapper.selectList(merchantQueryWrapper);
        page.setTotalCount(merchants.size());
        List<AdvertisingBoardType> records = merchatPage.getRecords();
        page.setList(records);
        return page;
    }
}
