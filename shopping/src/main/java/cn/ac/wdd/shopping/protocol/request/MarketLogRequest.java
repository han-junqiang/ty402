package cn.ac.wdd.shopping.protocol.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.web.bind.annotation.ResponseBody;


/**
 * @createTime 2022年05月10日 17:15:00
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class MarketLogRequest {

    private Long id;

    @ApiModelProperty("广告商品id")
    private Long productId;

    @ApiModelProperty("广告版面id")
    private Long advertisingBoardId;

    @ApiModelProperty("eventType")
    private String eventType;

}
