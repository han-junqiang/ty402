package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.User;
import cn.ac.wdd.shopping.service.IUserService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserService userService;

    @PostMapping("/update")
    @ApiOperation("更新用户信息")
    public ResultMessage<Boolean> update(@RequestBody User user){
//        SecurityUtils.getCurrUserIdChecked()
        boolean update = userService.updateById(user);
        return R.ok(update);

    }

    @GetMapping("/")
    @ApiOperation("用户信息")
    public ResultMessage<User> getUserInfo(){
        Long userId = SecurityUtils.getCurrUserIdChecked();
        User user = userService.getById(userId);
        return R.ok(user);

    }

}
