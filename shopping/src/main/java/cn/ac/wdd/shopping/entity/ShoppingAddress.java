package cn.ac.wdd.shopping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Getter
@Setter
@TableName("t_shopping_address")
@ApiModel(value = "ShoppingAddress对象", description = "")
public class ShoppingAddress implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("城市")
    @TableField("city")
    private String city;

    @ApiModelProperty("详细地址")
    @TableField("detail")
    private String detail;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("默认地址")
    @TableField("default_address")
    private Boolean defaultAddress;

    @ApiModelProperty("电话")
    @TableField("phone")
    private String phone;

    @ApiModelProperty("姓名")
    @TableField("user_name")
    private String userName;


    public static final String ID = "id";

    public static final String USER_ID = "user_id";

    public static final String CITY = "city";

    public static final String DETAIL = "detail";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String DEFAULT_ADDRESS = "default_address";

    public static final String PHONE = "phone";

    public static final String USER_NAME = "user_name";

}
