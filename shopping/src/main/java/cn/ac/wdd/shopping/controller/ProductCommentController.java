package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.Order;
import cn.ac.wdd.shopping.entity.Product;
import cn.ac.wdd.shopping.entity.ProductComment;
import cn.ac.wdd.shopping.entity.User;
import cn.ac.wdd.shopping.service.IOrderService;
import cn.ac.wdd.shopping.service.IProductCommentService;
import cn.ac.wdd.shopping.service.IProductService;
import cn.ac.wdd.shopping.service.IUserService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-13
 */
@RestController
@RequestMapping("/product/comment")
public class ProductCommentController {

    @Autowired
    private IProductCommentService productCommentService;
    @Autowired
    private IUserService userService;

    @Autowired
    private IProductService productService;

    @Autowired
    private IOrderService orderService;

    @PostMapping("/add")
    public ResultMessage<Boolean> add(@RequestBody  ProductComment productComment){
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        productComment.setUserId(userIdChecked);
        Order order = Order.builder().id(productComment.getOrderId()).orderState("closure").build();
        orderService.updateById(order);
        boolean save = productCommentService.save(productComment);
        return R.ok(save);
    }

    @GetMapping("/")
    @ApiOperation("根据产品id获取产品评价")
    public ResultMessage<List<ProductComment>> getCommentList(Long productId){
        QueryWrapper<ProductComment> productCommentQueryWrapper = new QueryWrapper<>();
        productCommentQueryWrapper.eq(ProductComment.PRODUCT_ID,productId);
        List<ProductComment> list = productCommentService.list(productCommentQueryWrapper);
        list.stream().forEach(p->{
            User byId = userService.getById(p.getUserId());
            p.setAvatar(byId.getAvatar());
            p.setNickName(byId.getNickName());
        });
        return R.ok(list);
    }

    @GetMapping("/products")
    @ApiOperation("获取评价历史")
    public ResultMessage<ArrayList<Product>> getProducts(){
        QueryWrapper<ProductComment> productCommentQueryWrapper = new QueryWrapper<>();
        productCommentQueryWrapper.eq(ProductComment.USER_ID,SecurityUtils.getCurrUserIdChecked());
        List<ProductComment> list = productCommentService.list(productCommentQueryWrapper);
        ArrayList<Product> products = new ArrayList<>();

        list.forEach(p->{
            Product byId = productService.getById(p.getProductId());
            byId.setComment(p.getCommentDetail());
            products.add(byId);
        });
        return R.ok(products);
    }
}
