package cn.ac.wdd.shopping.entity;

import com.alibaba.fastjson.JSON;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Getter
@Setter
@TableName("t_integral_product")
@ApiModel(value = "IntegralProduct对象", description = "")
public class IntegralProduct implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("商品名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("副标题")
    @TableField("subtitle")
    private String subtitle;

    @ApiModelProperty("商品图片数组")
    @TableField("detail_photo")
    private String detailPhoto;

    @ApiModelProperty("商品图片数组")
    @TableField(exist = false)
    private List<String> detailPhotoList;

    @ApiModelProperty("商品状态（up：上架  down:下架）")
    @TableField("detail_video")
    private String detailVideo;

    @ApiModelProperty("商品详情")
    @TableField("detail")
    private String detail;

    @ApiModelProperty("商品参数")
    @TableField("attribute_list")
    private String attributeList;

    @ApiModelProperty("商品星级")
    @TableField("goods_grade")
    private Integer goodsGrade;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("商品积分价格")
    @TableField("price")
    private Long price;

    public void setDetailPhoto(String detailPhoto) {
        this.detailPhotoList = JSON.parseArray(detailPhoto,String.class);
        this.detailPhoto = detailPhoto;
    }

    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String SUBTITLE = "subtitle";

    public static final String DETAIL_PHOTO = "detail_photo";

    public static final String DETAIL_VIDEO = "detail_video";

    public static final String DETAIL = "detail";

    public static final String ATTRIBUTE_LIST = "attribute_list";

    public static final String GOODS_GRADE = "goods_grade";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String PRICE = "price";

}
