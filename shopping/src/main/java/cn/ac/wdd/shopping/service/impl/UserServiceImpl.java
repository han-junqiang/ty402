package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.User;
import cn.ac.wdd.shopping.mapper.UserMapper;
import cn.ac.wdd.shopping.service.IUserService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {

    @Override
    public boolean updateById(User entity) {
        entity.setId(SecurityUtils.getCurrUserIdChecked());
        return super.updateById(entity);
    }
}
