package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.StringUtil;
import cn.ac.wdd.shopping.entity.Merchant;
import cn.ac.wdd.shopping.entity.User;
import cn.ac.wdd.shopping.mapper.MerchantMapper;
import cn.ac.wdd.shopping.mapper.UserMapper;
import cn.ac.wdd.shopping.service.IMerchantService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Service
public class MerchantServiceImpl extends ServiceImpl<MerchantMapper, Merchant> implements IMerchantService {

    @Autowired
    private MerchantMapper merchantMapper;
    @Autowired
    private UserMapper userMapper;

    @Override
    public boolean save(Merchant entity) {

        entity.setUserId(  SecurityUtils.getCurrUserIdChecked());
        User user = userMapper.selectById(SecurityUtils.getCurrUserIdChecked());
        user.setPhoneNunmber(entity.getPhone());
        userMapper.updateById(user);
        return super.save(entity);
    }

    @Override
    public Merchant getMerchantProducts(Long merchantId) {

        return null;
    }

    @Override
    public Merchant getByUserId() {
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        merchantQueryWrapper.eq(Merchant.USER_ID, SecurityUtils.getCurrUserIdChecked());
        Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);

        return merchant;
    }

    @Override
    public Page<Merchant> getListByPage(Page<Merchant> page) {
        QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
        if (page.getParams().get("name") != null && StringUtil.isNotEmpty(page.getParams().get("name").toString())){
            merchantQueryWrapper.like(Merchant.NAME,page.getParams().get("name").toString());
        }
        if (page.getParams().get("status") != null  && StringUtil.isNotEmpty(page.getParams().get("status").toString())){
            merchantQueryWrapper.or().like(Merchant.STATUS,Integer.valueOf(page.getParams().get("status").toString()));
        }
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<Merchant> merchatPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>();
        merchatPage.setSize(page.getPageSize());
        merchatPage.setCurrent(page.getCurrentPage());
        merchatPage = merchantMapper.selectPage(merchatPage, merchantQueryWrapper);

        page.setTotalCount((int)merchatPage.getTotal());
        page.setCurrentPage((int)merchatPage.getCurrent());
        List<Merchant> merchants = merchantMapper.selectList(merchantQueryWrapper);
        page.setTotalCount(merchants.size());

        List<Merchant> records = merchatPage.getRecords();
        records.forEach(p->{
            p.setMarketingVos(new ArrayList<>());
        });
        page.setList(records);
        return page;
    }
}
