package cn.ac.wdd.shopping.protocol.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @createTime 2022年05月13日 17:01:00
 */
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class AttributeRequest {

    @ApiModelProperty("规格id，查询or删除时使用")
    private Long id;

    @ApiModelProperty("分类Id")
    private Long categoryId;

    @ApiModelProperty("规格名称")
    private String attributeKey;

    @ApiModelProperty("可选项")
    private String attributeValue;
}
