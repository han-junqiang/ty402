package cn.ac.wdd.shopping.config;

import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;

/**
 * @author yang date 2020/4/13
 **/
@ControllerAdvice
@Slf4j
@ApiIgnore
public class CommonControllerAdvice {

  @ResponseBody
  @ExceptionHandler({Exception.class})
  public ResultMessage<String> authException(Exception ex) {
    log.error("EXCEPTION -> {}", ex.toString(), ex);
    return R.errWithData(ex.getMessage());
  }

}
