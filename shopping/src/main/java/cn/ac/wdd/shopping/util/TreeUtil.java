package cn.ac.wdd.shopping.util;


import cn.ac.wdd.shopping.entity.Category;

import java.util.ArrayList;
import java.util.List;

/**
 * @createTime 2022年05月07日 17:05:00
 */
public class TreeUtil {

    public static List<Category> findMenuRoot(List<Category> categoryVoList){
        List<Category> rootCategory = new ArrayList<>();
        for(Category category : categoryVoList){
            if( category.getParentId() == null || category.getParentId() == 0){
                rootCategory.add(category);
            }
        }
        return rootCategory;
    }

    public static  List<Category> findCategoryChild(Long id, List<Category> categoryVoList){
        List<Category> childList = new ArrayList<Category>();
        for(Category menuVoChild : categoryVoList){
            if(menuVoChild.getParentId().equals(id)){
                childList.add(menuVoChild);
            }
        }
        //递归
        for (Category nav : childList) {
            nav.setChildList(findCategoryChild(nav.getId(), categoryVoList));
        }
        //如果节点下没有子节点，返回一个空List（递归退出）
        if(childList.size() == 0){
            return new ArrayList<Category>();
        }
        return childList;

    }
}
