package cn.ac.wdd.shopping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
@Getter
@Setter
@TableName("t_advertising_board_type")
@ApiModel(value = "AdvertisingBoardType对象", description = "")
public class AdvertisingBoardType implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("广告版面类型名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;


    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

}
