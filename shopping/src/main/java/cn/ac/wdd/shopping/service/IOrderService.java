package cn.ac.wdd.shopping.service;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.shopping.entity.Order;
import cn.ac.wdd.shopping.protocol.request.OrderRequest;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
public interface IOrderService extends IService<Order> {

    public List<Order> addOrder(List<OrderRequest> orderRequestList);

    List<Order> selectOrderByStatus(String orderStauts);

    void orderPaid(Order order);

    Page<Order> selectOrderByPage(Page<Order> page);
}
