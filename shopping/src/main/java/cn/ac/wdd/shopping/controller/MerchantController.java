package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.Merchant;
import cn.ac.wdd.shopping.service.IMerchantService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@RestController
@RequestMapping("/merchant")
public class MerchantController {

    @Autowired
    private IMerchantService merchantService;

    @PostMapping("/list")
    @ApiOperation("获取商家列表")
    public ResultMessage<Page<Merchant>> getListByPage(@RequestBody Page<Merchant> page){
        return R.ok(merchantService.getListByPage(page));
    }

    @PostMapping("/add")
    @ApiOperation("申请成为商家")
    public ResultMessage<Boolean> save(@RequestBody Merchant merchant){
        boolean save = merchantService.save(merchant);
        return R.ok(save);
    }

    @PostMapping("/update")
    @ApiOperation("更新商家信息")
    public ResultMessage<Boolean> update(@RequestBody Merchant merchant){
        boolean update = merchantService.updateById(merchant);
        return R.ok(update);
    }

    @GetMapping("/")
    @ApiOperation("获取商家信息")
    public ResultMessage<Merchant> getMerchant(){
        Merchant merchant = merchantService.getByUserId();
        return R.ok(merchant);
    }

    @GetMapping("/id")
    @ApiOperation("根据id获取商家信息")
    public ResultMessage<Merchant> getMerchantById(Long id){
        Merchant merchant = merchantService.getById(id);
        return R.ok(merchant);
    }

    @PostMapping("/delet")
    @ApiOperation("删除商家信息")
    public ResultMessage<Boolean> delete(@RequestBody Merchant merchant){
        boolean delete = merchantService.removeById(merchant.getId());
        return R.ok(delete);
    }
}
