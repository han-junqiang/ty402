package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.MarketRegister;
import cn.ac.wdd.shopping.entity.MarketRegister;
import cn.ac.wdd.shopping.service.IMarketRegisterService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
@RestController
@RequestMapping("/market/register")
public class MarketRegisterController {

    @Autowired
    private IMarketRegisterService marketRegisterService;

    @ApiOperation("新建广告投放")
    @PostMapping("/add")
    public ResultMessage<Boolean> save(@RequestBody MarketRegister marketRegister){
        boolean save = marketRegisterService.save(marketRegister);
        return R.ok(save);
    }

    @ApiOperation("获取广告投放列表")
    @PostMapping("/list")
    public ResultMessage<Page<MarketRegister>> getList(@RequestBody Page<MarketRegister> page){
        Page<MarketRegister> advertisingBoardPage = marketRegisterService.listByPage(page);
        return R.ok(advertisingBoardPage);
    }

    @ApiOperation("根据Id获取广告投放")
    @GetMapping("")
    public ResultMessage<MarketRegister> getList(Long id){
        MarketRegister marketRegister= marketRegisterService.getById(id);
        return R.ok(marketRegister);
    }

    @ApiOperation("根据Id删除广告投放")
    @PostMapping("/delete")
    public ResultMessage<Boolean> removeList(@RequestBody  MarketRegister marketRegister){
        Boolean remove = marketRegisterService.removeById(marketRegister.getId());
        return R.ok(remove);
    }

    @ApiOperation("根据Id更新广告投放")
    @PostMapping("/update")
    public ResultMessage<Boolean> updateList(@RequestBody MarketRegister marketRegister){
        Boolean remove = marketRegisterService.updateById(marketRegister);
        return R.ok(remove);
    }

}
