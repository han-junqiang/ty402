package cn.ac.wdd.shopping.service;

import cn.ac.wdd.shopping.entity.ProductSpecs;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
public interface IProductSpecsService extends IService<ProductSpecs> {

}
