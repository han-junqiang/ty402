package cn.ac.wdd.shopping.config;


import cn.ac.wdd.common.util.StringUtil;
import cn.ac.wdd.common.util.TokenUtil;
import cn.ac.wdd.shopping.entity.User;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;


@Component
@Slf4j
public class ShoppingAuthInterceptor implements HandlerInterceptor {

    @Autowired
    private RedisTemplate<String, User> redisUserInfoTemplate;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) {
        // 获取请求头信息authorization信息
        final String authHeader = request.getHeader(TokenUtil.AUTH_HEADER_KEY);
        if (TokenUtil.getTokenFromHeader(authHeader).isPresent()){
            User userInfo = redisUserInfoTemplate.opsForValue().get(TokenUtil.REDIS_TOKEN_PREFIX + TokenUtil.getTokenFromHeader(authHeader).get());
            if (userInfo != null) {
                request.setAttribute(TokenUtil.USER_INFO_ATTRIBUTE_NAME, userInfo);
            }
        }
        return true;
    }
}
