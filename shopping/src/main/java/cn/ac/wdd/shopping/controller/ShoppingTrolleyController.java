package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.ShoppingTrolley;
import cn.ac.wdd.shopping.service.IShoppingTrolleyService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
@RestController
@RequestMapping("/trolley")
public class ShoppingTrolleyController {

    @Autowired
    private IShoppingTrolleyService shoppingTrolleyService;

    @GetMapping("/list")
    @ApiOperation("购物车列表")
    public ResultMessage<List<ShoppingTrolley>> getTorlleyList(){
        List<ShoppingTrolley> shoppingTrolleyList = shoppingTrolleyService.getTorlleyList();
        return R.ok(shoppingTrolleyList);
    }


    @PostMapping("/add")
    @ApiOperation("添加购物车")
    public ResultMessage<List<ShoppingTrolley>> addTorlley(@RequestBody ShoppingTrolley shoppingTrolley){
        List<ShoppingTrolley> shoppingTrolleyList = shoppingTrolleyService.addTorlleyshoppingTrolley(shoppingTrolley);
        return R.ok(shoppingTrolleyList);
    }

    @PostMapping("/delete")
    @ApiOperation("删除购物车商品")
    public ResultMessage<List<ShoppingTrolley>> deleteTorlleyById(@RequestBody ShoppingTrolley shoppingTrolley){
        List<ShoppingTrolley> shoppingTrolleyList = shoppingTrolleyService.deleteTorlleyById(shoppingTrolley);
        return R.ok(shoppingTrolleyList);
    }

    @PostMapping("/update")
    @ApiOperation("更新购物车商品数量")
    public ResultMessage<List<ShoppingTrolley>> updateTorlleyById(@RequestBody ShoppingTrolley shoppingTrolley){
        List<ShoppingTrolley> shoppingTrolleyList = shoppingTrolleyService.updateTorlleyById(shoppingTrolley);
        return R.ok(shoppingTrolleyList);
    }
}
