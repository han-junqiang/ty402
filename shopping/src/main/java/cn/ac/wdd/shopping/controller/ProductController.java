package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.Merchant;
import cn.ac.wdd.shopping.entity.Product;
import cn.ac.wdd.shopping.service.IMerchantService;
import cn.ac.wdd.shopping.service.IProductService;
import com.alibaba.fastjson.JSONArray;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
@RestController
@RequestMapping("/product")
public class ProductController {

    @Autowired
    private IProductService productService;
    @Autowired
    private IMerchantService merchantService;
    @ApiOperation("根据分类id获取商品")
    @GetMapping("/category")
    public ResultMessage<List<Product>> getProductByCategoryId(Long categoryId){
        List<Product> products = productService.list(categoryId);
        return R.ok(products);
    }

    @ApiOperation("根据id获取商品信息")
    @GetMapping("")
    public ResultMessage<Product> getProductById(Long productId){
        Product product = productService.getById(productId);
        return R.ok(product);
    }

    @ApiOperation("根据id更新商品信息")
    @PostMapping("/update")
    public ResultMessage<Boolean> updateProducts(@RequestBody Product product){
        Boolean update = productService.updateProduct(product);
        return R.ok(update);
    }
    @ApiOperation("根据id上架下架产品")
    @PostMapping("/update/status")
    public ResultMessage<Boolean> updateProductsStatus(@RequestBody Product product){
        Boolean update = productService.updateProductsStatus(product);
        return R.ok(update);
    }
    @ApiOperation("根据id删除产品")
    @PostMapping("/delete")
    public ResultMessage<Boolean> deleteProducts(@RequestBody Product product){
        Boolean remove = productService.removeById(product.getId());
        return R.ok(remove);
    }
    @ApiOperation("搜索框搜索商品")
    @GetMapping("/search")
    public ResultMessage<List<Product>> productSearch(String productSearchRequest){
        List<Product> products = productService.productSearch(productSearchRequest);
        return R.ok(products);
    }

    @ApiOperation("根据商家id获取商家信息及商品信息")
    @GetMapping("/merchant")
    public ResultMessage<Merchant> getMerchantProducts(Long merchantId){
        Merchant merchant = productService.getMerchantProducts(merchantId);
        return R.ok(merchant);
    }

    @ApiOperation("获取当前用户的产品列表")
    @GetMapping("/merchant/curruser")
    public ResultMessage<List<Product>> getMerchantProductsCurrUser(){
        List<Product> products = productService.getMerchantProductsCurrUser();
        return R.ok(products);
    }


    @ApiOperation("获取当前用户的产品列表通过分页")
    @PostMapping("/merchant/curruser/byPage")
    public ResultMessage<Page<Product>> getMerchantProductsCurrUserByPage(@RequestBody Page<Product> page){
        page = productService.getMerchantProductsCurrUserByPage(page);
        return R.ok(page);
    }


    @ApiOperation("添加商品")
    @PostMapping("/add")
    public ResultMessage<Boolean> saveProducts(@RequestBody Product product){
        boolean save = productService.save(product);
        return R.ok(save);
    }
}
