package cn.ac.wdd.shopping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-13
 */
@Getter
@Setter
@TableName("t_product_comment")
@ApiModel(value = "ProductComment对象", description = "")
public class ProductComment implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("商品id")
    @TableField("product_id")
    private String productId;

    @ApiModelProperty("orderId")
    @TableField(exist = false)
    private Long orderId;

    @ApiModelProperty("评价内容")
    @TableField("comment_detail")
    private String commentDetail;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("用户头像")
    @TableField(exist = false)
    private String avatar;

    @ApiModelProperty("用户昵称")
    @TableField(exist = false)
    private String nickName;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;


    public static final String ID = "id";

    public static final String USER_ID = "user_id";

    public static final String PRODUCT_ID = "product_id";

    public static final String COMMENT_DETAIL = "comment_detail";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

}
