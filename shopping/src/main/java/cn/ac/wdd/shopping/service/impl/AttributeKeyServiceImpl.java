package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.AttributeKey;
import cn.ac.wdd.shopping.mapper.AttributeKeyMapper;
import cn.ac.wdd.shopping.service.IAttributeKeyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-13
 */
@Service
public class AttributeKeyServiceImpl extends ServiceImpl<AttributeKeyMapper, AttributeKey> implements IAttributeKeyService {

}
