package cn.ac.wdd.shopping.service;

import cn.ac.wdd.shopping.entity.AttributeKey;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-13
 */
public interface IAttributeKeyService extends IService<AttributeKey> {

}
