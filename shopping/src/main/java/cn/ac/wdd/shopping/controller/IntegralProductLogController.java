package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.IntegralProductLog;
import cn.ac.wdd.shopping.service.IIntegralProductLogService;
import cn.ac.wdd.shopping.service.IUserService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@RestController
@RequestMapping("/integral/product/log")
public class IntegralProductLogController {

    @Autowired
    private IIntegralProductLogService integralProductLogService;

    @Autowired
    private IUserService userService;

    @ApiOperation("购买积分商品")
    @PostMapping("/add")
    private ResultMessage<Boolean> save(@RequestBody IntegralProductLog integralProductLog){
        integralProductLog.setTotalPrice(integralProductLog.getPrice());
        integralProductLog.setNumber(1);
        boolean save = integralProductLogService.save(integralProductLog);
        return R.ok(save);
    }

    @ApiOperation("我购买积分商品的历史")
    @GetMapping("list")
    private ResultMessage<List<IntegralProductLog>> getIntegralProductLog() {
        List<IntegralProductLog> list = integralProductLogService.getList();
        return R.ok(list);
    }

}
