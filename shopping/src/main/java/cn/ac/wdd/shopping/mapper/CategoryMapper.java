package cn.ac.wdd.shopping.mapper;

import cn.ac.wdd.shopping.entity.Category;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
public interface CategoryMapper extends BaseMapper<Category> {

    List<Category> selectAll();
}
