package cn.ac.wdd.shopping.mapper;

import cn.ac.wdd.shopping.entity.MarketEffectLog;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
public interface MarketEffectLogMapper extends BaseMapper<MarketEffectLog> {

}
