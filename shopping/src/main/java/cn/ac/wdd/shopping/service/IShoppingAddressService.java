package cn.ac.wdd.shopping.service;

import cn.ac.wdd.shopping.entity.ShoppingAddress;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
public interface IShoppingAddressService extends IService<ShoppingAddress> {

    List<ShoppingAddress> selectByUserId();

    void defaultAddress(ShoppingAddress shoppingAddress);
}
