package cn.ac.wdd.shopping.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.math.BigDecimal;

/**
 * @createTime 2022年05月08日 15:05:00
 */
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MarketingVo {

    private static final long serialVersionUID = 1L;

    private Long id;

    @ApiModelProperty("广告商品id")
    private Long productId;

    @ApiModelProperty("广告版面id")
    private Long advertisingBoardId;

    @ApiModelProperty("广告商品名称")
    private String name;

    @ApiModelProperty("广告商品描述")
    private String detail;

    @ApiModelProperty("商品价格")
    private BigDecimal salePrice;

    @ApiModelProperty("广告物料地址")
    private String url;

    @ApiModelProperty("是否显示")
    private Boolean show;


    @ApiModelProperty("是否是推广")
    private Boolean isMarketing;
}
