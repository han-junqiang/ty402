package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.ShoppingAddress;
import cn.ac.wdd.shopping.service.IShoppingAddressService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@RestController
@RequestMapping("/address")
public class ShoppingAddressController {
    @Autowired
    private IShoppingAddressService shoppingAddressService;

    @GetMapping("/List")
    @ApiOperation("获取用户收获地址列表")
    public ResultMessage<List<ShoppingAddress>> getAddress(){
        List<ShoppingAddress> shoppingAddresses = shoppingAddressService.selectByUserId();
        return R.ok(shoppingAddresses);
    }
    @PostMapping("/add")
    @ApiOperation("添加收获地址")
    public ResultMessage<Boolean> saveAddress(@RequestBody ShoppingAddress shoppingAddress){
        boolean save = shoppingAddressService.save(shoppingAddress);
        return R.ok(save);
    }
    @PostMapping("/update")
    @ApiOperation("更新收获地址")
    public ResultMessage<Boolean> updateAddress(@RequestBody ShoppingAddress shoppingAddress){
        boolean update = shoppingAddressService.updateById(shoppingAddress);
        return R.ok(update);
    }
    @PostMapping("/delete")
    @ApiOperation("删除收获地址")
    public ResultMessage<Boolean> deleteAddress(@RequestBody ShoppingAddress shoppingAddress){
        boolean remove = shoppingAddressService.removeById(shoppingAddress.getId());
        return R.ok(remove);
    }

    @GetMapping("")
    @ApiOperation("根据id获取收获地址")
    public ResultMessage<ShoppingAddress> getAddressById(Long id){
        return R.ok(shoppingAddressService.getById(id));
    }

    @PostMapping("/defaultAddress")
    @ApiOperation("设置地址为默认地址，只传入id即可，用body的方式")
    public ResultMessage<Boolean> defaultAddress(@RequestBody ShoppingAddress shoppingAddress){
        shoppingAddressService.defaultAddress(shoppingAddress);
        return R.ok();
    }
}
