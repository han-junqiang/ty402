package cn.ac.wdd.shopping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
@Getter
@Setter
@TableName("t_shopping_trolley")
@ApiModel(value = "ShoppingTrolley对象", description = "")
public class ShoppingTrolley implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("商品id")
    @TableField("prudoct_id")
    private Long prudoctId;

    @ApiModelProperty("商家id")
    @TableField("merchant_id")
    private Long merchantId;

    @ApiModelProperty("商品加入时价格")
    @TableField("accession_time_price")
    private Integer accessionTimePrice;

    @ApiModelProperty("商品数量")
    @TableField("prudoct_number")
    private Integer prudoctNumber;

    @ApiModelProperty("商品来源，目前均指广告id")
    @TableField("prudoct_cource")
    private Long prudoctCource;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("规格id")
    @TableField("prodoct_specs_id")
    private Long prodoctSpecsId;


    @ApiModelProperty("规格详情")
    @TableField(exist = false)
    private ProductSpecs prodoctSpecs;

    @ApiModelProperty("商品详情")
    @TableField(exist = false)
    private Product prodoctDetail;

    public static final String ID = "id";

    public static final String USER_ID = "user_id";

    public static final String PRUDOCT_ID = "prudoct_id";

    public static final String MERCHANT_ID = "merchant_id";

    public static final String ACCESSION_TIME_PRICE = "accession_time_price";

    public static final String PRUDOCT_NUMBER = "prudoct_number";

    public static final String PRUDOCT_COURCE = "prudoct_cource";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String PRODOCT_SPECS_ID = "prodoct_specs_id";

}
