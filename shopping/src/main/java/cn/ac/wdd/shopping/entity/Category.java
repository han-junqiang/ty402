package cn.ac.wdd.shopping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
@Getter
@Setter
@TableName("t_category")
@ApiModel(value = "Category对象", description = "")
public class Category implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("分类名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("排序")
    @TableField("sort")
    private String sort;

    @ApiModelProperty("状态")
    @TableField("status")
    private String status;

    @ApiModelProperty("父分类")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty("创建人")
    @TableField("create_by")
    private Integer createBy;

    @ApiModelProperty("更新人")
    @TableField("update_by")
    private Integer updateBy;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("图片")
    @TableField("url")
    private String url;

    @ApiModelProperty("孩子分类")
    @TableField(exist = false)
    private List<Category> childList;

    @ApiModelProperty("是否有规格")
    @TableField(exist = false)
    private Boolean hasSpecs;



    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String SORT = "sort";

    public static final String STATUS = "status";

    public static final String PARENT_ID = "parent_id";

    public static final String CREATE_BY = "create_by";

    public static final String UPDATE_BY = "update_by";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String URL = "url";

}
