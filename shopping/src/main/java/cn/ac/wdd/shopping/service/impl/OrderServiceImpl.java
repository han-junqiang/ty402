package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.StringUtil;
import cn.ac.wdd.shopping.entity.*;
import cn.ac.wdd.shopping.id.SnowFlake;
import cn.ac.wdd.shopping.mapper.*;
import cn.ac.wdd.shopping.protocol.request.OrderRequest;
import cn.ac.wdd.shopping.service.IMarketEffectLogService;
import cn.ac.wdd.shopping.service.IOrderService;
import cn.ac.wdd.shopping.service.IProductService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
@Service
public class OrderServiceImpl extends ServiceImpl<OrderMapper, Order> implements IOrderService {

    @Autowired
    private SnowFlake snowFlake;

    @Autowired
    private OrderMapper orderMapper;

    @Autowired
    private OrderDetailMapper orderDetailMapper;

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private ProductMapper productMapper;
    @Autowired
    private ProductSpecsMapper  productSpecsMapper;

    @Autowired
    private ShoppingAddressMapper shoppingAddressMapper;

    @Autowired
    private IMarketEffectLogService marketEffectLogService;
    @Override
    public List<Order> addOrder(List<OrderRequest> orderRequestList) {
        long snowFlakeId = snowFlake.nextId();
        Long userId = SecurityUtils.getCurrUserIdChecked();
        orderRequestList.forEach(p->{
            if (p.getMarketRegisterId() != null){
                marketEffectLogService.saveLog(MarketEffectLog.builder()
                                .eventType("create_order")
                                .userId(userId)
                                .productId(p.getProductId())
                                .marketRegisterId(p.getMarketRegisterId())
                        .build());
            }
            Order order = Order.builder()
                    .orderNumber(String.valueOf(snowFlakeId))
                    .userId(userId)
                    .orderState(String.valueOf(p.getMarketRegisterId()))
                    .merchantId(p.getMerchantId())
                    .orderState("unpaid")
                    .payType("wechat")
                    .addressId(p.getAddressId())
                    .comment(p.getComment())
                    .build();
            orderMapper.insert(order);

            OrderDetail orderDetail = OrderDetail.builder()
                    .productSpecsId(p.getProductSpecsId())
                    .orderNumber(String.valueOf(snowFlakeId))
                    .productId(p.getProductId())
                    .productNumber(p.getProductNumber())
                    .oriPrice(p.getBuyAmount())
                    .buyPrice(p.getBuyAmount())
                    .payPrice(p.getBuyAmount())
                    .orderId(order.getId())
                    .buyAmount(p.getBuyAmount().multiply(BigDecimal.valueOf(p.getProductNumber())))
                    .build();
            orderDetailMapper.insert(orderDetail);
        });
        List<Order> orderList = selectOrderByOrderNumber(String.valueOf(snowFlakeId));
        return  addProduct(orderList);
    }

    @Override
    public List<Order> selectOrderByStatus(String orderStauts) {
        Long userId = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.eq(Order.USER_ID,userId);
        if (StringUtil.isNotEmpty(orderStauts)){
            orderQueryWrapper.eq(Order.ORDER_STATE,orderStauts);
        }
        List<Order> orderList = orderMapper.selectList(orderQueryWrapper);
        orderList.forEach(p->{
            QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
            orderDetailQueryWrapper.eq(OrderDetail.ORDER_ID,p.getId());
            OrderDetail orderDetail = orderDetailMapper.selectOne(orderDetailQueryWrapper);
            p.setOrderDetail(orderDetail);
        });
        return addProduct(orderList);
    }

    @Override
    public void orderPaid(Order order) {
        Long userId = SecurityUtils.getCurrUserIdChecked();
        final Integer[] score = {new Integer(0)};
        if (order.getOrderNumber() != null){
            List<Order> orderList = selectOrderByOrderNumber(order.getOrderNumber());
            orderList.forEach(p->{
                p.setOrderState("paid");
                p.setPayFinishedTime(LocalDateTime.now());
                orderMapper.updateById(p);
                OrderDetail orderDetail = p.getOrderDetail();
                orderDetail.setPayAmount(orderDetail.getBuyAmount());
                orderDetail.setRealPayAmount(orderDetail.getBuyAmount());
                orderDetail.setPayFinishedTime(LocalDateTime.now());
                orderDetailMapper.updateById(orderDetail);
                score[0] = score[0] + orderDetail.getRealPayAmount().intValue();

                if (p.getOrderCource() != null){
                    marketEffectLogService.saveLog(MarketEffectLog.builder()
                            .eventType("order_paid")
                            .userId(userId)
                            .productId(orderDetail.getProductId())
                            .marketRegisterId(Long.parseLong(p.getOrderCource()))
                            .build());
                }
            });


        }else if (order.getId() != null){
            Order orderById = orderMapper.selectById(order.getId());
            orderById.setOrderState("paid");
            orderById.setPayFinishedTime(LocalDateTime.now());
            orderMapper.updateById(orderById);
            QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
            orderDetailQueryWrapper.eq(OrderDetail.ORDER_ID, order.getId());
            OrderDetail orderDetail = orderDetailMapper.selectOne(orderDetailQueryWrapper);
            orderDetail.setRealPayAmount(orderDetail.getBuyAmount());
            orderDetail.setPayFinishedTime(LocalDateTime.now());
            orderDetailMapper.updateById(orderDetail);
            score[0] = score[0] + orderDetail.getRealPayAmount().intValue();
            if (orderById.getOrderCource() != null){
                marketEffectLogService.saveLog(MarketEffectLog.builder()
                        .eventType("order_paid")
                        .userId(userId)
                        .productId(orderDetail.getProductId())
                        .marketRegisterId(Long.parseLong(order.getOrderCource()))
                        .build());
            }
        }else{
            throw new RuntimeException("没有找到订单");
        }

        User user = userMapper.selectById(userId);
        user.setScore( score[0] + user.getScore());
        userMapper.updateById(user);
    }

    @Override
    public Page<Order> selectOrderByPage(Page<Order> page) {
        Long userId = SecurityUtils.getCurrUserIdChecked();
        page.getParams().put("userId",userId);
        List<Order> orderList = orderMapper.selectOrderByPage(page);
        orderList.forEach(p->{
            QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
            orderDetailQueryWrapper.eq(OrderDetail.ORDER_ID, p.getId());
            OrderDetail orderDetail = orderDetailMapper.selectOne(orderDetailQueryWrapper);
            p.setOrderDetail(orderDetail);

            ShoppingAddress shoppingAddress = shoppingAddressMapper.selectById(p.getAddressId());
            p.setAddressCity(shoppingAddress.getCity() );
            p.setUserName(shoppingAddress.getUserName() );
            p.setUserPhone(shoppingAddress.getPhone() );
            p.setAddressDetail(shoppingAddress.getDetail() );
        });
        Integer total = orderMapper.selectOrderByPageCount(page);
        page.setTotalCount(total);
        page.setList(orderList);
        return page;
    }

    public List<Order> selectOrderByOrderNumber(String orderNumber) {
        Long userId = SecurityUtils.getCurrUserIdChecked();

        QueryWrapper<Order> orderQueryWrapper = new QueryWrapper<>();
        orderQueryWrapper.eq(Order.ORDER_NUMBER,orderNumber);
        List<Order> orderList = orderMapper.selectList(orderQueryWrapper);

        orderList.forEach(p->{
            QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
            orderDetailQueryWrapper.eq(OrderDetail.ORDER_ID,p.getId());
            OrderDetail orderDetail = orderDetailMapper.selectOne(orderDetailQueryWrapper);
            p.setOrderDetail(orderDetail);
        });
        return orderList;
    }

    private List<Order> addProduct(List<Order> orderList){
        orderList.forEach(p->{
            QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
            orderDetailQueryWrapper.eq(OrderDetail.ORDER_ID,p.getId());
            OrderDetail orderDetail = orderDetailMapper.selectOne(orderDetailQueryWrapper);

            Product product = productMapper.selectId(orderDetail.getProductId());
            ProductSpecs productSpecs = productSpecsMapper.selectById(orderDetail.getProductSpecsId());
            p.setProductSpecs(productSpecs);
            p.setProduct(product);
        });
        return orderList;
    }

    @Override
    public Order getById(Serializable id) {
        Order order = super.getById(id);
        QueryWrapper<OrderDetail> orderDetailQueryWrapper = new QueryWrapper<>();
        orderDetailQueryWrapper.eq(OrderDetail.ORDER_ID, order.getId());
        OrderDetail orderDetail = orderDetailMapper.selectOne(orderDetailQueryWrapper);
        order.setOrderDetail(orderDetail);

        QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
        productQueryWrapper.eq(Product.ID, orderDetail.getProductId());
        Product product = productMapper.selectOne(productQueryWrapper);
        order.setProductName(product.getName());

        ShoppingAddress shoppingAddress = shoppingAddressMapper.selectById(order.getAddressId());
        order.setAddressCity(shoppingAddress.getCity() );
        order.setUserName(shoppingAddress.getUserName() );
        order.setUserPhone(shoppingAddress.getPhone() );
        order.setAddressDetail(shoppingAddress.getDetail() );
        return order;
    }
}
