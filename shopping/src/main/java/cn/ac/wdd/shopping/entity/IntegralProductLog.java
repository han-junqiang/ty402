package cn.ac.wdd.shopping.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Getter
@Setter
@TableName("t_integral_product_log")
@ApiModel(value = "IntegralProductLog对象", description = "")
public class IntegralProductLog implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("商品名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("副标题")
    @TableField("subtitle")
    private String subtitle;

    @ApiModelProperty("商品图片数组")
    @TableField("detail_photo")
    private String detailPhoto;

    @ApiModelProperty("商品视频")
    @TableField("detail_video")
    private String detailVideo;

    @ApiModelProperty("商品详情")
    @TableField("detail")
    private String detail;

    @ApiModelProperty("商品参数")
    @TableField("attribute_list")
    private String attributeList;

    @ApiModelProperty("商品星级")
    @TableField("goods_grade")
    private Integer goodsGrade;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("商品积分价格")
    @TableField("price")
    private Long price;

    @ApiModelProperty("数量")
    @TableField("number")
    private Integer number;

    @ApiModelProperty("总积分")
    @TableField("total_price")
    private Long totalPrice;

    @ApiModelProperty("用户id")
    @TableField("user_id")
    private Long userId;

    @ApiModelProperty("积分商品id")
    @TableField("integral_product_id")
    private Long integralProductId;


    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String SUBTITLE = "subtitle";

    public static final String DETAIL_PHOTO = "detail_photo";

    public static final String DETAIL_VIDEO = "detail_video";

    public static final String DETAIL = "detail";

    public static final String ATTRIBUTE_LIST = "attribute_list";

    public static final String GOODS_GRADE = "goods_grade";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String PRICE = "price";

    public static final String NUMBER = "number";

    public static final String TOTAL_PRICE = "total_price";

    public static final String USER_ID = "user_id";

    public static final String INTEGRAL_PRODUCT_ID = "integral_product_id";

}
