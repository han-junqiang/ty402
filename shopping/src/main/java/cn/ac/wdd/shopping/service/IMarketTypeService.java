package cn.ac.wdd.shopping.service;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.shopping.entity.MarketType;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
public interface IMarketTypeService extends IService<MarketType> {

    Page<MarketType> listByPage(Page<MarketType> page);
}
