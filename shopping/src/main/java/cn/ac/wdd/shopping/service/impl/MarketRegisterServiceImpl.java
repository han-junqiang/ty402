package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.StringUtil;
import cn.ac.wdd.shopping.entity.*;
import cn.ac.wdd.shopping.mapper.*;
import cn.ac.wdd.shopping.service.IMarketRegisterService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcloud.cos.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.Serializable;
import java.util.List;
import java.util.Properties;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
@Service
public class MarketRegisterServiceImpl extends ServiceImpl<MarketRegisterMapper, MarketRegister> implements IMarketRegisterService {

    @Autowired
    private MarketRegisterMapper marketRegisterMapper;
    @Autowired
    private MerchantMapper merchantMapper;
    @Autowired
    private AdvertisingBoardMapper advertisingBoardMapper;
    @Autowired
    private MarketTypeMapper marketTypeMapper;

    @Autowired
    private ProductMapper productMapper;

    @Override
    public Page<MarketRegister> listByPage(Page<MarketRegister> page) {

        QueryWrapper<MarketRegister> marketRegisterQueryWrapper = new QueryWrapper<>();
        if (page.getParams().get("name") != null && StringUtil.isNotEmpty(page.getParams().get("name").toString())){
            marketRegisterQueryWrapper.like(MarketRegister.NAME,page.getParams().get("name").toString());
        }
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        if (page.getParams().get("type") != null && StringUtil.isNotEmpty(page.getParams().get("type").toString()) && "CurrUser".equals(page.getParams().get("type").toString())){
            QueryWrapper<Merchant> merchantQueryWrapper = new QueryWrapper<>();
            merchantQueryWrapper.eq(Merchant.USER_ID,userIdChecked);
            Merchant merchant = merchantMapper.selectOne(merchantQueryWrapper);
            QueryWrapper<Product> productQueryWrapper = new QueryWrapper<>();
            productQueryWrapper.eq(Product.MERCHANT_ID,merchant.getId());
            List<Product> products = productMapper.selectList(productQueryWrapper);
            List<Long> prodictIds = products.stream().map(Product::getId).collect(Collectors.toList());
            if (!CollectionUtils.isNullOrEmpty(prodictIds)){
                marketRegisterQueryWrapper.in(MarketRegister.PRODUCT_ID,prodictIds);
            }else {
                marketRegisterQueryWrapper.isNull(MarketRegister.ID);
            }
        }
        if (page.getParams().get("status") != null && StringUtil.isNotEmpty(page.getParams().get("status").toString())){
            marketRegisterQueryWrapper.eq(MarketRegister.STATUS,page.getParams().get("status").toString());
        }

        com.baomidou.mybatisplus.extension.plugins.pagination.Page<MarketRegister> marketRegisterPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>();
        marketRegisterPage.setSize(page.getPageSize());
        marketRegisterPage.setCurrent(page.getCurrentPage());
        marketRegisterPage = marketRegisterMapper.selectPage(marketRegisterPage, marketRegisterQueryWrapper);
        page.setTotalCount((int)marketRegisterPage.getTotal());
        page.setCurrentPage((int)marketRegisterPage.getCurrent());
        List<MarketRegister> merchants = marketRegisterMapper.selectList(marketRegisterQueryWrapper);
        page.setTotalCount(merchants.size());
        List<MarketRegister> records = marketRegisterPage.getRecords();

        records.forEach(p->{
            AdvertisingBoard advertisingBoard = advertisingBoardMapper.selectById(p.getAdvertisingBoardId());
            MarketType marketType = marketTypeMapper.selectById(p.getMarketTypeId());
            p.setMarketTypeName(marketType.getName());
            p.setAdvertisingBoardName(advertisingBoard.getName());
        });
        page.setList(records);
        return page;
    }


    @Override
    public MarketRegister getById(Serializable id) {
        MarketRegister byId = super.getById(id);
        AdvertisingBoard advertisingBoard = advertisingBoardMapper.selectById(byId.getAdvertisingBoardId());
        MarketType marketType = marketTypeMapper.selectById(byId.getMarketTypeId());
        byId.setMarketTypeName(marketType.getName());
        byId.setAdvertisingBoardName(advertisingBoard.getName());
        return byId;
    }
}
