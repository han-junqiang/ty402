package cn.ac.wdd.shopping.service;

import cn.ac.wdd.shopping.entity.MarketEffectLog;
import com.baomidou.mybatisplus.extension.service.IService;
import org.springframework.web.multipart.MultipartFile;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-09
 */
public interface IFileService {


    String upload(MultipartFile file);
}
