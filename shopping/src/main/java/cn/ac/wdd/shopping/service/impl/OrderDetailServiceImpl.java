package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.OrderDetail;
import cn.ac.wdd.shopping.mapper.OrderDetailMapper;
import cn.ac.wdd.shopping.service.IOrderDetailService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Service
public class OrderDetailServiceImpl extends ServiceImpl<OrderDetailMapper, OrderDetail> implements IOrderDetailService {

}
