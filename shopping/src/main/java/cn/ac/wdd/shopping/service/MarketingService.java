package cn.ac.wdd.shopping.service;

import cn.ac.wdd.shopping.vo.MarketingVo;

import java.util.List;

/**
 * @createTime 2022年05月08日 15:50:00
 */
public interface MarketingService {
    List<MarketingVo> bannerList();

    List<MarketingVo> productListBywechat();

    List<MarketingVo> topSearch();

    List<MarketingVo>  choicenessProduct();

    MarketingVo popUpProduct();
}
