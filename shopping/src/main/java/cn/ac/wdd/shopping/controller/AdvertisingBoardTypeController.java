package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.AdvertisingBoard;
import cn.ac.wdd.shopping.entity.AdvertisingBoardType;
import cn.ac.wdd.shopping.entity.MarketType;
import cn.ac.wdd.shopping.entity.Merchant;
import cn.ac.wdd.shopping.service.IAdvertisingBoardService;
import cn.ac.wdd.shopping.service.IAdvertisingBoardTypeService;
import cn.ac.wdd.shopping.service.IMarketTypeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.qcloud.cos.utils.CollectionUtils;
import io.swagger.annotations.ApiOperation;
import org.apache.commons.lang3.concurrent.ConcurrentUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
@RestController
@RequestMapping("/advertising/board/type")
public class AdvertisingBoardTypeController {
    @Autowired
    private IAdvertisingBoardTypeService advertisingBoardTypeService;

    @Autowired
    private IAdvertisingBoardService advertisingBoardService;

    @ApiOperation("获取广告位类型列表")
    @PostMapping("/list")
    public ResultMessage<Page<AdvertisingBoardType>> getList(@RequestBody Page<AdvertisingBoardType> page){
        Page<AdvertisingBoardType> advertisingBoardTypePage = advertisingBoardTypeService.listByPage(page);
        return R.ok(advertisingBoardTypePage);
    }

    @ApiOperation("根据Id获取广告位类型")
    @GetMapping("")
    public ResultMessage<AdvertisingBoardType> getList(Long id){
        AdvertisingBoardType advertisingBoardType= advertisingBoardTypeService.getById(id);
        return R.ok(advertisingBoardType);
    }

    @ApiOperation("根据Id删除广告位类型")
    @PostMapping("/delete")
    public ResultMessage<Boolean> removeList(@RequestBody AdvertisingBoardType advertisingBoardType){

        QueryWrapper<AdvertisingBoard> advertisingBoardQueryWrapper = new QueryWrapper<>();
        advertisingBoardQueryWrapper.eq(AdvertisingBoard.ADVERTISING_BOARD_TYPE_ID, advertisingBoardType.getId());
        List<AdvertisingBoard> list = advertisingBoardService.list(advertisingBoardQueryWrapper);
        if (!CollectionUtils.isNullOrEmpty(list)){
            return R.err("存在广告位引用,请先删除广告位");
        }
        Boolean remove = advertisingBoardTypeService.removeById(advertisingBoardType.getId());
        return R.ok(remove);
    }

    @ApiOperation("根据Id更新广告位类型")
    @PostMapping("/update")
    public ResultMessage<Boolean> updateList(@RequestBody AdvertisingBoardType advertisingBoardType){
        Boolean remove = advertisingBoardTypeService.updateById(advertisingBoardType);
        return R.ok(remove);
    }

    @ApiOperation("添加广告位类型")
    @PostMapping("/add")
    public ResultMessage<Boolean> save(@RequestBody AdvertisingBoardType advertisingBoardType){
        Boolean save = advertisingBoardTypeService.save(advertisingBoardType);
        return R.ok(save);
    }
}
