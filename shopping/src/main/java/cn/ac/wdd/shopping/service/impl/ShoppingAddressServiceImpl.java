package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.ShoppingAddress;
import cn.ac.wdd.shopping.mapper.ShoppingAddressMapper;
import cn.ac.wdd.shopping.mapper.UserMapper;
import cn.ac.wdd.shopping.service.IShoppingAddressService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Service
public class ShoppingAddressServiceImpl extends ServiceImpl<ShoppingAddressMapper, ShoppingAddress> implements IShoppingAddressService {

    @Autowired
    private ShoppingAddressMapper shoppingAddressMapper;

    @Override
    public List<ShoppingAddress> selectByUserId() {
        Long userId = SecurityUtils.getCurrUserIdChecked();
        QueryWrapper<ShoppingAddress> shoppingAddressQueryWrapper = new QueryWrapper<>();
        shoppingAddressQueryWrapper.eq(ShoppingAddress.USER_ID,userId);
        List<ShoppingAddress> shoppingAddresses = shoppingAddressMapper.selectList(shoppingAddressQueryWrapper);
        return shoppingAddresses;
    }

    @Override
    public void defaultAddress(ShoppingAddress shoppingAddress) {
        ShoppingAddress addree = shoppingAddressMapper.selectById(shoppingAddress.getId());
        if (addree != null){
            UpdateWrapper<ShoppingAddress> shoppingAddressUpdateWrapper = new UpdateWrapper<>();
            shoppingAddressUpdateWrapper.set(ShoppingAddress.DEFAULT_ADDRESS,0);
            shoppingAddressUpdateWrapper.isNotNull(ShoppingAddress.ID);
            shoppingAddressMapper.update(null,shoppingAddressUpdateWrapper);
            shoppingAddressMapper.updateById(addree);
        }
    }

    @Override
    public boolean save(ShoppingAddress entity) {
        Long userId = SecurityUtils.getCurrUserIdChecked();
        entity.setUserId(userId);
        return super.save(entity);
    }
}
