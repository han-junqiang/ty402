package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.StringUtil;
import cn.ac.wdd.shopping.entity.MarketType;
import cn.ac.wdd.shopping.entity.MarketType;
import cn.ac.wdd.shopping.entity.Merchant;
import cn.ac.wdd.shopping.mapper.MarketTypeMapper;
import cn.ac.wdd.shopping.service.IMarketTypeService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
@Service
public class MarketTypeServiceImpl extends ServiceImpl<MarketTypeMapper, MarketType> implements IMarketTypeService {

    @Autowired
    private MarketTypeMapper marketTypeMapper;
    @Override
    public Page<MarketType> listByPage(Page<MarketType> page) {
        QueryWrapper<MarketType> merchantQueryWrapper = new QueryWrapper<>();
        if (page.getParams().get("name") != null && StringUtil.isNotEmpty(page.getParams().get("name").toString())){
            merchantQueryWrapper.like(Merchant.NAME,page.getParams().get("name").toString());
        }
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<MarketType> merchatPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>();
        merchatPage.setSize(page.getPageSize());
        merchatPage.setCurrent(page.getCurrentPage());
        merchatPage = marketTypeMapper.selectPage(merchatPage, merchantQueryWrapper);
        page.setTotalCount((int)merchatPage.getTotal());
        page.setCurrentPage((int)merchatPage.getCurrent());
        List<MarketType> merchants = marketTypeMapper.selectList(merchantQueryWrapper);
        page.setTotalCount(merchants.size());
        List<MarketType> records = merchatPage.getRecords();
        page.setList(records);
        return page;
    }
}
