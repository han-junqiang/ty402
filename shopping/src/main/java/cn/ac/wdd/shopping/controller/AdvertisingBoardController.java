package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.AdvertisingBoard;
import cn.ac.wdd.shopping.entity.AdvertisingBoard;
import cn.ac.wdd.shopping.entity.AdvertisingBoardType;
import cn.ac.wdd.shopping.service.IAdvertisingBoardService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
@RestController
@RequestMapping("/advertising/board")
public class AdvertisingBoardController {

    @Autowired
    private IAdvertisingBoardService advertisingBoardService;

    @ApiOperation("获取广告位列表")
    @PostMapping("/list")
    public ResultMessage<Page<AdvertisingBoard>> getList(@RequestBody Page<AdvertisingBoard> page){
        Page<AdvertisingBoard> advertisingBoardPage = advertisingBoardService.listByPage(page);
        return R.ok(advertisingBoardPage);
    }

    @ApiOperation("根据Id获取广告位")
    @GetMapping("")
    public ResultMessage<AdvertisingBoard> getList(Long id){
        AdvertisingBoard advertisingBoard= advertisingBoardService.getById(id);
        return R.ok(advertisingBoard);
    }

    @ApiOperation("根据Id删除广告位")
    @PostMapping("/delete")
    public ResultMessage<Boolean> removeList(@RequestBody  AdvertisingBoard advertisingBoard){
        Boolean remove = advertisingBoardService.removeById(advertisingBoard.getId());
        return R.ok(remove);
    }

    @ApiOperation("根据Id更新广告位")
    @PostMapping("/update")
    public ResultMessage<Boolean> updateList(@RequestBody AdvertisingBoard advertisingBoard){
        Boolean remove = advertisingBoardService.updateById(advertisingBoard);
        return R.ok(remove);
    }

    @ApiOperation("添加广告位")
    @PostMapping("/add")
    public ResultMessage<Boolean> save(@RequestBody AdvertisingBoard advertisingBoard){
        Boolean save = advertisingBoardService.save(advertisingBoard);
        return R.ok(save);
    }
}
