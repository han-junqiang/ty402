package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.Category;
import cn.ac.wdd.shopping.mapper.AttributeKeyMapper;
import cn.ac.wdd.shopping.mapper.CategoryMapper;
import cn.ac.wdd.shopping.protocol.request.AttributeRequest;
import cn.ac.wdd.shopping.service.ICategoryService;
import cn.ac.wdd.shopping.util.TreeUtil;
import cn.ac.wdd.shopping.entity.AttributeKey;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcloud.cos.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
@Service
public class CategoryServiceImpl extends ServiceImpl<CategoryMapper, Category> implements ICategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    private AttributeKeyMapper attributeKeyMapper;

    @Override
    public Map<String, List<Category>> getAllCategory() {


        List<Category> categoryList = categoryMapper.selectAll();
        categoryList.forEach(p->{
            Long categoryId = p.getId();
            QueryWrapper<AttributeKey> attributeKeyQueryWrapper = new QueryWrapper<>();
            attributeKeyQueryWrapper.eq(AttributeKey.CATEGORY_ID,categoryId);
            List<AttributeKey> attributeKeys = attributeKeyMapper.selectList(attributeKeyQueryWrapper);
            if (!CollectionUtils.isNullOrEmpty(attributeKeys)){
                p.setHasSpecs(true);
            }else {
                p.setHasSpecs(false);
            }
        });
        /*找根节点*/
        List<Category> rootCategory = TreeUtil.findMenuRoot(categoryList);
        /*为根节点设置子菜单*/
        for(Category category : rootCategory){
            /*获取某个根节点的子菜单*/
            List<Category> childList = TreeUtil.findCategoryChild(category.getId(),categoryList);
            category.setChildList(childList);
        }


        Map<String, List<Category>> map = new HashMap<>();
        map.put("tree", rootCategory);
        return map;
    }

    @Override
    public void saveAttribute(AttributeRequest attributeRequest) {
        AttributeKey attribute = AttributeKey.builder().categoryId(attributeRequest.getCategoryId()).attributeName(attributeRequest.getAttributeKey()).value(attributeRequest.getAttributeValue()).build();
        attributeKeyMapper.insert(attribute);
    }

    @Override
    public void updateAttribute(AttributeRequest attributeRequest) {
        AttributeKey attribute = AttributeKey.builder().attributeName(attributeRequest.getAttributeKey()).value(attributeRequest.getAttributeValue()).id(attributeRequest.getId()).build();
        attributeKeyMapper.updateById(attribute);
    }
}
