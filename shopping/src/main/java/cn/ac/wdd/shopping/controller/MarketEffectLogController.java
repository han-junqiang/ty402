package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.MarketEffectLog;
import cn.ac.wdd.shopping.service.IMarketEffectLogService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */

@RequestMapping("/market/log")
@RestController
public class MarketEffectLogController {

    @Autowired
    private IMarketEffectLogService marketEffectLogService;

    @PostMapping("/save")
    @ApiOperation(value = "事件类型 onclick(点击) add_shopping_trolley(添加购物车) create_order(下单) pay_finish(支付完成) banner_show(banner显示) goods_information_stremam(信息流)")
    public ResultMessage<Boolean> saveLog(@RequestBody MarketEffectLog marketLogRequest){
        boolean save = marketEffectLogService.saveLog(marketLogRequest);
        return R.ok("保持成功",save);
    }
}
