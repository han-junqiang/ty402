package cn.ac.wdd.shopping.controller;

import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.service.MarketingService;
import cn.ac.wdd.shopping.vo.MarketingVo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;


/**
 * @createTime 2022年05月08日 15:01:00
 */
@RestController
@RequestMapping("/marketing")
@Api(value = "推广相关")
public class MarketingController {


    @Autowired
    private MarketingService marketingService;


    @GetMapping("/banner/list")
    @ResponseBody
    public ResultMessage<List<MarketingVo>> bannerList(){

        List<MarketingVo> marketingVos =  marketingService.bannerList();

        return R.ok(marketingVos);
    }
    @GetMapping("/list/wechat")
    @ApiOperation(value = "小程序商品信息流")
    public ResultMessage<List<MarketingVo>> productListBywechat(){
        return R.ok(marketingService.productListBywechat());
    }

    @GetMapping("/top/search")
    @ApiOperation(value = "热门搜索")
    public ResultMessage<List<MarketingVo>> topSearch(){
        return R.ok(marketingService.topSearch());
    }

    @GetMapping("/choiceness/product")
    @ApiOperation(value = "精选商品")
    public ResultMessage<List<MarketingVo>> choicenessProduct(){
        return R.ok(marketingService.choicenessProduct());
    }

    @GetMapping("/pop_up/product")
    @ApiOperation(value = "弹窗广告")
    public ResultMessage<MarketingVo> popUpProduct(){
        return R.ok(marketingService.popUpProduct());
    }

}
