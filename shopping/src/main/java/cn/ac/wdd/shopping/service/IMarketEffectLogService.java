package cn.ac.wdd.shopping.service;

import cn.ac.wdd.shopping.entity.MarketEffectLog;
import cn.ac.wdd.shopping.protocol.request.MarketLogRequest;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
public interface IMarketEffectLogService extends IService<MarketEffectLog> {

    boolean saveLog(MarketEffectLog marketLogRequest);
}
