package cn.ac.wdd.shopping.service.impl;

import cn.ac.wdd.shopping.entity.IntegralProductLog;
import cn.ac.wdd.shopping.entity.User;
import cn.ac.wdd.shopping.mapper.IntegralProductLogMapper;
import cn.ac.wdd.shopping.mapper.UserMapper;
import cn.ac.wdd.shopping.service.IIntegralProductLogService;
import cn.ac.wdd.shopping.util.SecurityUtils;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
@Service
public class IntegralProductLogServiceImpl extends ServiceImpl<IntegralProductLogMapper, IntegralProductLog> implements IIntegralProductLogService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    private IntegralProductLogMapper integralProductLogMapper;


    @Override
    public boolean save(IntegralProductLog entity) {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        User user = userMapper.selectById(userIdChecked);
        if (user.getScore() < entity.getTotalPrice()){
            throw new RuntimeException("积分数量不够");
        }
        user.setScore(user.getScore() - entity.getTotalPrice());
        userMapper.updateById(user);
        return super.save(entity);
    }


    @Override
    public List<IntegralProductLog> getList() {
        QueryWrapper<IntegralProductLog> integralProductLogQueryWrapper = new QueryWrapper<>();
        integralProductLogQueryWrapper.eq(IntegralProductLog.USER_ID,SecurityUtils.getCurrUserIdChecked());
        return integralProductLogMapper.selectList(integralProductLogQueryWrapper);
    }
}
