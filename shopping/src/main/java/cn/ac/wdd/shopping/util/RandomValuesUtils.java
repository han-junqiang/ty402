package cn.ac.wdd.shopping.util;

import java.util.*;

/**
 * @createTime 2022年05月09日 10:56:00
 */
public class RandomValuesUtils {

    public static <T> List<T> randomValues(List<T>list, int length){
        Set<T> randomSource=new HashSet<T>(list);
        //重复数据防越界
        if(randomSource.size()<=length){
            return new ArrayList<T>(randomSource);
        }
        List<T>randomResult=new ArrayList<>(randomSource);

        Set<T>  randomList=new HashSet<T>();
        Random random =new Random();
        while (randomList.size()<length){
            randomList.add(randomResult.get(random.nextInt(randomResult.size())));
        }
        return new ArrayList<>(randomList);
    }
}
