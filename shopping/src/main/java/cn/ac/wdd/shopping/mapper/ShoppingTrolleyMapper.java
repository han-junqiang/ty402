package cn.ac.wdd.shopping.mapper;

import cn.ac.wdd.shopping.entity.ShoppingTrolley;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
public interface ShoppingTrolleyMapper extends BaseMapper<ShoppingTrolley> {

}
