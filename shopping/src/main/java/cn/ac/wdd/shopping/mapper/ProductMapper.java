package cn.ac.wdd.shopping.mapper;

import cn.ac.wdd.shopping.entity.Product;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-11
 */
public interface ProductMapper extends BaseMapper<Product> {

    List<Product> selectRand();

    Product selectId(Serializable id);
}
