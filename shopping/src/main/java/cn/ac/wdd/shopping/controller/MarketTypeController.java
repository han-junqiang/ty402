package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.MarketType;
import cn.ac.wdd.shopping.service.IMarketTypeService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-14
 */
@RestController
@RequestMapping("/market/type")
public class MarketTypeController {

    @Autowired
    private IMarketTypeService marketTypeService;

    @ApiOperation("获取推广类型列表")
    @PostMapping("/list")
    public ResultMessage<Page<MarketType>> getList(@RequestBody Page<MarketType> page){
        Page<MarketType> marketTypePage = marketTypeService.listByPage(page);
        return R.ok(marketTypePage);
    }

    @ApiOperation("根据Id获取推广类型")
    @GetMapping("")
    public ResultMessage<MarketType> getList(Long id){
        MarketType marketType= marketTypeService.getById(id);
        return R.ok(marketType);
    }

    @ApiOperation("根据Id删除推广类型")
    @PostMapping("/delete")
    public ResultMessage<Boolean> removeList(@RequestBody MarketType marketType){
        Boolean remove = marketTypeService.removeById(marketType.getId());
        return R.ok(remove);
    }

    @ApiOperation("根据Id更新推广类型")
    @PostMapping("/update")
    public ResultMessage<Boolean> updateList(@RequestBody MarketType marketType){
        Boolean remove = marketTypeService.updateById(marketType);
        return R.ok(remove);
    }

    @ApiOperation("添加推广类型")
    @PostMapping("/add")
    public ResultMessage<Boolean> save(@RequestBody  MarketType marketType){
        Boolean save = marketTypeService.save(marketType);
        return R.ok(save);
    }
}
