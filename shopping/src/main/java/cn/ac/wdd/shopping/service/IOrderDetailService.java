package cn.ac.wdd.shopping.service;

import cn.ac.wdd.shopping.entity.OrderDetail;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-12
 */
public interface IOrderDetailService extends IService<OrderDetail> {

}
