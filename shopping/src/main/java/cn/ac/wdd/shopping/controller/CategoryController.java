package cn.ac.wdd.shopping.controller;


import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.shopping.entity.AttributeKey;
import cn.ac.wdd.shopping.entity.Category;
import cn.ac.wdd.shopping.protocol.request.AttributeRequest;
import cn.ac.wdd.shopping.service.IAttributeKeyService;
import cn.ac.wdd.shopping.service.ICategoryService;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private ICategoryService categoryService;
    @Autowired
    private IAttributeKeyService attributeKeyService;


    @RequestMapping(value = "/getAllCategory",method = RequestMethod.GET)
    @ApiOperation(value = "查询所有分类信息")
    public ResultMessage<Map<String, List<Category>>> getAllCategory(){
        Map<String, List<Category>> map = categoryService.getAllCategory();
        return R.ok(map);
    }

    @ApiOperation("添加分类信息")
    @PostMapping("/add")
    public ResultMessage<Boolean> save(@RequestBody Category category){
        boolean save = categoryService.save(category);
        return R.ok(save);
    }
    @ApiOperation("更新分类信息")
    @PostMapping("/update")
    public ResultMessage<Boolean> update(@RequestBody Category category){
        boolean save = categoryService.updateById(category);
        return R.ok(save);
    }
    @ApiOperation("删除分类")
    @PostMapping("/delete")
    public ResultMessage<Boolean> delete(@RequestBody Category category){
        boolean remove = categoryService.removeById(category.getId());
        return R.ok(remove);
    }

    @ApiOperation("根据id获取分类")
    @GetMapping("")
    public ResultMessage<Category> getEntityById(Long id){
        Category entity = categoryService.getById(id);
        return R.ok(entity);
    }

    @ApiOperation("添加规格")
    @PostMapping("/add/attribute")
    public ResultMessage<Object> saveAttribute(@RequestBody AttributeRequest attributeRequest){
        categoryService.saveAttribute(attributeRequest);
        return R.ok();
    }


    @ApiOperation("根据规格id查询规格")
    @GetMapping("/attribute")
    public ResultMessage<AttributeKey> getAttribute(Long id){
        return R.ok(attributeKeyService.getById(id));
    }


    @ApiOperation("更新规格")
    @PostMapping("/update/attribute")
    public ResultMessage<Object> updateAttribute(@RequestBody AttributeRequest attributeRequest){
        categoryService.updateAttribute(attributeRequest);
        return R.ok();
    }

    @ApiOperation("删除规格")
    @PostMapping("/delete/attribute")
    public ResultMessage<Object> deleteAttribute(@RequestBody AttributeRequest attributeRequest){
        attributeKeyService.removeById(attributeRequest.getId());
        return R.ok();
    }

    @ApiOperation("更具分类id获取规格List")
    @GetMapping("/list/attribute")
    public ResultMessage<List<AttributeKey>> getAttributeList(Long id){
        QueryWrapper<AttributeKey> attributeKeyQueryWrapper = new QueryWrapper<>();
        attributeKeyQueryWrapper.eq(AttributeKey.CATEGORY_ID,id);
        return R.ok(attributeKeyService.list(attributeKeyQueryWrapper));
    }
}
