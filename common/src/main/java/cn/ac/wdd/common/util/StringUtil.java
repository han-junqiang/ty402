package cn.ac.wdd.common.util;

import java.math.BigDecimal;
import java.util.Arrays;
import java.util.List;

public class StringUtil {

  public static final String SPACE = " ";
  public static final String EMPTY = "";
  public static final String LF = "\n";
  public static final String CR = "\r";

  public static boolean isEmpty(String str) {
    return null == str || str.length() == 0;
  }

  public static boolean isNotEmpty(String str) {
    return null != str && str.length() != 0;
  }

  public static String toStr(Object obj, String defaultStr) {
    if (obj == null) {
      return defaultStr;
    }
    if (obj instanceof BigDecimal) {
      return BigDecimals.prettyFormat((BigDecimal) obj);
    }
    return obj.toString();
  }

  public static String numberToSimpleStr(Integer totalTimes) {
    if (totalTimes < 10) {
      return totalTimes + "+";
    } else if (totalTimes < 1000) {
      return (totalTimes - totalTimes % 10) + "+";
    } else if (totalTimes < 10000) {
      return (totalTimes / 100) / 10.0 + "K+";
    } else {
      return (totalTimes / 1000) / 10.0 + "W+";
    }
  }

  public static List<Integer> getFromComboString(String comboStr, int... args) {
    if (StringUtil.isEmpty(comboStr)) {
      return Arrays.asList(0, 0);
    }
    int min = 5;
    int max = 10;
    if (args.length > 0) {
      min = args[0];
    }
    if (args.length > 1) {
      max = args[1];
    }
    int total = 0;
    int count = 0;
    int score = 0;
    int length = comboStr.length();
    for (int i = 0; i < length; i++) {
      if (comboStr.charAt(i) == '1') {
        count++;
        if (count > 1) {
          total++;
          score += getScoreByComBoNum(count - 1, min, max);
        }
      } else {
        count = 0;
      }
    }
    return Arrays.asList(total, score);
  }

  private static int getScoreByComBoNum(int comboNum, int min, int max) {
    return Math.min(max, comboNum + min - 1);
  }

  public static String formatTrainedNumber(int trainedNumber) {
    if (trainedNumber < 1000) {
      return trainedNumber + "";
    }
    return trainedNumber / 1000 + "K+";
  }

}
