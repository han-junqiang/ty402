/**
 * *****************************************************
 *
 * Copyright (C) 2021 bodypark.ai All Rights Reserved This file is part of bodypark project.
 * Unauthorized copy of this file, via any medium is strictly prohibited. Proprietary and
 * Confidential.
 *
 * ****************************************************
 **/
package cn.ac.wdd.common.util;

/**
 * @author Zou Pengfei<zoupengfei@bodypark.ai>
 * @date 05/17/2021 22:21
 */
public interface IEnum {

  String code();

  String desc();
}
