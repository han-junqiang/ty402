package cn.ac.wdd.common.util;

public class ResultMessage<T> {
  private int resCode = 1;
  private String resMsg;
  private T result;

  public ResultMessage() { }

  public ResultMessage(int resCode, String resMsg, T result) {
    this.resCode = resCode;
    this.resMsg = resMsg;
    this.result = result;
  }

  public T getResult() {
    return result;
  }

  public ResultMessage<T> setResult(T result) {
    this.result = result;
    return this;
  }

  public ResultMessage<T> success() {
    this.setResCode(1);
    this.setResMsg("操作成功");
    return this;
  }

  public ResultMessage<T> failed() {
    this.setResCode(0);
    this.setResMsg("操作失败");
    return this;
  }

  public ResultMessage<T> success(T result) {
    return new ResultMessage<>(1, "操作成功", result);
  }

  public ResultMessage<T> failed(T result) {
    return new ResultMessage<>(0, "操作失败", result);
  }

  public int getResCode() {
    return resCode;
  }

  public void setResCode(int resCode) {
    this.resCode = resCode;
  }

  public String getResMsg() {
    return resMsg;
  }

  public ResultMessage<T> setResMsg(String resMsg) {
    this.resMsg = resMsg;
    return this;
  }

}
