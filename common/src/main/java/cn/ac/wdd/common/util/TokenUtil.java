package cn.ac.wdd.common.util;

import java.util.Optional;

public class TokenUtil {

  public static final String AUTH_HEADER_KEY = "Authorization";
  public static final String TOKEN_PREFIX = "Bearer ";
  public static final String REDIS_TOKEN_PREFIX = "TOKEN:";
  public static final String REDIS_TOKEN_USERID_MAPPING_PREFIX = "USER_TOKEN:";
  public static final String USER_INFO_ATTRIBUTE_NAME = "userInfo";
  public static final String TEST_USER_HEADER_KEY = "x-test-user";

    public static Optional<String> getTokenFromHeader(String authHeader) {
        if (StringUtil.isEmpty(authHeader) || !authHeader.startsWith(TokenUtil.TOKEN_PREFIX)
                || authHeader
                .equals(TokenUtil.TOKEN_PREFIX)) {
            return Optional.empty();
        }

        String token = authHeader.substring(TokenUtil.TOKEN_PREFIX.length());
        return Optional.of(token);
    }
}

