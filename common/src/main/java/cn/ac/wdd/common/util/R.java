package cn.ac.wdd.common.util;



/**
 */
public class R {

  public static <T> ResultMessage<T> ok() {
    return new ResultMessage<T>().success(null);
  }

  public static <T> ResultMessage<T> ok(T data) {
    return new ResultMessage<T>().success(data);
  }

  public static <T> ResultMessage<T> ok(String msg, T data) {
    return new ResultMessage<T>().success(data).setResMsg(msg);
  }

  public static <T> ResultMessage<T> err() {
    return new ResultMessage<T>().failed();
  }

  public static <T> ResultMessage<T> err(String msg) {
    return new ResultMessage<T>().failed().setResMsg(msg);
  }

  public static <T> ResultMessage<T> errWithData(T data) {
    return new ResultMessage<T>().failed().setResult(data);
  }

  public static <T> ResultMessage<T> err(String msg, T data) {
    return new ResultMessage<T>().failed(data).setResMsg(msg);
  }

}
