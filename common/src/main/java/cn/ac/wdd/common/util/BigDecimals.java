/**
 * *****************************************************
 *
 * Copyright (C) 2021 bodypark.ai All Rights Reserved This file is part of bodypark project.
 * Unauthorized copy of this file, via any medium is strictly prohibited. Proprietary and
 * Confidential.
 *
 * ****************************************************
 **/
package cn.ac.wdd.common.util;



//import lombok.extern.slf4j.Slf4j;
//
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.Arrays;

/**
 * bigdecimal工具类
 *
 */
//@Slf4j
public class BigDecimals {

  /**
   * 功能描述: 一百
   *
   * @author v-zoupengfei.ea
   * @date 20:21 20:21
   */
  public final static BigDecimal ONE_HUNDRED = multiply(BigDecimal.TEN, BigDecimal.TEN);

  /**
   * 功能描述: string转换为bigdecimal,如果为空返回默认值
   */
  public static BigDecimal parse(String str, BigDecimal... defaultValue) {
    if (str == null || str == "") {
      return defaultValue.length > 0 ? defaultValue[0] : null;
    }
    return new BigDecimal(str.trim());
  }

  /**
   * 功能描述: null时默认值
   */
  public static BigDecimal defaultIfNull(BigDecimal value, BigDecimal defaultValue) {
    return value == null ? defaultValue : value;
  }

  public static BigDecimal valueOf(String s) {
    return parse(s);
  }

  /**
   * 格式化为整洁的数字，示例： 1000 -> 1000 1000.000 -> 1000 1000.1000 -> 1000.1 0.1000 -> 0.1
   */
  public static String prettyFormat(BigDecimal b) {
    if (b == null) {
      return null;
    }
    char[] digitalChars = b.toPlainString().toCharArray();
    int len = digitalChars.length;
    boolean dot = false;
    int start = -1;
    int end = -1;
    int i = 0;
    for (; ; ) {
      if (i == len) {
        if (!dot) {
          end = i;
        }
        break;
      }
      char c = digitalChars[i++];
      switch (c) {
        case '0':
          break;
        case '.':
          dot = true;
          if (start == -1) {
            start = i - 2;
          }
          end = i - 1;
          break;
        default:
          if (dot) {
            end = i;
          } else {
            if (start == -1) {
              start = i - 1;
            }
          }
      }
    }
    return new String(digitalChars, start, end - start);
  }

  /**
   * 格式化数字为千分位
   */
  public static String format(BigDecimal a, String s) {
    if (a == null) {
      return "";
    }
    DecimalFormat df = null;
    if (s != null && s != "") {
      df = new DecimalFormat(s);
    } else {
      df = new DecimalFormat("##,####,###.#########");
    }
    return df.format(a);
  }


  /**
   * 比较
   */
  public static boolean lessThan(BigDecimal a, BigDecimal b) {
    if (a == null && b == null) {
      return false;
    }
    if (a == null && b != null) {
      return true;
    }
    if (a != null && b == null) {
      return false;
    }
    return a.compareTo(b) < 0;
  }

  public static boolean lessThanOrEqual(BigDecimal a, BigDecimal b) {
    return !greaterThan(a, b);
  }

  public static boolean greaterThan(BigDecimal a, BigDecimal b) {
    if (a == null && b == null) {
      return false;
    }
    if (a == null && b != null) {
      return false;
    }
    if (a != null && b == null) {
      return true;
    }
    return a.compareTo(b) > 0;
  }

  public static boolean greaterThanOrEqual(BigDecimal a, BigDecimal b) {
    return !lessThan(a, b);
  }


  /**
   * 除法
   */
  public static BigDecimal divide(BigDecimal a, BigDecimal b) {
    if (illegalDivide(a, b)) {
      return null;
    }
    return a.divide(b);
  }

  public static BigDecimal divide(BigDecimal a, BigDecimal b, int scale) {
    if (illegalDivide(a, b)) {
      return null;
    }
    return a.divide(b, scale, BigDecimal.ROUND_HALF_UP);
  }

  public static BigDecimal reciprocal(BigDecimal a) {
    return reciprocal(a, 4);
  }

  public static BigDecimal reciprocal(BigDecimal a, int scale) {
    return divide(BigDecimal.ONE, a, scale);
  }


  /**
   * 功能描述: 乘法
   */
  public static BigDecimal multiply(BigDecimal... a) {
    if (illegalBigdecimals(a)) {
      return BigDecimal.ZERO;
    }
    return Arrays.stream(a).reduce(BigDecimal.ONE, BigDecimal::multiply);
  }

  public static BigDecimal multiply(int scale, BigDecimal... a) {
    return multiply(a)
        .setScale(scale, RoundingMode.HALF_UP);
  }

  public static BigDecimal multiply(BigDecimal a, BigDecimal b, int scale) {
    return multiply(scale, a, b);
  }

  public static BigDecimal multiply(BigDecimal a, BigDecimal b, BigDecimal c, int scale) {
    return multiply(scale, a, b, c);
  }

  public static BigDecimal power(BigDecimal a) {
    return multiply(a, a);
  }

  public static BigDecimal power(BigDecimal a, int power) {
    if (power <= 0) {
      return a;
    }
    BigDecimal start = BigDecimal.ONE;
    for (int i = 0; i < power; i++) {
      start = multiply(start, a);
    }
    return start;
  }


  /**
   * 功能描述: 加法
   */
  public static BigDecimal add(BigDecimal... a) {
    if (a.length == 0) {
      return BigDecimal.ZERO;
    }
    BigDecimal total = BigDecimal.ZERO;
    for (BigDecimal b : a) {
      if (b != null) {
        total = total.add(b);
      }
    }
    return total;
  }

  public static BigDecimal add(int scale, BigDecimal... a) {
    return add(a)
        .setScale(scale, RoundingMode.HALF_UP);
  }

  public static BigDecimal add(BigDecimal a, BigDecimal b, int scale) {
    return add(a, b)
        .setScale(scale, RoundingMode.HALF_UP);
  }

  public static BigDecimal add(BigDecimal a, BigDecimal b, BigDecimal c, int scale) {
    return add(a, b, c)
        .setScale(scale, RoundingMode.HALF_UP);
  }

  /**
   * 减法
   */
  public static BigDecimal subtract(BigDecimal... a) {
    if (a.length == 0) {
      return BigDecimal.ZERO;
    }
    BigDecimal total = a[0];
    for (int i = 1; i < a.length; i++) {
      if (a[i] != null) {
        total = total.subtract(a[i]);
      }
    }

    return total;
  }

  public static BigDecimal subtract(int scale, BigDecimal... a) {
    return subtract(a)
        .setScale(scale, RoundingMode.HALF_UP);
  }

  public static BigDecimal subtract(BigDecimal a, BigDecimal b, int scale) {
    return subtract(a, b)
        .setScale(scale, RoundingMode.HALF_UP);
  }

  public static BigDecimal subtract(BigDecimal a, BigDecimal b, BigDecimal c, int scale) {
    return subtract(a, b, c)
        .setScale(scale, RoundingMode.HALF_UP);
  }


  /**
   * 功能描述: 检查数字的精度、标度是否满足条件
   */
  public static boolean scaleCheck(BigDecimal b, int precision, int scale) {
    if (b == null) {
      return true;
    }
    int bPresicion = b.precision();
    int bScale = b.scale();
    int bIntegerLen = b.toBigInteger().toString().length();
    return bPresicion <= precision && bIntegerLen <= (precision - scale) && bScale <= scale;
  }

  private static boolean illegalDivide(BigDecimal a, BigDecimal b) {
    return illegalBigdecimals(a, b) || BigDecimal.ZERO.compareTo(b) == 0;
  }

  private static boolean illegalBigdecimals(BigDecimal... bigDecimals) {
    if (bigDecimals.length == 0) {
      return true;
    }
    for (BigDecimal b : bigDecimals) {
      if (b == null) {
        return true;
      }
    }
    return false;
  }
}
