package cn.ac.wdd.common.util;


import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;


/**
 * 枚举通用工具类
 *
 * @author Zou Pengfei<zoupengfei@bodypark.ai>
 * @date 05/18/2021 09:28
 */
public class Enums {

  /**
   * 获取枚举值列表
   */
  public static <T extends IEnum> T[] values(Class<T> enumType) {
    return enumType.getEnumConstants();
  }

  /**
   * 获取枚举值Code列表
   */
  public static <T extends IEnum> List<String> codes(Class<T> enumType) {
    return Arrays.stream(values(enumType)).map(IEnum::code).collect(Collectors.toList());
  }
  /**
   * 获取枚举值Code列表
   */
  public static <T extends IEnum> boolean contains(Class<T> enumType, String code) {
    return Arrays.stream(values(enumType)).anyMatch(v->v.code().equalsIgnoreCase(code));
  }
  /**
   * 获取枚举值Code列表
   */
  public static <T extends IEnum> Map<String, T> toMap(Class<T> enumType) {
    return Arrays.stream(values(enumType)).collect(Collectors.toMap(
        IEnum::code,
        Function.identity()
    ));
  }

  /**
   * 根据code查找
   */
  public static <T extends IEnum> T byCode(Class<T> enumType, String code) {
    return byCode(enumType, code, null);
  }

  /**
   * 根据code查找
   */
  public static <T extends IEnum> T byCode(Class<T> enumType, String code, T defaultValue) {
    T[] enums = values(enumType);
    if (enums != null && enums.length > 0) {
      return Arrays.stream(enums)
          .filter(e -> e.code().equalsIgnoreCase(code))
          .findFirst()
          .orElse(defaultValue);
    }
    return defaultValue;
  }

  /**
   * 根据name查找
   */
  public static <T extends IEnum> T byDesc(Class<T> enumType, String desc) {
    return byDesc(enumType, desc, null);
  }

  /**
   * 根据name查找
   */
  public static <T extends IEnum> T byDesc(Class<T> enumType, String desc, T defaultValue) {
    T[] enums = values(enumType);
    if (enums != null && enums.length > 0) {
      return Arrays.stream(enums)
          .filter(e -> e.desc().equalsIgnoreCase(desc))
          .findFirst()
          .orElse(defaultValue);
    }
    return defaultValue;
  }

}
