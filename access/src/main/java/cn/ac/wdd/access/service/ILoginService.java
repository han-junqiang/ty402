package cn.ac.wdd.access.service;

import cn.ac.wdd.access.protocol.request.MiniUserReq;
import cn.ac.wdd.access.protocol.request.SmsLoginReq;
import cn.ac.wdd.access.protocol.request.WechatLoginReq;
import cn.ac.wdd.access.vo.LoginVo;

import java.io.IOException;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-06
 */
public interface ILoginService {
    String getSms(SmsLoginReq smsLoginReq);

    LoginVo smsWebLogin(SmsLoginReq smsLoginReq);

    LoginVo wechatWebLogin(WechatLoginReq wechatLoginReq);

    LoginVo wechatRegister(MiniUserReq userReq);
}
