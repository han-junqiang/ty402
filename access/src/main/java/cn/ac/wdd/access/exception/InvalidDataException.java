/**
 * *****************************************************
 *
 * Copyright (C) 2021 bodypark.ai All Rights Reserved This file is part of bodypark project.
 * Unauthorized copy of this file, via any medium is strictly prohibited. Proprietary and
 * Confidential.
 *
 * ****************************************************
 **/
package cn.ac.wdd.access.exception;

/**
 * @author Zou Pengfei<zoupengfei@bodypark.ai>
 * @date 05/18/2021 10:11
 */
public class InvalidDataException extends BaseException {

  private static final long serialVersionUID = -6088615573886199389L;
  private static final int ERROR_CODE = 500;

  public InvalidDataException(String message) {
    super(ERROR_CODE, message);
  }

  public InvalidDataException(String message, Throwable throwable) {
    super(ERROR_CODE, message);
  }
}
