package cn.ac.wdd.access.controller;


import cn.ac.wdd.access.entity.Menu;
import cn.ac.wdd.access.entity.Role;
import cn.ac.wdd.access.service.IRoleService;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;

import java.util.List;

/**
 * <p>
 * 角色表 前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    @Autowired
    private IRoleService roleService;

    @RequestMapping(value = "/roleList",method = RequestMethod.POST)
    @ApiOperation(value = "分页查询角色信息",notes = "分页查询角色信息")
    public ResultMessage<Page<Role>> getByPage(@RequestBody Page<Role> page){
        page = roleService.getAllByPage(page);
        return  R.ok(page);
    }

    @RequestMapping(value = "/getAllRole",method = RequestMethod.GET)
    @ApiOperation(value = "查询所有角色信息",notes = "查询所有角色信息")
    public ResultMessage<List<Role>> getAllRole(){
        List<Role> roleList = roleService.list();
        return R.ok(roleList);
    }


    @RequestMapping(value = "/addRole",method = RequestMethod.POST)
    @ApiOperation(value = "添加角色",notes = "添加角色")
    public ResultMessage<Boolean> addRole(@RequestBody Role role){
        boolean save = roleService.save(role);
        return R.ok(save);
    }

    @RequestMapping(value = "/deleteRole/{id}",method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ID删除角色",notes = "根据ID删除角色")
    public ResultMessage<String> deleteRole(@PathVariable Integer id){
        boolean check = roleService.removeById(id);
        if (check == true){
            return  R.ok("删除成功！");
        }else{
            return R.ok("删除失败！");
        }
    }


    @RequestMapping(value = "/viewRole/{id}",method = RequestMethod.GET)
    @ApiOperation(value = "根据ID查看角色信息",notes = "根据ID查看角色信息")
    public ResultMessage<Role> viewRole(@PathVariable Integer id){
        Role role = roleService.getById(id);
        return  R.ok(role);
    }


    @RequestMapping(value = "/update",method = RequestMethod.PUT)
    @ApiOperation(value = "根据ID更新角色信息",notes = "根据ID更新角色信息")
    public ResultMessage<String> update(@RequestBody Role role){
        boolean check = roleService.updateById(role);
        if (check == true){
            return  R.ok("更新成功！");
        }else{
            return R.ok("更新失败！");
        }
    }

    @RequestMapping(value = "/getMenuByRoleId/{id}",method = RequestMethod.GET)
    @ApiOperation(value = "加载角色的权限菜单",notes = "加载角色的权限菜单")
    public ResultMessage<List<Menu>> getMenuByRoleId(@PathVariable Long id){
        List<Menu> list = roleService.getMenuByRoleId(id);
        return  R.ok(list);
    }



    @RequestMapping(value = "/addMenus/{id}",method = RequestMethod.POST)
    @ApiOperation(value = "为角色批量添加菜单",notes = "为角色批量添加菜单")
    public ResultMessage<String> addMenus(@RequestBody Integer[] ids, @PathVariable Integer id){
        roleService.removeMenus(id);
        int number = roleService.addMenus(ids,id);
        if (number > 0){
            return  R.ok("添加成功");
        }else{
            return R.ok("添加失败");
        }
    }

}
