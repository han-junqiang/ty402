package cn.ac.wdd.access.util;

import cn.ac.wdd.access.entity.Menu;

import java.util.ArrayList;
import java.util.List;

/**
 * @createTime 2022年05月07日 17:05:00
 */
public class TreeUtil {

    public static List<Menu> findMenuRoot(List<Menu> menuVoList){
        List<Menu> rootMenu = new ArrayList<>();
        for(Menu menu : menuVoList){


            if( menu.getParentId() == null || menu.getParentId() == 0){
                rootMenu.add(menu);
            }
        }
        return rootMenu;
    }

    public static  List<Menu> findMenuChild(Long id, List<Menu> menuVoList){
        List<Menu> childList = new ArrayList<Menu>();
        for(Menu menuVoChild : menuVoList){
            if(menuVoChild.getParentId().equals(id)){
                childList.add(menuVoChild);
            }
        }
        //递归
        for (Menu nav : childList) {
            nav.setChildren(findMenuChild(nav.getId(), menuVoList));
        }
        //如果节点下没有子节点，返回一个空List（递归退出）
        if(childList.size() == 0){
            return new ArrayList<Menu>();
        }
        return childList;

    }
}
