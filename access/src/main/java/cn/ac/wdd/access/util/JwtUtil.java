package cn.ac.wdd.access.util;

import cn.ac.wdd.access.constant.ResultMsg;
import cn.ac.wdd.access.entity.User;
import cn.ac.wdd.access.mapper.UserMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.TimeoutUtils;
import cn.ac.wdd.common.util.IDUtils;
import cn.ac.wdd.common.util.TokenUtil;

import javax.security.auth.message.AuthException;
import java.util.concurrent.TimeUnit;

@Configuration
public class JwtUtil {

  @Autowired
  private UserMapper userMapper;
  @Autowired
  private RedisTemplate<String, User> userPoRedisTemplate;
  @Autowired
  private RedisTemplate<String, String> redisTemplate;

  private final long SESSION_TOKEN_EXPIRES_IN_SECONDS = TimeoutUtils.toSeconds(60, TimeUnit.DAYS);


  public String createJWT(Long userId) {
    String token = generateToken(userId);
    userPoRedisTemplate.opsForValue()
        .set(TokenUtil.REDIS_TOKEN_PREFIX + token, userMapper.selectById(userId),
            SESSION_TOKEN_EXPIRES_IN_SECONDS, TimeUnit.SECONDS);
    redisTemplate.opsForValue()
        .set(TokenUtil.REDIS_TOKEN_USERID_MAPPING_PREFIX + userId, token,
            SESSION_TOKEN_EXPIRES_IN_SECONDS, TimeUnit.SECONDS);
    return token;
  }

  private String generateToken(Long userId) {
    return IDUtils.createUUID();
  }

  public User getUserInfo(String token) throws AuthException {
    User userInfo = userPoRedisTemplate.opsForValue().get(TokenUtil.REDIS_TOKEN_PREFIX + token);
    if (null == userInfo) {
      throw new AuthException(MessageUtil.get(ResultMsg.PERMISSION_TOKEN_EXPIRED));
    } else {
      refreshToken(token);
      return userInfo;
    }
  }

  private void refreshToken(String token) {
    userPoRedisTemplate
        .expire(TokenUtil.REDIS_TOKEN_PREFIX + token, SESSION_TOKEN_EXPIRES_IN_SECONDS,
            TimeUnit.SECONDS);
  }

}
