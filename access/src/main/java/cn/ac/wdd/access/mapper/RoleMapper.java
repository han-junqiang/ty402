package cn.ac.wdd.access.mapper;

import cn.ac.wdd.access.entity.Role;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
public interface RoleMapper extends BaseMapper<Role> {

}
