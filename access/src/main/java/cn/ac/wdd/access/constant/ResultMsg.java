package cn.ac.wdd.access.constant;

public class ResultMsg {
  public static String USER_NOT_LOGGED_IN = "USER_NOT_LOGGED_IN";
  public static String PERMISSION_SIGNATURE_ERROR = "PERMISSION_SIGNATURE_ERROR";
  public static String PERMISSION_TOKEN_EXPIRED = "PERMISSION_TOKEN_EXPIRED";
  public static String PERMISSION_TOKEN_INVALID = "PERMISSION_TOKEN_INVALID";
  public static String PHONE_LOGIN_USER_NOT_EXISTS_ERROR = "PHONE_LOGIN_USER_NOT_EXISTS_ERROR";
}
