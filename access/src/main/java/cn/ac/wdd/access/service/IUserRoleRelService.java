package cn.ac.wdd.access.service;

import cn.ac.wdd.access.entity.Role;
import cn.ac.wdd.access.entity.UserRoleRel;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 用户-角色关联表 服务类
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
public interface IUserRoleRelService extends IService<UserRoleRel> {

    int addRoles(Integer[] ids,Integer id);

    void removeRole(Integer id);

    Map<String, Object> getUserMenu(Integer id);

    List<Role> getUserRole(Integer id);
}
