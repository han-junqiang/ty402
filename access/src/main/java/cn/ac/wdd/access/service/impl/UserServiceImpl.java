package cn.ac.wdd.access.service.impl;

import cn.ac.wdd.access.entity.User;
import cn.ac.wdd.access.mapper.UserMapper;
import cn.ac.wdd.access.service.IUserService;
import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.StringUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.List;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-15
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements IUserService {


    @Autowired
    private UserMapper userMapper;

    @Override
    public Page<User> selectByPage(Page<User> page) {

        QueryWrapper<User> merchantQueryWrapper = new QueryWrapper<>();
        if (page.getParams().get("nickName") != null && StringUtil.isNotEmpty(page.getParams().get("nickName").toString())){
            merchantQueryWrapper.like(User.NICKNAME,page.getParams().get("nickName").toString());
        }
        com.baomidou.mybatisplus.extension.plugins.pagination.Page<User> userPage = new com.baomidou.mybatisplus.extension.plugins.pagination.Page<>();
        userPage.setSize(page.getPageSize());
        userPage.setCurrent(page.getCurrentPage());
        userPage = userMapper.selectPage(userPage, merchantQueryWrapper);
        page.setTotalCount((int)userPage.getTotal());
        page.setCurrentPage((int)userPage.getCurrent());
        List<User> merchants = userMapper.selectList(merchantQueryWrapper);
        page.setTotalCount(merchants.size());
        List<User> records = userPage.getRecords();

        records.forEach(p->{

            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            Instant instant = p.getLastVisitAt().toInstant();
            ZoneId zoneId = ZoneId.systemDefault();
            LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();
            String format = dateTimeFormatter.format(localDateTime);
            p.setLastVisitAtStr(format);
        });

        page.setList(records);
        return page;
    }
}
