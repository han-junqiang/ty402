package cn.ac.wdd.access.protocol.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@ToString
@Data
@AllArgsConstructor
@NoArgsConstructor
public class MiniUserReq {
  private String avatarUrl;
  private String nickName;
  private Integer gender;
  private String language;
}
