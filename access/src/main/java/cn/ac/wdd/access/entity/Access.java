package cn.ac.wdd.access.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 权限表
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
@Getter
@Setter
@TableName("t_access")
@Builder
@ApiModel(value = "Access对象", description = "权限表")
public class Access implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("权限类型")
    @TableField("access_type")
    private Integer accessType;

    @ApiModelProperty("权限路径")
    @TableField("url")
    private String url;

    @ApiModelProperty("API请求方法")
    @TableField("method")
    private String method;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("创建用户id")
    @TableField("create_by")
    private Long createBy;

    @ApiModelProperty("修改用户id")
    @TableField("update_by")
    private Long updateBy;


    public static final String ID = "id";

    public static final String ACCESS_TYPE = "access_type";

    public static final String URL = "url";

    public static final String METHOD = "method";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String CREATE_BY = "create_by";

    public static final String UPDATE_BY = "update_by";

}
