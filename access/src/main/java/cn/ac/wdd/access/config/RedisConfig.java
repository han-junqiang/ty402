package cn.ac.wdd.access.config;


import cn.ac.wdd.access.entity.User;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.connection.RedisPassword;
import org.springframework.data.redis.connection.RedisStandaloneConfiguration;
import org.springframework.data.redis.connection.jedis.JedisClientConfiguration;
import org.springframework.data.redis.connection.jedis.JedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.serializer.Jackson2JsonRedisSerializer;
import org.springframework.data.redis.serializer.StringRedisSerializer;
import redis.clients.jedis.JedisPoolConfig;

import java.time.Duration;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ExecutorService;

// TODO resolve these warnings
@SuppressWarnings({"unchecked", "rawtypes"})
@Configuration
@EnableCaching
public class RedisConfig {

  @Value("${spring.redis.host}")
  private String host;
  @Value("${spring.redis.port}")
  private Integer port;
  @Value("${spring.redis.database:0}")
  private Integer database;
  @Value("${spring.redis.password}")
  private String password;
  @Value("${spring.redis.timeout}")
  private Integer timeout;

  @Value("${spring.redis.jedis.pool.max-active}")
  private Integer maxActive;
  @Value("${spring.redis.jedis.pool.max-idle}")
  private Integer maxIdle;
  @Value("${spring.redis.jedis.pool.max-wait}")
  private Long maxWait;
  @Value("${spring.redis.jedis.pool.min-idle}")
  private Integer minIdle;


  @Bean
  public RedisConnectionFactory connectionFactory() {
    JedisPoolConfig poolConfig = new JedisPoolConfig();
    poolConfig.setMaxTotal(maxActive);
    poolConfig.setMaxIdle(maxIdle);
    poolConfig.setMaxWaitMillis(maxWait);
    poolConfig.setMinIdle(minIdle);
    poolConfig.setTestWhileIdle(true);
    JedisClientConfiguration jedisClientConfiguration = JedisClientConfiguration.builder()
        .usePooling().poolConfig(poolConfig).and().readTimeout(Duration.ofMillis(timeout))
        .build();
    RedisStandaloneConfiguration redisStandaloneConfiguration = new RedisStandaloneConfiguration();
    redisStandaloneConfiguration.setDatabase(database);
    redisStandaloneConfiguration.setPort(port);
    redisStandaloneConfiguration.setHostName(host);
    redisStandaloneConfiguration.setPassword(password);
    return new JedisConnectionFactory(redisStandaloneConfiguration, jedisClientConfiguration);
  }

  @Bean("redisTemplateDefault")
  public RedisTemplate redisTemplate(
      RedisConnectionFactory factory) {
    RedisTemplate template = new RedisTemplate();
    template.setConnectionFactory(factory);
    StringRedisSerializer stringRedisSerializer = new StringRedisSerializer();
    template.setDefaultSerializer(stringRedisSerializer);
    template.setKeySerializer(stringRedisSerializer);
    template.setHashKeySerializer(stringRedisSerializer);
    template.setValueSerializer(stringRedisSerializer);
    return template;
  }

  @Bean
  public RedisTemplate<String, User> redisUserInfoTemplate(
      RedisConnectionFactory redisConnectionFactory) {
    RedisTemplate<String, User> redisTemplate = new RedisTemplate<>();
    redisTemplate.setConnectionFactory(redisConnectionFactory);
    redisTemplate.setKeySerializer(new StringRedisSerializer());
    redisTemplate.setValueSerializer(new Jackson2JsonRedisSerializer(User.class));
    return redisTemplate;
  }
}
