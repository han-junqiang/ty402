package cn.ac.wdd.access.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
@Getter
@Setter
@TableName("t_menu")
@ApiModel(value = "Menu对象", description = "")
public class Menu implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("menu type  1表示按钮 2表示菜单")
    @TableField("type")
    private Integer type;

    @ApiModelProperty("菜单名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("菜单url_id")
    @TableField("access_id")
    private Long accessId;

    @ApiModelProperty("图标")
    @TableField("icon")
    private String icon;

    @ApiModelProperty("排序")
    @TableField("sort")
    private String sort;

    @ApiModelProperty("父菜单id")
    @TableField("parent_id")
    private Long parentId;

    @ApiModelProperty("是否显示")
    @TableField("is_show")
    private Boolean isShow;

    @ApiModelProperty("创建人")
    @TableField("create_by")
    private Integer createBy;

    @ApiModelProperty("更新人")
    @TableField("update_by")
    private Integer updateBy;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("菜单url")
    @TableField(exist = false)
    private String url;

    @ApiModelProperty("请求方式")
    @TableField(exist = false)
    private String method;

    @ApiModelProperty("子菜单")
    @TableField(exist = false)
    private List<Menu> Children;

    public static final String ID = "id";

    public static final String TYPE = "type";

    public static final String NAME = "name";

    public static final String ACCESS_ID = "access_id";

    public static final String ICON = "icon";

    public static final String SORT = "sort";

    public static final String PARENT_ID = "parent_id";

    public static final String IS_SHOW = "is_show";

    public static final String CREATE_BY = "create_by";

    public static final String UPDATE_BY = "update_by";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

}
