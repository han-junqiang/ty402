package cn.ac.wdd.access.service;

import cn.ac.wdd.access.entity.User;
import cn.ac.wdd.common.util.Page;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-06
 */
public interface IUserService extends IService<User> {

    Page<User> selectByPage(Page<User> page);
}
