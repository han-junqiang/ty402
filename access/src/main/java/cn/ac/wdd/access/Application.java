package cn.ac.wdd.access;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

/**
 * @Auther hanjunqiang
 * @Date 2022/3/29
 */
@SpringBootApplication
@EnableWebMvc
@MapperScan("cn.ac.wdd.access.mapper")
public class Application {
  public static void main(String[] args) {
    SpringApplication.run(Application.class, args);
  }
}
