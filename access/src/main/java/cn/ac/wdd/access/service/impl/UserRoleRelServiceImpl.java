package cn.ac.wdd.access.service.impl;

import cn.ac.wdd.access.entity.Menu;
import cn.ac.wdd.access.entity.Role;
import cn.ac.wdd.access.entity.UserRoleRel;
import cn.ac.wdd.access.mapper.RoleMapper;
import cn.ac.wdd.access.mapper.UserRoleRelMapper;
import cn.ac.wdd.access.service.IUserRoleRelService;
import cn.ac.wdd.access.util.SecurityUtils;
import cn.ac.wdd.access.util.TreeUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.qcloud.cos.utils.CollectionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * <p>
 * 用户-角色关联表 服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
@Service
public class UserRoleRelServiceImpl extends ServiceImpl<UserRoleRelMapper, UserRoleRel> implements IUserRoleRelService {

    @Autowired
    private UserRoleRelMapper userRoleRelMapper;

    @Autowired
    private RoleMapper roleMapper;

    @Override
    public int addRoles(Integer[] ids,Integer id) {
        HashMap<String, Object> rolesMap = new HashMap<>();
        rolesMap.put("userId", id);
        rolesMap.put("roleIds",ids);
        Integer result = userRoleRelMapper.insertBatch(rolesMap);
        return result;
    }

    @Override
    public void removeRole(Integer id) {
        QueryWrapper<UserRoleRel> userRoleRelQueryWrapper = new QueryWrapper<>();
        userRoleRelQueryWrapper.eq(UserRoleRel.USER_ID,id);
        userRoleRelMapper.delete(userRoleRelQueryWrapper);
    }

    @Override
    public Map<String, Object> getUserMenu(Integer id) {
        List<Menu> menuList = userRoleRelMapper.selectAllByUserId(id);
        /*找根节点*/
        List<Menu> rootMenu = TreeUtil.findMenuRoot(menuList);
        /*为根节点设置子菜单*/
        for(Menu menuVo : rootMenu){
            /*获取某个根节点的子菜单*/
            List<Menu> childList = TreeUtil.findMenuChild(menuVo.getId(),menuList);
            menuVo.setChildren(childList);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("tree", rootMenu);
        return map;
    }

    @Override
    public List<Role> getUserRole(Integer id) {
        QueryWrapper<UserRoleRel> userRoleRelQueryWrapper = new QueryWrapper<>();
        userRoleRelQueryWrapper.eq(UserRoleRel.USER_ID,id);
        List<UserRoleRel> userRoleRels = userRoleRelMapper.selectList(userRoleRelQueryWrapper);
        List<Role> roleList = new ArrayList<>();
        if (!CollectionUtils.isNullOrEmpty(userRoleRels)){
            roleList = roleMapper.selectBatchIds(userRoleRels.stream().map(UserRoleRel::getRoleId).collect(Collectors.toList()));
        }
        return roleList;
    }
}
