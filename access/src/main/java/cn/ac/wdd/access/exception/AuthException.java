package cn.ac.wdd.access.exception;

/**
 * @author yang
 * @date 2021/3/5
 **/
public class AuthException extends BaseException {
  private static final long serialVersionUID = 1L;

  private static final int ERROR_CODE = 403;

  public AuthException(String message) {
    super(ERROR_CODE, message);
  }

  public AuthException(int code, String message) {
    super(code, message);
  }
}
