package cn.ac.wdd.access.service;

import cn.ac.wdd.access.entity.Menu;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
public interface IMenuService extends IService<Menu> {

    Map<String, Object> getAllMenu();

}
