package cn.ac.wdd.access.controller;


import cn.ac.wdd.access.entity.Menu;
import cn.ac.wdd.access.service.IMenuService;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.*;

import org.springframework.stereotype.Controller;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;

import javax.annotation.Resource;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
@RestController
@RequestMapping("/menu")
public class MenuController {


    @Resource
    private IMenuService menuService;

    @RequestMapping(value = "/getAllMenu",method = RequestMethod.GET)
    @ApiOperation(value = "查询所有菜单信息",notes = "查询所有菜单信息")
    public ResultMessage<Map<String, Object>> getAllMenu(){
        Map<String, Object> map = menuService.getAllMenu();
        return R.ok(map);
    }


    @RequestMapping(value = "/addMenu",method = RequestMethod.POST)
    @ApiOperation(value = "添加菜单",notes = "添加菜单")
    public ResultMessage<String> addMenu(@RequestBody Menu menu){
        menuService.save(menu);
        return  R.ok("添加成功");
    }

    @RequestMapping(value = "/deleteMenu/{id}",method = RequestMethod.DELETE)
    @ApiOperation(value = "根据ID删除菜单",notes = "根据ID删除菜单")
    public ResultMessage<String> deleteMenu(@PathVariable Integer id){
        boolean check = menuService.removeById(id);
        if (check == true){
            return  R.ok("删除成功");
        }else{
            return R.err("删除失败！");
        }
    }


    @RequestMapping(value = "/viewMenu/{id}",method = RequestMethod.GET)
    @ApiOperation(value = "根据ID查看用户菜单",notes = "根据ID查看用户菜单")
    public ResultMessage<Menu> viewMenu(@PathVariable Integer id){
        Menu menu = menuService.getById(id);
        return  R.ok(menu);
    }


    @RequestMapping(value = "/update",method = RequestMethod.PUT)
    @ApiOperation(value = "根据ID更新用户菜单",notes = "根据ID更新用户菜单")
    public ResultMessage<String> update(@RequestBody Menu menu){
        boolean check = menuService.updateById(menu);
        if (check == true){
            return  R.ok("更新成功！");
        }else{
            return R.err("更新失败！");
        }
    }

}
