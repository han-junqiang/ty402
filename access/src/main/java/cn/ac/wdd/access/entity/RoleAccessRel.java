package cn.ac.wdd.access.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Getter;
import lombok.Setter;

/**
 * <p>
 * 角色-权限关联表
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
@Getter
@Setter
@TableName("t_role_access_rel")
@ApiModel(value = "RoleAccessRel对象", description = "角色-权限关联表")
public class RoleAccessRel implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("角色id")
    @TableField("role_id")
    private Long roleId;

    @ApiModelProperty("权限id")
    @TableField("access_id")
    private Long accessId;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private LocalDateTime createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private LocalDateTime updateAt;

    @ApiModelProperty("创建用户id")
    @TableField("create_by")
    private Long createBy;

    @ApiModelProperty("修改用户id")
    @TableField("update_by")
    private Long updateBy;


    public static final String ID = "id";

    public static final String ROLE_ID = "role_id";

    public static final String ACCESS_ID = "access_id";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String CREATE_BY = "create_by";

    public static final String UPDATE_BY = "update_by";

}
