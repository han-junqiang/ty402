package cn.ac.wdd.access.protocol.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class WechatLoginReq {
    @ApiModelProperty(value = "code")
    private String code;
}
