package cn.ac.wdd.access;


import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.util.Collections;

/**
 * @createTime 2022年05月06日 13:05:00
 */
public class generatorUtilAdv {
    public static void main(String[] args) {
        FastAutoGenerator.create("jdbc:mysql://127.0.0.1:3306/ecommerce?useUnicode=true&characterEncoding=UTF-8&zeroDateTimeBehavior=convertToNull&serverTimezone=GMT%2B8", "root", "admin")
                .globalConfig(builder -> {
                    builder.author("") // 设置作者
                            .enableSwagger() // 开启 swagger 模式
                            .fileOverride()
                            .outputDir("E:\\WorkCode\\wdd_graduation_project\\ty402\\advertising\\src\\main\\java"); // 指定输出目录
                })
                .packageConfig(builder -> {
                    builder.parent("cn.ac.wdd") // 设置父包名
                            .moduleName("advertising") // 设置父包模块名
                            .pathInfo(Collections.singletonMap(OutputFile.mapperXml, "E:\\WorkCode\\wdd_graduation_project\\ty402\\advertising\\src\\main\\resources\\mapper")); // 设置mapperXml生成路径
                })
                .templateConfig(builder -> {
                    builder
                            .disable(TemplateType.CONTROLLER)
                            .disable(TemplateType.SERVICEIMPL)
                            .disable(TemplateType.SERVICE)
//                            .disable(TemplateType.MAPPER)
//                            .disable(TemplateType.XML)
//                            .disable(TemplateType.ENTITY)
                            .build();
                })
                .strategyConfig(builder -> {
                    builder.addInclude("t_order_detail")// 设置需要生成的表名
                            .addTablePrefix("t_") // 设置过滤表前缀
                            .entityBuilder()
                            .enableTableFieldAnnotation()
                            .enableColumnConstant()
                            .enableLombok().build()
                    ;
                })
                .templateEngine(new FreemarkerTemplateEngine()) // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .execute();
    }
}
