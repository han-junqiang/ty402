package cn.ac.wdd.access.controller;


import cn.ac.wdd.access.entity.Role;
import cn.ac.wdd.access.entity.User;
import cn.ac.wdd.access.service.IUserRoleRelService;
import cn.ac.wdd.access.service.IUserService;
import cn.ac.wdd.common.util.Page;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  前端控制器
 * </p>
 *
 * @author 
 * @since 2022-05-15
 */
@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private IUserRoleRelService userRoleRelService;

    @Autowired
    private IUserService userService;


    @ApiOperation("分页获取用户列表")
    @PostMapping("/list")
    public ResultMessage<Page<User>> getList(@RequestBody Page<User> page){
        page = userService.selectByPage(page);
        return R.ok(page);
    }

    @RequestMapping(value = "/addRoles/{id}",method = RequestMethod.POST)
    @ApiOperation(value = "为用户批量添加角色",notes = "")
    public ResultMessage<String> addRoles(@RequestBody Integer[] ids, @PathVariable Integer id){

        userRoleRelService.removeRole(id);
        int number = userRoleRelService.addRoles(ids,id);
        if (number > 0){
            return  R.ok("添加成功");
        }else{
            return R.ok("添加失败");
        }
    }

    @RequestMapping(value = "/getUserRole/{id}",method = RequestMethod.GET)
    @ApiOperation(value = "查询用户角色")
    public ResultMessage<List<Role>> getUserRole(@PathVariable Integer id){
        List<Role> roleList = userRoleRelService.getUserRole(id);
        return R.ok(roleList);
    }

    
    @RequestMapping(value = "/delete/{id}", method = RequestMethod.DELETE)
    @ApiOperation("根据id删除用户")
    public ResultMessage<Boolean> deleteUserById(@PathVariable Long id){
        boolean remove = userService.removeById(id);
        return R.ok(remove);
    }

//    @RequestMapping(value = "/getUserMenu/{id}",method = RequestMethod.GET)
//    @ApiOperation(value = "查询用户权限",notes = "根据ID查询用户权限")
//    public  void getUserMenu(@PathVariable Integer id){
//        Map<String, Object> map = userRoleRelService.getUserMenu(id);
//
//        return R.ok()
//    }

}
