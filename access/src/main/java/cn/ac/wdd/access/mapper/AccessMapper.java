package cn.ac.wdd.access.mapper;

import cn.ac.wdd.access.entity.Access;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
@Mapper
public interface AccessMapper extends BaseMapper<Access> {

    Integer checkAuthorization(Long userId, String url, String method);
}
