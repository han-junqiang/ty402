package cn.ac.wdd.access.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthorizeDto {
  private String userId;
  private Integer accessType;
  private String url;
  private String method;
}
