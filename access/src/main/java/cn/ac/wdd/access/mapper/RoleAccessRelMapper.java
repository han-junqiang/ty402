package cn.ac.wdd.access.mapper;

import cn.ac.wdd.access.entity.RoleAccessRel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 角色-权限关联表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
public interface RoleAccessRelMapper extends BaseMapper<RoleAccessRel> {

}
