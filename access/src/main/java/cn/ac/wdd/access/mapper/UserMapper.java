package cn.ac.wdd.access.mapper;

import cn.ac.wdd.access.entity.User;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-06
 */
@Mapper
public interface UserMapper extends BaseMapper<User> {

}
