package cn.ac.wdd.access.config;

import cn.ac.wdd.access.exception.AuthException;
import cn.ac.wdd.access.exception.InvalidDataException;
import cn.ac.wdd.access.exception.InvalidDataForUserException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import springfox.documentation.annotations.ApiIgnore;
import cn.ac.wdd.common.util.R;
import cn.ac.wdd.common.util.ResultMessage;

/**
 * @author yang date 2020/4/13
 **/
@ControllerAdvice
@Slf4j
@ApiIgnore
public class CommonControllerAdvice {

  /**
   * 数据非法错误
   */
  @ResponseBody
  @ExceptionHandler({InvalidDataException.class})
  public ResultMessage<String> validException(InvalidDataException ex) {
    log.error("EXCEPTION -> {}", ExceptionUtils.getRootCauseMessage(ex), ex);
    return R.err("请求数据异常，请稍后再试");
  }

  /**
   * 数据非法错误 - 面向用户提示
   */
  @ResponseBody
  @ExceptionHandler({InvalidDataForUserException.class})
  public ResultMessage<String> validForUserException(InvalidDataForUserException ex) {
    log.error("EXCEPTION -> {}", ExceptionUtils.getRootCauseMessage(ex), ex);
    return R.err(ex.getMessage());
  }

  @ResponseBody
  @ExceptionHandler({AuthException.class})
  public ResultMessage<String> authException(AuthException ex) {
    log.error("EXCEPTION -> {}", ex.toString(), ex);
    return R.errWithData(ex.getMessage());
  }

}
