package cn.ac.wdd.access.protocol.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
public class SmsLoginReq {
    @ApiModelProperty(value = "手机号")
    private String phoneNumber;
    private String smsCode;
}
