package cn.ac.wdd.access.mapper;

import cn.ac.wdd.access.entity.Menu;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
@Mapper
public interface MenuMapper extends BaseMapper<Menu> {

    List<Menu> selectAllList();

    List<Menu> selectAllListByIds(List<Long> ids);
}
