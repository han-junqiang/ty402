package cn.ac.wdd.access.config;


import cn.ac.wdd.access.constant.ResultMsg;
import cn.ac.wdd.access.entity.User;
import cn.ac.wdd.access.exception.AuthException;
import cn.ac.wdd.access.util.JwtUtil;
import com.google.common.collect.Lists;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.ModelAndView;
import cn.ac.wdd.common.util.TokenUtil;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.List;
import java.util.Optional;


@Component
@Slf4j
public class JwtInterceptor implements HandlerInterceptor {

  @Autowired
  private JwtUtil jwtUtil;

  // 白名单接口
  private final List<String> WHITE_LIST_URL = Lists.newArrayList(
      ".*/swagger-ui.html", ".*/swagger-ui/.*", ".*/swagger-resources.*",
      ".*/v3/api-docs", ".*/webjars/.*",".*/doc.html"
  );


  @Override
  public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
      throws Exception {
    allowCors(response);

    if (isExcludedPath(request.getRequestURI())) {
      return true;
    }

    // 获取请求头信息authorization信息
    final String authHeader = request.getHeader(TokenUtil.AUTH_HEADER_KEY);
    log.info("## authHeader= {}", authHeader);
    Optional<String> tokenOptional = TokenUtil.getTokenFromHeader(authHeader);
    if (!tokenOptional.isPresent()) {
      log.warn("### 用户未登录，请先登录 ###");
      throw new AuthException(ResultMsg.USER_NOT_LOGGED_IN);
    }

    User user = jwtUtil.getUserInfo(tokenOptional.get());
    request.setAttribute(TokenUtil.USER_INFO_ATTRIBUTE_NAME, user);
    return true;
  }

  private boolean isExcludedPath(String path) {
    return WHITE_LIST_URL.stream().anyMatch(path::matches);
  }

  @Override
  public void postHandle(HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      Object o, ModelAndView modelAndView) throws Exception {
  }

  @Override
  public void afterCompletion(HttpServletRequest httpServletRequest,
      HttpServletResponse httpServletResponse,
      Object o, Exception e) throws Exception {
  }

  private void allowCors(HttpServletResponse response){
    response.setHeader("Access-Control-Allow-Origin",  "true");
    response.setHeader("Access-Control-Allow-Credentials", "true");
    response.setHeader("Access-Control-Allow-Methods", "POST, GET, PATCH, DELETE, PUT");
    response.setHeader("Access-Control-Allow-Headers", "*");
  }
}