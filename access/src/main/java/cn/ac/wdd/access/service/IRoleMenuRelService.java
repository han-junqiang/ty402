package cn.ac.wdd.access.service;

import cn.ac.wdd.access.entity.RoleMenuRel;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
public interface IRoleMenuRelService extends IService<RoleMenuRel> {

}
