package cn.ac.wdd.access.mapper;

import cn.ac.wdd.access.entity.Role;
import cn.ac.wdd.access.entity.RoleMenuRel;
import cn.ac.wdd.common.util.Page;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
public interface RoleMenuRelMapper extends BaseMapper<RoleMenuRel> {
//    @Param("menusMap")
    Integer insertBatch(HashMap<String, Object> menusMap);

    List<Role> getAllByPage(Page<Role> page);

    int getCountByPage(Page<Role> page);
}
