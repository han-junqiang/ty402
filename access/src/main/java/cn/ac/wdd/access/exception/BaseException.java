package cn.ac.wdd.access.exception;

/**
 * @author yang
 * @date 2021/3/5
 **/
public class BaseException extends RuntimeException {

  private static final long serialVersionUID = 1L;

  private int code;
  private String message;

  public BaseException(int code, String message) {
    this.code = code;
    this.message = message;
  }

  public BaseException(int code, String message, Throwable throwable) {
    super(message, throwable);
    this.code = code;
    this.message = message;

  }

  public int getCode() {
    return code;
  }

  public void setCode(int code) {
    this.code = code;
  }

  @Override
  public String getMessage() {
    return message;
  }

  public void setMessage(String message) {
    this.message = message;
  }
}
