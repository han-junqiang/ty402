package cn.ac.wdd.access.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WechatAuthCodeResponseDto {
  private String openid;
  private String session_key;
  private String errcode;
  private String errmsg;
  private Long expiresIn;
  private String unionid;
}
