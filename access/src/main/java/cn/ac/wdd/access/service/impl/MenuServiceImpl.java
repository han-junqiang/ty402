package cn.ac.wdd.access.service.impl;

import cn.ac.wdd.access.entity.Access;
import cn.ac.wdd.access.entity.Menu;
import cn.ac.wdd.access.mapper.AccessMapper;
import cn.ac.wdd.access.mapper.MenuMapper;
import cn.ac.wdd.access.service.IMenuService;
import cn.ac.wdd.access.util.SecurityUtils;
import cn.ac.wdd.access.util.TreeUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import cn.ac.wdd.common.util.StringUtil;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
@Service
public class MenuServiceImpl extends ServiceImpl<MenuMapper, Menu> implements IMenuService {


    @Autowired
    private AccessMapper accessMapper;
    @Autowired
    private MenuMapper menuMapper;

    @Override
    @Transactional
    public boolean save(Menu entity) {
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        if (StringUtil.isNotEmpty(entity.getUrl())){
            Access access = Access.builder().accessType(1).method(entity.getMethod()).url(entity.getUrl()).createBy(userIdChecked).build();
            accessMapper.insert(access);
            entity.setAccessId(access.getId());
        }
        boolean save = super.save(entity);
        return save;
    }

    @Override
    public Map<String, Object> getAllMenu() {
        List<Menu> menuList = menuMapper.selectAllList();
        /*找根节点*/
        List<Menu> rootMenu = TreeUtil.findMenuRoot(menuList);
        /*为根节点设置子菜单*/
        for(Menu menuVo : rootMenu){
            /*获取某个根节点的子菜单*/
            List<Menu> childList = TreeUtil.findMenuChild(menuVo.getId(),menuList);
            menuVo.setChildren(childList);
        }
        Map<String, Object> map = new HashMap<>();
        map.put("tree", rootMenu);
        return map;
    }

    @Override
    public Menu getById(Serializable id) {
        Menu menu = super.getById(id);
        if (menu.getAccessId() != null){
            Access access = accessMapper.selectById(menu.getAccessId());
            menu.setMethod(access.getMethod());
            menu.setUrl(access.getUrl());
        }
        return menu;
    }

    @Override
    public boolean removeById(Serializable id) {
        Menu menu = menuMapper.selectById(id);
        accessMapper.deleteById(menu.getAccessId());
        return super.removeById(id);
    }

    @Override
    public boolean updateById(Menu entity) {
        Menu menu = menuMapper.selectById(entity.getId());
        Access access = accessMapper.selectById(entity.getAccessId());
        if (StringUtil.isNotEmpty(entity.getMethod()) && StringUtil.isNotEmpty(entity.getUrl())){
            if ( !entity.getMethod().equals(access.getMethod()) || !entity.getUrl().equals(access.getUrl())){
                accessMapper.deleteById(access.getId());
                access = Access.builder().accessType(1).method(entity.getMethod()).url(entity.getUrl()).createBy(SecurityUtils.getCurrUserIdChecked()).build();
                accessMapper.insert(access);
                menu.setAccessId(access.getId());
            }
        }
        return super.updateById(entity);
    }

}
