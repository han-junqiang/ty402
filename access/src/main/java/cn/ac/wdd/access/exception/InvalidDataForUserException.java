/**
 * *****************************************************
 * <p>
 * Copyright (C) 2021 bodypark.ai All Rights Reserved This file is part of bodypark project.
 * Unauthorized copy of this file, via any medium is strictly prohibited. Proprietary and
 * Confidential.
 * <p>
 * ****************************************************
 **/
package cn.ac.wdd.access.exception;

import lombok.Getter;

/**
 * @author Zou Pengfei<zoupengfei@bodypark.ai>
 * @date 05/18/2021 10:11
 */
@Getter
public class InvalidDataForUserException extends BaseException {

  private static final long serialVersionUID = -3257646757141507815L;
  private static final int ERROR_CODE = 500;
  private String errorCode;

  public InvalidDataForUserException(String message) {
    super(ERROR_CODE, message);
  }

  public InvalidDataForUserException(String errorCode, String message) {
    super(ERROR_CODE, message);
    this.errorCode = errorCode;
  }

  public InvalidDataForUserException(String errorCode, String message, Throwable throwable) {
    super(ERROR_CODE, message);
    this.errorCode = errorCode;
  }
}
