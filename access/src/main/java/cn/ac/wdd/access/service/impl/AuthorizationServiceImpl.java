package cn.ac.wdd.access.service.impl;

import cn.ac.wdd.access.dto.AuthorizeDto;
import cn.ac.wdd.access.entity.Access;
import cn.ac.wdd.access.entity.User;
import cn.ac.wdd.access.grpc.AuthorizationRequest;
import cn.ac.wdd.access.grpc.AuthorizationResponse;
import cn.ac.wdd.access.grpc.AuthorizationServiceGrpc;
import cn.ac.wdd.access.mapper.AccessMapper;
import cn.ac.wdd.access.util.SecurityUtils;
import io.grpc.stub.StreamObserver;
import lombok.extern.slf4j.Slf4j;
import org.lognet.springboot.grpc.GRpcService;
import org.springframework.beans.factory.annotation.Autowired;

import javax.security.auth.message.AuthException;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

@GRpcService
@Slf4j
public class AuthorizationServiceImpl extends
        AuthorizationServiceGrpc.AuthorizationServiceImplBase{


    @Autowired
    private AccessMapper accessMapper;

    public void authorize(AuthorizationRequest request,
                          StreamObserver<AuthorizationResponse> responseObserver) {
        try {
            Integer resCode = buildAuthorizationDto(request);
            responseObserver.onNext(AuthorizationResponse.newBuilder()
                    .setResCode(resCode).build());
            log.debug("[{}]{} -> {}", request.getMethod(), request.getUrl(), resCode);
            responseObserver.onCompleted();
        } catch (AuthException e) {
            responseObserver
                    .onNext(AuthorizationResponse.newBuilder().setResCode(2).build());
            responseObserver.onCompleted();
        } catch (Exception e) {
            log.error("grpc exception: ", e);
        }
    }

    private Integer buildAuthorizationDto(AuthorizationRequest request) throws AuthException {
        String url = request.getUrl();
        String method = request.getMethod();
        boolean matched = false;
        if (request.getAccessType() == 1) {
            for (Access access : accessMapper.selectList(null)) {
                if (access.getMethod() != null && access.getMethod().equals(method)
                        && access.getUrl() != null && url.matches(access.getUrl().replace("*", ".*"))) {
                    url = access.getUrl();
                    matched = true;
                    break;
                }
            }
        }
        if (!matched) {
            throw new AuthException(String.format("do not find access url %s method %s", url, method));
        }
        User user = SecurityUtils.getCurrUserChecked();
        return accessMapper.checkAuthorization(user.getId(), url, method);
    }
}
