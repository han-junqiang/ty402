package cn.ac.wdd.access.service.impl;

import cn.ac.wdd.access.entity.Menu;
import cn.ac.wdd.access.entity.Role;
import cn.ac.wdd.access.entity.RoleAccessRel;
import cn.ac.wdd.access.entity.RoleMenuRel;
import cn.ac.wdd.access.mapper.*;
import cn.ac.wdd.access.service.IRoleService;
import cn.ac.wdd.access.util.SecurityUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.joda.time.format.DateTimeFormat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import cn.ac.wdd.common.util.Page;

import javax.management.QueryEval;
import java.io.Serializable;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/**
 * <p>
 * 角色表 服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Autowired
    private RoleMapper roleMapper;
    @Autowired
    private RoleAccessRelMapper roleAccessRelMapper;
    @Autowired
    private MenuMapper menuMapper;
    @Autowired
    private RoleMenuRelMapper roleMenuRelMapper;

    @Override
    public boolean save(Role entity) {
        entity.setCreateBy(SecurityUtils.getCurrUserIdChecked());
        return super.save(entity);
    }

    @Override
    public boolean removeById(Serializable id) {
        Role role = roleMapper.selectById(id);
        QueryWrapper<RoleAccessRel> roleAccessRelQueryWrapper = new QueryWrapper<>();
        QueryWrapper<RoleMenuRel> roleMenuRelQueryWrapper = new QueryWrapper<>();
        roleAccessRelQueryWrapper.eq(RoleAccessRel.ROLE_ID,role.getId());
        roleMenuRelQueryWrapper.eq(RoleAccessRel.ROLE_ID,role.getId());
        roleAccessRelMapper.delete(roleAccessRelQueryWrapper);
        roleMenuRelMapper.delete(roleMenuRelQueryWrapper);
        return super.removeById(id);
    }

    @Override
    public List<Menu> getMenuByRoleId(Long roleId) {
        QueryWrapper<RoleMenuRel> roleMenuRelQueryWrapper = new QueryWrapper<>();
        roleMenuRelQueryWrapper.eq(RoleMenuRel.ROLE_ID, roleId);
        List<RoleMenuRel> roleMenuRels = roleMenuRelMapper.selectList(roleMenuRelQueryWrapper);
        List<Menu> menuList = menuMapper.selectAllListByIds(roleMenuRels.stream().map(RoleMenuRel::getMenuId).collect(Collectors.toList()));
        return menuList;
    }

    @Override
    public int addMenus(Integer[] menuIds, Integer roleId) {
        HashMap<String, Object> menusMap = new HashMap<>();
        menusMap.put("menuIds",menuIds);
        menusMap.put("roleId",roleId);
        Integer result = roleMenuRelMapper.insertBatch(menusMap);
        return result;
    }

    @Override
    public Page<Role> getAllByPage(Page<Role> page) {
        List<Role> roleList = roleMenuRelMapper.getAllByPage(page);

        roleList.forEach(p->{
            DateTimeFormatter dateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
            Instant instant = p.getUpdateAt().toInstant();
            ZoneId zoneId = ZoneId.systemDefault();
            LocalDateTime localDateTime = instant.atZone(zoneId).toLocalDateTime();
            String format = dateTimeFormatter.format(localDateTime);
            p.setUpdateTimeStr(format);
        });
        int tatalCount = roleMenuRelMapper.getCountByPage(page);
        page.setList(roleList);
        page.setTotalCount(tatalCount);
        return page;
    }

    @Override
    public void removeMenus(Integer id) {
        QueryWrapper<RoleMenuRel> roleMenuRelQueryWrapper = new QueryWrapper<>();
        roleMenuRelQueryWrapper.eq(RoleMenuRel.ROLE_ID,id);
        roleMenuRelMapper.delete(roleMenuRelQueryWrapper);
    }
}
