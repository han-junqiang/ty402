package cn.ac.wdd.access.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Date;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;

/**
 * <p>
 * 角色表
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
@Getter
@Setter
@TableName("t_role")
@ApiModel(value = "Role对象", description = "角色表")
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Role implements Serializable {

    private static final long serialVersionUID = 1L;

    @TableId(value = "id", type = IdType.AUTO)
    private Long id;

    @ApiModelProperty("角色名称")
    @TableField("name")
    private String name;

    @ApiModelProperty("父角色id（目前没有权限继承的概念）")
    @TableField("parent_role_id")
    private Long parentRoleId;

    @ApiModelProperty("创建时间")
    @TableField("create_at")
    private Date createAt;

    @ApiModelProperty("更新时间")
    @TableField("update_at")
    private Date updateAt;

    @ApiModelProperty("展示更新时间")
    @TableField(exist = false)
    private String updateTimeStr;

    @ApiModelProperty("创建用户id")
    @TableField("create_by")
    private Long createBy;

    @ApiModelProperty("修改用户id")
    @TableField("update_by")
    private Long updateBy;


    public static final String ID = "id";

    public static final String NAME = "name";

    public static final String PARENT_ROLE_ID = "parent_role_id";

    public static final String CREATE_AT = "create_at";

    public static final String UPDATE_AT = "update_at";

    public static final String CREATE_BY = "create_by";

    public static final String UPDATE_BY = "update_by";

}
