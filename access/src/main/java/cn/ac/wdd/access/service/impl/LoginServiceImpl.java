package cn.ac.wdd.access.service.impl;

import cn.ac.wdd.access.constant.RedisKeyTemplate;
import cn.ac.wdd.access.dto.WechatAuthCodeResponseDto;
import cn.ac.wdd.access.entity.Role;
import cn.ac.wdd.access.entity.User;
import cn.ac.wdd.access.entity.UserRoleRel;
import cn.ac.wdd.access.exception.AuthException;
import cn.ac.wdd.access.mapper.UserMapper;
import cn.ac.wdd.access.protocol.request.MiniUserReq;
import cn.ac.wdd.access.protocol.request.SmsLoginReq;
import cn.ac.wdd.access.protocol.request.WechatLoginReq;
import cn.ac.wdd.access.service.ILoginService;
import cn.ac.wdd.access.service.IRoleService;
import cn.ac.wdd.access.service.IUserRoleRelService;
import cn.ac.wdd.access.util.*;
import cn.ac.wdd.access.vo.LoginVo;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import cn.ac.wdd.common.util.StringUtil;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 
 * @since 2022-05-06
 */
@Service
@Slf4j
public class LoginServiceImpl implements ILoginService {

    private static final Long EXPIRES = 86400L;
    @Autowired
    private  UserMapper userMapper;
    @Autowired
    private SendMessage sendMessage;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;

    @Autowired
    private JwtUtil jwtUtil;

    @Value("${wechat.appid}")
    private String appid;

    @Value(("${wechat.secret}"))
    private String secret;
    @Value(("${wechat.grantType}"))
    private String grantType;
    @Value(("${wechat.sessionHost}"))
    private String wechatHost;

    @Autowired
    private IUserRoleRelService userRoleRelService;

    @Autowired
    private TencentOSSClient tencentOSSClient;

    @Override
    public String getSms(SmsLoginReq smsLoginReq) {
        String phoneNumber = smsLoginReq.getPhoneNumber();
        // 检测用户是否注册
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        QueryWrapper<User> userWrapper = userQueryWrapper.eq(User.PHONE_NUNMBER, smsLoginReq.getPhoneNumber());
        User user = userMapper.selectOne(userWrapper);
        if (user ==null){
            log.info(phoneNumber + ":  用户未注册");
            throw new AuthException("用户未注册，请联系管理员或者小程序端注册");
        }
        //随机生成六位验证码
        String randomCode = String.valueOf((int) ((Math.random() * 9 + 1) * 100000));
        try {
            sendMessage.sendSms(phoneNumber, randomCode);
            redisTemplate.opsForValue().set(RedisUtil.buildReidsKey(RedisKeyTemplate.T_VERIFICATION_CODE, phoneNumber), randomCode, 2, TimeUnit.MINUTES);
            log.info(phoneNumber + ":  发送了一条验证码");
            return randomCode;
        }catch (Exception e){
            log.info(phoneNumber + ":  验证码发送失败，请稍后再试");
            return null;
        }
    }

    @Override
    public LoginVo smsWebLogin(SmsLoginReq smsLoginReq) {
        LoginVo loginVo;
        if (StringUtil.isNotEmpty(smsLoginReq.getSmsCode())){
            String phoneNumber = smsLoginReq.getPhoneNumber();
            String smsCode = redisTemplate.opsForValue().get(RedisUtil.buildReidsKey(RedisKeyTemplate.T_VERIFICATION_CODE, phoneNumber));
            if (smsLoginReq.getSmsCode().equals(smsCode)){
                QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
                QueryWrapper<User> userWrapper = userQueryWrapper.eq(User.PHONE_NUNMBER, smsLoginReq.getPhoneNumber());
                User user = userMapper.selectOne(userWrapper);
                String token = jwtUtil.createJWT(user.getId());
                loginVo = LoginVo.builder().loginUser(User.builder().nickName(user.getNickName()).avatar(user.getAvatar()).gender(user.getGender()).build()).token(token).build();
            }else {
                throw new AuthException("验证码已过期或者验证码错误");
            }
        }else {
            throw new AuthException("验证码不能为空");
        }
        return loginVo;
    }

    @Override
    public LoginVo wechatWebLogin(WechatLoginReq wechatLoginReq) {

        String urlString = "?appid={appid}&secret={secret}&js_code={code}&grant_type={grantType}";
        String response = new RestTemplate().getForObject(
                wechatHost + urlString, String.class,
                appid,
                secret,
                wechatLoginReq.getCode(),
                grantType);
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        ObjectReader reader = objectMapper.readerFor(WechatAuthCodeResponseDto.class);
        WechatAuthCodeResponseDto res;
        try {
            res = reader.readValue(response);
        } catch (IOException e) {
            log.error("小程序第三方验证失败", e);
            res = null;
        }
        if (res == null || res.getErrcode() != null) {
            throw new RuntimeException("微信验证接口调用失败");
        }
        res.setExpiresIn(res.getExpiresIn() != null ? res.getExpiresIn() : EXPIRES);
        res.getSession_key();
        redisTemplate.opsForValue().set(RedisUtil.buildReidsKey(RedisKeyTemplate.SESSION_KEY,res.getOpenid()),res.getSession_key(),60, TimeUnit.DAYS);
        QueryWrapper<User> userQueryWrapper = new QueryWrapper<>();
        userQueryWrapper.eq(User.OPPEN_ID,res.getOpenid());
        User user = userMapper.selectOne(userQueryWrapper);
        if (user == null){
            user = User.builder().oppenId(res.getOpenid()).nickName(null).avatar("/默认头像url").build();
            userMapper.insert(user);
            userRoleRelService.save(UserRoleRel.builder().userId(user.getId()).roleId(1L).build());
        }

        String token = jwtUtil.createJWT(user.getId());
        return LoginVo.builder().loginUser(user).token(token).build();
    }

    @Override
    public LoginVo wechatRegister(MiniUserReq userReq){
        Long userIdChecked = SecurityUtils.getCurrUserIdChecked();
        User user = User.builder().id(userIdChecked)
                .gender(userReq.getGender())
                .avatar(userReq.getAvatarUrl())
                .nickName(userReq.getNickName())
                .build();
        userMapper.updateById(user);
        return LoginVo.builder().loginUser(user).build();
    }
}
