package cn.ac.wdd.access.mapper;

import cn.ac.wdd.access.entity.Menu;
import cn.ac.wdd.access.entity.UserRoleRel;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import java.util.HashMap;
import java.util.List;

/**
 * <p>
 * 用户-角色关联表 Mapper 接口
 * </p>
 *
 * @author 
 * @since 2022-05-10
 */
public interface UserRoleRelMapper extends BaseMapper<UserRoleRel> {

    Integer insertBatch(HashMap<String, Object> rolesMap);

    List<Menu> selectAllByUserId(Integer id);
}
