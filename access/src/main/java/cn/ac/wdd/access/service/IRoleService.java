package cn.ac.wdd.access.service;

import cn.ac.wdd.access.entity.Menu;
import cn.ac.wdd.access.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;
import cn.ac.wdd.common.util.Page;

import java.util.List;

/**
 * <p>
 * 角色表 服务类
 * </p>
 *
 * @author 
 * @since 2022-05-07
 */
public interface IRoleService extends IService<Role> {


    List<Menu> getMenuByRoleId(Long roleId);


    int addMenus(Integer[] menuIds, Integer roleId);

    Page<Role> getAllByPage(Page<Role> page);

    void removeMenus(Integer id);
}
