package cn.ac.wdd.access.vo;

import cn.ac.wdd.access.entity.User;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class LoginVo {
  int state;
  String token;
  User loginUser;
}
