package cn.ac.wdd.access.controller;

import cn.ac.wdd.access.protocol.request.MiniUserReq;
import cn.ac.wdd.access.protocol.request.SmsLoginReq;
import cn.ac.wdd.access.protocol.request.WechatLoginReq;
import cn.ac.wdd.access.service.ILoginService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import cn.ac.wdd.access.util.SendMessage;
import cn.ac.wdd.access.vo.LoginVo;
import cn.ac.wdd.common.util.ResultMessage;
import cn.ac.wdd.common.util.StringUtil;

import java.io.IOException;

/**
 * @Auther hanjunqiang
 * @Date 2022/3/29
 */
@RestController
@Slf4j
@RequestMapping("/auth")
@Api("access")
public class LoginController {

  @Autowired
  private SendMessage sendMessage;
  @Autowired
  private ILoginService loginService;

  @GetMapping("/getSms")
  @ApiOperation(value = "获取短信验证码")
  @ApiImplicitParam( name = "phoneNumber",required = true, value = "组件id（修改时为必填）")
  @ResponseBody
  public ResultMessage<String> getSms(SmsLoginReq smsLoginReq){
    ResultMessage<String> result;
    String code = loginService.getSms(smsLoginReq);
    if(StringUtil.isNotEmpty(code)){
      result = new ResultMessage<String>().success(code).setResMsg("发送成功");
    }else {
      result = new ResultMessage<String>().failed("发送失败");
    }
    return result;
  }

  @PostMapping("/phoneNumber/web")
  @ApiOperation(value = "短信验证码登录（该方式只能后端登录）")
  @ResponseBody
  public ResultMessage<LoginVo> smsWebLogin(@RequestBody SmsLoginReq smsLoginReq) {
    LoginVo loginVo = loginService.smsWebLogin(smsLoginReq);
    return new ResultMessage<LoginVo>().success().setResult(loginVo);
  }

  @PostMapping("/wechat/login")
  @ApiOperation(value = "wechat登录")
  @ResponseBody
  public ResultMessage<LoginVo> wechatWebLogin(@RequestBody WechatLoginReq wechatLoginReq) {
    LoginVo loginVo = loginService.wechatWebLogin(wechatLoginReq);
    return new ResultMessage<LoginVo>().success().setResult(loginVo);
  }

  @PostMapping("/wechat/register")
  @ApiOperation(value = "wechat注册")
  @ResponseBody
  public ResultMessage<LoginVo> wechatRegister(@RequestBody MiniUserReq userReq){
    LoginVo loginVo = loginService.wechatRegister(userReq);
    return new ResultMessage<LoginVo>().success().setResult(loginVo);
  }
}
